function [D,beta_i,betahat,niter,betadiffs,diffs,allDs,allbs]=LGTS_EM(E,betahat0,Dhat0,beta_ihat,conv)
%%%------------------------------------------------------------------------
%
%   function LGTS_EM.m
%	Second Stage EM algotithm based on 5.12, 5.13 and 5.14 in Davidians book, Chapter 5. 
%
%  INPUT==>  E: FIM for all the estimates, 3D array
%			 betahat0: initial parameters of the mean
%			 Dhat0: initial parameters of the covariance
%			 beta_ihat: individual estimates from the first stage,
% 			 conv: structure containing information regarding convergence crietria, 
%					tolerance, maximum iteration
%
%  OUTPUT==> D: Cov. matrix 
%			 beta_i: ebayes individual estimates
%			 betahat: mean parameter estimate
%			 betadiffs: convergence of parameter vector: differences
%			 diffs: convergence of D: differences
%			 allbs: convergence of parameter vector
%			 allDs:  convergence of D
%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------

%% Initialize
betahat=betahat0;
D = Dhat0;

% Convergence criteria
diffs=[];     % stores the differences in successive iteration in D
betadiffs=[]; % stores the differences in successive iteration in betahat
allbs=[];	  % stores the values of betahats at each iteration
allDs=[];	  % stores the values of D estimates at each iteration


% Initialize
diffbeta=betahat; 
diffD=D;		 
niter=0; 		 % iteration counter


% convergence estimate
conv_flag=1;
maxiter=conv.maxiter;
tol=conv.tol;

while conv_flag==1
% 

%% E-step
beta_i=zeros(size(beta_ihat));
for  i=1:length(beta_ihat)
C_inv=E(:,:,i);
D_inv=get_stableinverse(D);
beta_i(i,:)=get_stableinverse(C_inv+D_inv)*(C_inv*beta_ihat(i,:)'+D_inv*betahat'); %5.12
end

%% M step
wi_t1=zeros(size(D)); %term 1 of Weight below 5.13
betadiff=zeros(size(betahat));
betahat0=betahat; %at iteration c
D0=D;  %at iteration c
% estimate betahat
wi_t1=length(beta_i)*D_inv; %sum over all D^-1
Wi=get_stableinverse(wi_t1)*D_inv; %5.13

betahat=zeros(length(beta_i),size(betahat,2));

for  i=1:length(beta_i)
betahat(i,:)=(Wi*beta_i(i,:)')';%sigma Wi_k*beta_i_k
end
betahat=sum(betahat);

% estimate D, given betahat
D_t1=zeros(size(D));
D_t2=0;
for i=1:length(beta_i)
C_inv=E(:,:,i);
D_t1=D_t1+get_stableinverse(C_inv+D_inv);  %term 1 from 5.15
betadiff=(betahat-beta_i(i,:));
D_t2=D_t2+((betadiff'*betadiff)); %term 2 from 5.15
betadiff=(betahat-beta_i(i,:));
end

D_t2=D_t2./size(beta_i,1);
D=D_t1./size(beta_i,1)+D_t2;

diffD=abs(D0-D);%./max(D0,D);
diffs=cat(1,diffs,max(max(diffD)));
diffbeta=abs(betahat0-betahat);
betadiffs=cat(1,betadiffs,max(max(diffbeta)));

allDs=cat(1,allDs,reshape(diag(D),1,size(D,2)));
allbs=cat(1,allbs,betahat);
disp([max(diffbeta),max(max(diffD))])

niter=niter+1;
conv_flag=niter<maxiter&&~all([(max(diffbeta))<tol,max(max(diffD))<tol]);
end