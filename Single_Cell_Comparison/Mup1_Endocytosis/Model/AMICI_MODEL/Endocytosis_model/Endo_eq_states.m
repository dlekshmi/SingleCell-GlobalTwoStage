function [f,T,obs_y,obs_sens_y,FIM,G] = Endo_eq(px,time_exp,ydata_response,theta_hat,int_opts)

% Assign the parameters: pp are the parameters for which sensitivities will
% be calulated and k are the parameters for which they will not be. 

N_C=3;                        % Number of endocytic channels; max number of endosomes; sets max V_E implicitly
bbeta=4;                      % Hill coefficient for fraction of prot.produced to go into membrane
ggamma=8;                     % Parameter related to detection.
tm= 0;                     % Methionine addition
N_E00=0;					  % initial conditions, fixed	
n_E00=0;					  % initial conditions, fixed
k1=8.41;                      % Assumed. 

u_in=1;

% Natural-log transform, so transform back.
px=[exp(px(1:16)'),px(17),px(18)]; 					% parameters which are estimated
k=[tm u_in N_C,bbeta,ggamma, k1, N_E00, n_E00];		% parameters which are assumed
pp=px;
pp0=px;

% Model equilibration
tsp0=linspace(-130,-10,30);
options = amioption(...
'atol',1e-4,'rtol',1e-4,'tstart',tsp0(1));
pp0(10)=0;
pp0(11)=0;
sol0=simulate_Endo_model(tsp0,[pp0],k,[],options);
pp(15:18)=sol0.x(end,[1,2,4,5]);       % Inital conditions are changed
k([7:8])=sol0.x(end,[3,6]);            % NE00 and nE00 are changed 

% Before input
tsp1=linspace(-10,tm,30);
options = amioption('sensi',0,...
'atol',int_opts.atol,'rtol',int_opts.rtol,'tstart',tsp1(1));
sol1=simulate_Endo_model(tsp1,[pp],k,[],options);

% After Input
tsp2=linspace(tm,90,100);
k(2)=0; %u_in 
pp(15:18)=sol1.x(end,[1,2,4,5]);       % Inital conditions are changed
k([7:8])=sol1.x(end,[3,6]);            % NE00 and nE00 are changed 
options = amioption('atol',int_opts.atol,'rtol',int_opts.rtol,'tstart',tm);
sol2=simulate_Endo_model(tsp2,pp,k,[],options);
f=[sol1.status, sol2.status];
f=any(f<0);

if(f==1)
    obs_y=[];
    T=[];
    FIM=[];
    obs_sens_y=[];
    G=[];
else
% aggregate results.
T=cat(1,sol1.t(1:end-1),sol2.t);
obs_y=cat(1,sol1.y(1:end-1,1:6,:),sol2.y(:,1:6,:));
%sens_y=cat(1,sol1.sy(1:end-1,1:6,:),sol2.sy(:,1:6,:));

% Get and Assign Sensitivities
% We are interested in the LOG space for all except intial conditions of
% the intensity.
obs_sens_y=[];
%obs_sens_y=zeros(length(time_exp),6,length(pp));
%for index=1:6
%obs_sens_y(:,index,:)=interp1(T,squeeze(sens_y(:,index,:)).*[pp(1:16),1,1],time_exp);
%end

% States
obs_y=interp1(T,obs_y,time_exp);

% Get covariance matrix 
if(isempty(theta_hat))
    FIM=[];
    G=[];
else
Noise_model=repmat(0,size(obs_y,1),1);
FIM=zeros(length(pp),length(pp));
G=zeros(1,length(pp));

for i=1:6
% Noise model, avoid division by zero.
 Y = ydata_response(:,i);
 availtime =~isnan(Y);
 Noise_model(:,i)=theta_hat(i,1)+theta_hat(i,2).*obs_y(:,i);
 Noise_model(Noise_model(:,i)<10^-10,5)=10^-10;

% Calculating likelihood gradient
%dNoise_dtheta=theta_hat(i,2).*1./obs_y(availtime,i).*squeeze(obs_sens_y(availtime,i,:));
%term1=-2*((diag(1./Noise_model(availtime,i).^3)*(Y(availtime)-obs_y(availtime,i)).^2))'*dNoise_dtheta;
%term2=-2.*(diag(1./Noise_model(availtime,i).^2)*(-obs_y(availtime,i)+Y(availtime)))'*(1./obs_y(availtime,i).*squeeze(obs_sens_y(availtime,i,:)));
%grads=+term1+term2;
grads=[];
% Calculating GLS FIM.
%c1=(squeeze(obs_sens_y(availtime,i,:))'*get_stable_inverse(diag(Noise_model(availtime,i).^2))*squeeze(obs_sens_y(availtime,i,:)));
%FIM=FIM+c1;
%G=G+grads;
end
end
end
end
