function h=var_fun(f_t,theta_hat)

h=zeros(size(f_t,1),size(f_t,2));
for i=1:size(f_t,2)
    h(:,i)=theta_hat(i,1)+f_t(:,i).*theta_hat(i,2);
end