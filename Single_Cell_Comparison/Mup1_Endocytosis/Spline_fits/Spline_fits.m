% This code fits smoothing splines, specified by a smoothing parameter
% In the end you get the results of the fits stored in a structure 
% along with the smoothing parameter that was defined. 
% May 2017, DL


clear all
addpath(genpath('.'))

% Storing results in Final_params
Final_params = struct;

% define smoothing parameter
sm_param=[0,0,0.001,0.001,0.7,0.001]; %I dont want things too wiggly nor a straight line

% Load Data
load('../Data/Indcell_data.mat');

%% Construct splines (for all data) and find error values, after making sure

for cellno=[1:max(data.Track_index)]
% Get data
ydata_tot = data(:,[1,2,7:8,10,13:15,16]);     % Excluded SPOTS VOLUME ans SPOT NUMBER SO FAR
index = ydata_tot.Track_index==cellno;
ydata = ydata_tot(index,:);        
ydata_response = table2array(ydata);
time = ydata_response(:,1);
ydata_response = ydata_response(:,[3:4,6:9]);  

% Declare things to be saved from each fit.
resids = zeros(size(ydata_response));
rmse = zeros(1,size(ydata_response,2));
spline_qual = 1:size(ydata_response,2);
spline_obs= zeros(size(ydata_response));
covs= zeros(size(ydata_response));
dofs=zeros(1,size(ydata_response,2));

sprintf('Doing:%d',cellno);

for i=1:size(ydata_response,2)
    y = ydata_response(:,i);
    M = cat(2,time,y);   
    avail = ~isnan(y);
    M = M(avail,:);
    [f,g] = fit(M(:,1),M(:,2),'smoothingspline','SmoothingParam',sm_param(i));
    spline_qual(i)=g.adjrsquare;
    splines = feval(f,time);
    resids(:,i) =(y-splines);
    rmse(i) =sqrt(nanmean((y-splines).^2));
    spline_obs(:,i) =splines;
    covs(:,i)=(resids(:,i).^2)./splines;
    dofs(i) =g.dfe;
end


%Save EVERYTHING :params, gradient, hessian, fval, S Dsim ydata
Final_params(cellno).ydata = ydata_response;
Final_params(cellno).time=time;
Final_params(cellno).resids = resids;
Final_params(cellno).rmse = rmse;
Final_params(cellno).spline_obs = spline_obs;
Final_params(cellno).spline_qual = spline_qual;
Final_params(cellno).covs = covs;
Final_params(cellno).dfes =  dofs;
end


% Save along with the smoothing parameter
save('Spline_paramsall.mat', 'Final_params','sm_param')

