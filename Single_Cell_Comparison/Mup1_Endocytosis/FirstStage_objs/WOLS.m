function [Fval] = WOLS(kinetic_params,time,thetahat,ydata_response,int_opts)
%%%------------------------------------------------------------------------
%
%   function WOLS.m
%
%   Standard WOLS (Weighted Ordinary Least Squares) method. 
%
%   Tools needed: 
        % a) ODE SOLVER

%  INPUT==> t: Time values in experiment,
%           kinetic_params: log kinetic parameters
%           thetahat: Noise parameters
%           ydata_response: data
%           int_opts: Options passed to integrator, structure.

%  OUTPUT==> Fval: The objective function weighted ordinary least squares. 

%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------

[~,T,Dsim,~,~,~]=Endo_eq(kinetic_params,time,[],[],int_opts); % Getting simulated data (Dsim) and time.


if(isempty(T))
    Fval=10^30;
    if nargout>1
    G=zeros(1,18)+Inf;                      % Incase gradient has to be returned.
    end
else
    Fval=0;
    for i=1:size(Dsim,2)                    % For each observation, treated independent.
        Y = ydata_response(:,i);    
        availtime = ~isnan(Y);
        Y = Y(availtime);
        len = size(Y); 
        D = Dsim(:,i);       
        D = D(availtime);
       
        % NOISE MODEL
        E = thetahat(i,1)+thetahat(i,2).*D; % additive and multiplicative model

        % WOLS 
        E(E<10^-10)=10^-10;                 % Avoiding division by zero. 
        LS_sum=sum(((Y-D)./E).^2);
        Fval = Fval+ (LS_sum) ;
   
    end
end
