;model definition
<MODEL>
[LONGITUDINAL]
input={ k2l ,k3l, k4l, k5l , K_Ml, alpha_Rl, T_Pl, T_El, alpha_El, mu_Cl,mu_Ml,V_EMl, V_ECl, deltal,VC0l,VM0l,NE0l,IC0n,IM0n,nE0l}

EQUATION:
k2=exp(k2l)
k3=exp(k3l)
k4=exp(k4l)
k5=exp(k5l)
K_M=exp(K_Ml)
alpha_R=exp(alpha_Rl)
T_P=exp(T_Pl)
T_E=exp(T_El)
alpha_E=exp(alpha_El)
V_EM=exp(V_EMl)
V_EC=exp(V_ECl)
delta=exp(deltal)
VC0=exp(VC0l)
VM0=exp(VM0l)
NE0=exp(NE0l)
IC0=IC0n;intensity not log-normal, it was found to be normally distributed in data at t=0
IM0=IM0n ;intensity not log-normal, it was found to be normally distributed in data   t=0
nE0=exp(nE0l)

;Fixed parameters
beta=4                          ; Set, Hill coefficient for fraction of prot. produced to go into membrane
gamma=8                     ; Set, Hill coefficient spot detection
N_C=3                          ; Endocytotic channels
k1=8.41			   ; Maximal endocytosis rates	 
;set flag for experiment start
if t>=-10
flag=1
else
flag=0
end

;set tm= time of Met addition
if flag==1
tm=0
else 
tm=-10000
end

;set mu_C mu_M
if flag==1
mu_C=exp(mu_Cl)
mu_M=exp(mu_Ml)
else
mu_C=0
mu_M=0
end

;set input u (1) in Document
if flag==1&t>tm
u=0
else
u=1
end

;set the initial point of integration
t0=-130
V_M_0=VM0
V_C_0=VC0
N_E_0=NE0
I_C_0=IC0
I_M_0=IM0
n_E_0=nE0


;Input sensed in teh protein level and endosome level
u_P=u+(1-u)*exp(-log(2)/T_P*(t-tm))                                     	
u_E=alpha_E+(1-alpha_E)*(1-u)*(1-exp(-log(2)/T_E*(t-tm))) 

;algebraic relations 					
v_P=k4*V_C*u_P          								 ;Total GFP Production rate by the ribosome 
alpha_M2=k2*(1-((I_M)^beta)/(K_M^beta+(I_M)^beta))   		 ;protein in cytoplasm put into the membrane, k2 is a linear rate constant
mu_E=k1*N_C*u_E        				 				 ;Rate of formation of  endosomes 
v_E=V_EM*I_M*mu_E   						 		 ;Flux of GFP from membrane  to endosomes 
     				 			

; differential equations
odeType=stiff
ddt_V_C=mu_C*V_C                                     			 	;Total cell volume 
ddt_V_M=mu_M*V_M                                      				;Membrane volume 
ddt_N_E=mu_E-(k5+k3)*N_E                                   			;Number of endosomes 

;Intensity of cell
ddt_I_C = v_P/V_C-k3*(I_C-I_M*V_M/V_C)-mu_C*I_C
; Intensity in memebrane
ddt_I_M =-v_E/V_M+alpha_R*k5*(n_E)/V_M-alpha_M2*(I_C*V_C-I_M*V_M-n_E)/V_M-mu_M*I_M					
ddt_n_E=v_E-(k3+k5)*(n_E)                               	           		 ;Number of molecules in endosomes 


; Observation Relations
V_E0=V_EC*N_C          				 				; Total endosome volume 
I_E0=n_E/V_E0           							 	; GFP conc. in endosomes, average 
V_CP0=V_C-V_M-V_E0								; Actual volume of cytoplasm 
I_CP0=(I_C*V_C-I_M*V_M-n_E)/V_CP0         				; GFP Intensity in cytoplasm; concentration of GFP in cytoplasm 

									

;Spot selection
F_S = (I_E0)^gamma/((delta*I_CP0)^gamma+(I_E0)^gamma)	;Fraction of detectable endosomes 
V_S = F_S*V_E0 									;Spot volume 
N_S=F_S*N_E										;Number of Spots 
I_S = F_S*n_E/V_S;									;Spot intensity 
;Corrected Cytoplasmic intensity after accounting for detectability 
I_CP = ((I_C*V_C-I_M*V_M-n_E)+(1-F_S)*n_E)/(V_C-V_M-V_S)				


OUTPUT:
output={V_C, V_M, I_C, I_M,I_S, I_CP}
