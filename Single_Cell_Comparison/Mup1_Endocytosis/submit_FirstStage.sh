#!/bin/bash
#$ -V
#$ -q mpi01.q
#$ -N FirstStage
#$ -pe openmpi144 100
#$ -o ./FirstStage.out
#$ -e ./FirstStage.err
#$ -cwd

matlab -nosplash  -r 'FirstStageNLMD; exit'
