dirname='./images';
tosave=[];

for j=1:6
    

dat=imread(sprintf('./temp/dat_%d.png',j));
pred=imread(sprintf('./temp/pred_%d.png',j));
dat=im2bw(imresize(dat,[400,400]));
pred=im2bw(imresize(pred,[400,400]));

intersectImg = dat & pred;
unionImg = pred | dat;
Jac_index=sum(intersectImg(:))/sum(unionImg(:));
tosave=cat(1,tosave,[ j, Jac_index]);
end

tosave_GTS=tosave;
% dirname='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/GTS_paper/Pub_results/Data_sim';
% 
% tosave=[];
% for j=1:length(folder_names)
% folder_name=folder_names{j};
% for i=1:5
% dat=imread(sprintf('%s/%s/images/data_%d.png',dirname,folder_name,i));
% pred=imread(sprintf('%s/%s/images/predictions_%d.png',dirname,folder_name,i));
% dat=im2bw(imresize(dat,[400,400]));
% pred=im2bw(imresize(pred,[400,400]));
% intersectImg = pred & dat;
% unionImg = pred | dat;
% Jac_index=sum(intersectImg(:))/sum(unionImg(:));
% tosave=cat(1,tosave,[label_names(j), i, Jac_index]);
% end
% end
% tosave_GTS=tosave;
% dirname='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/GTS_paper/Pub_results_PL/Data_scrambled_bio';
% dirname='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/GTS_paper/Pub_results_PL/Data_sigma';
% dirname='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/GTS_paper/Pub_results_PL/Data_scrambled3';
% dirname='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/GTS_paper/Pub_results_PL/Data_scrambled3';
% 
% % tosave=[];
% % for j=1:length(folder_names)
% % folder_name=folder_names{j};
% % for i=1:5
% % dat=imread(sprintf('%s/%s/images/data_%d.png',dirname,folder_name,i));
% % pred=imread(sprintf('%s/%s/images/predictions_%d.png',dirname,folder_name,i));
% % dat=im2bw(imresize(dat,[400,400]));
% % pred=im2bw(imresize(pred,[400,400]));
% % intersectImg = pred & dat;
% % unionImg = pred | dat;
% % Jac_index=sum(intersectImg(:))/sum(unionImg(:));
% % tosave=cat(1,tosave,[label_names(j), i, Jac_index]);
% % end
% % end
% % tosave_GTS_PL=tosave;
% 
% save('./Data_sim/comp_pred.mat','tosave_GTS','tosave_SAEM');
% %save('./Data_scrambled3/comp_pred.mat','tosave_GTS_PL','tosave_GTS','tosave_SAEM');
