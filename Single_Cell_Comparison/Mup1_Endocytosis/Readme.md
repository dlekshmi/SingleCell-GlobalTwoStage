-----------------------------------------------------------------------------------
How to reproduce the results:
-----------------------------------------------------------------------------------
Firstly, make sure that the toolboxes AMICI, and NLOPT are correctly installed:
		- AMICI: https://github.com/ICB-DCM/AMICI
		- NLOPT: https://nlopt.readthedocs.io/en/latest/ (STABLE RELEASE USED)

Then proceed by building the mex solver for the ODE model. The relevant script 
is in the folder ``Model``. This already contains the compiled model for LINUX environment, 
compiled using latest gcc compiler compatible with MATLAB2016b. 
The data used is provided in .csv format and is used for estimation. 
A conversion script from the original data (.mat, from http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004706)
to .csv is provided as an R script. 

The contents of the Model and Data folder are summarised below:
Model: 	
Models in the folder: 	  Endocytosis_model/model_endocytosis (model compiled by AMICI)
						 
	
	Model related scripts:
	ConvertModel.m : Converts .m model to executables stored in Endocytosis_model/

	 Endo_eq.m runs the model with first order sensitivities
				INPUT==> time (t), parameters (para), int_opts (integrator options)
				OUTPUT==> simulated trajectory f_t
				
	 Endo_eq_states.m:  only states are integrated, no sensitivities returned. 
					
	 var_fun.m : Variance model/ noise model used: 
			   INPUT==> noise parameters needed to define the noise mode, and the 
			   		  simulated trajectories
			   OUTPUT==> Returns the noise model (std. deviation model).
			   


Data: 	 Contains data used for estimation and scripts needed to process it
			
			Datasets: 
			- Indcell_data.mat: processed data
			- .txt: same data used in monolix format.
								   

Then, proceed by running the script run_GTS (sh run_GTS) if using an SGE cluster. 
						 run_GTS_desktop (sh run_GTS_desktop) if using a desktop. 

Modify the submits script according to the computer used.  To run it locally, the 
relevant files to modify if needed are: 
1. MultiStart_WLS.m, FirstStage.m, and SecondStage.m  to do the two stages of GTS
2. The results are stored in Pubs_Results
4. Plot_images simulates the data from the estimated model and results are stored in 
PREDICTIONS folder


!!!NOTE!!!! Please modify lines related to data input, and storing if required. 
Make sure to change the paths of the installed toolboxes and the .mat files that are read 
in in the second stage. 
Export the NLOPT libraries correctly. 

-----------------------------------------------------------------------------------
Folders and their Contents
-----------------------------------------------------------------------------------

1. Auxilliary: Contains help functions/ assisting functions
				- get_stableinverse.m: 
					Description: Function that gets the inverse of a matrix. First Cholesky is tried. 
					If that fails, and the condition number is zero, then a small number 
					is added to the diagonal of the matrix. Then, the 'inv', based 
					on LU decomposition is used.  A warning is issued if Cholesky decomposition fails. 
					INPUT==> Matrix to invert
					OUTPUT==>Inverted Matrix, addition error indicator if Cholesky failed. 
					
				- getgradient_odesd.m:
						Gets sensitivity of the ODE model and calculates the gradients of the response
 						with respect to the kinetic parameters. Note we are not caring about the parameters
 						that are assumed fixed. Then the sensitivity is used to get the FIM/ HESSIAN. 
  						y=exp(-gp*tau)*f_t_tau, then based on chain rule
 						dy_dp=exp(-gp*tau)*df_dp+d{exp(gp*tau)}*f
   						or use gen_Sens_y (in Model folder) to symbolically check. 

   						Tools needed: 
        						 a) ODE SOLVER : ODESD

  						INPUT==> t: Time values in experiment,
		  						p0: kinetic parameters
 		  						ehat: Noise parameters, theta in the model
								d: data
 								sigma2: sigma^2
								int_opts: Options passed to integrator, structure.

  						OUTPUT==> DYDP: the gradients wrt parameters
            					d2ydp2: second order gradients
			 					noisefuns:	the 'g' in manuscript
								 C_inv:		the FIM
           						 Hessian: The Hessin using second order sensitivities.
            					f_t: observation 
            					gradient_states: dxdp (sensitivities of the state 2 wrt
            					parameters)


			
				- get_secondordgradient.m  (NOT used in manuscript)
				     Description: Gets sensitivity of the model output and calculates the 
					gradients of the response with respect to the kinetic parameters. 
					Then the sensitivity is used to get the FIM (C_inv) AND HESSIAN (C_INV_HESSIAN). 
					
					Tools needed: 
        						 a) ODE SOLVER : CVODES/ AMICI
        						 
				  INPUT==> t: Time values in experiment,
						   para: kinetic parameters
						ydata: data
 						noise_params: additive and multiplicative constant

   						
				OUTPUT==> f_t: observation 
							dydp, dy2dpdq: first and second order sensitivies of observation wrt parameters
							us: input state
							 H1, H2: C_inv/ C_inv_hessian
							residuals: residuals
							dx2dpdq: second order sensitivies of states wrt parameters
					
					
2. FirstStage_objs: Objective functions used in First stage of the pipeline: GLS-Pool
			- OLS.m: Standard OLS (Ordinary Least Squares) method
			  	INPUT==>  t: Time values in experiment,
						kinetic_params: log kinetic parameters
						noise_params: Noise parameters, theta in the model
						d: data
						int_opts: Options passed to integrator, structure.


			 	OUTPUT==> res: The objective function ordinary least squares 
			 				(squared residuals)
			 				
			- WOLS.m: Does weighted OLS
				INPUT==> t: Time values in experiment,
			  			 kinetic_params: log kinetic parameters
	 		    		 weights: Weights calculated based on theta_hat & last estimates
				           			of kinetic_params using var_fun (g in manuscript)
						d: data
	           			int_opts: Options passed to integrator, structure.


			  	OUTPUT==> res: The objective function weighted ordinary least squares. 

			
			- get_AR_gm: Gets the pooled AR (absolute residuals) likelihood. Implementation is based on 
						 https://www.jstor.org/stable/2532602?seq=1#page_scan_tab_contents 
  						 Please see the  appendix of the paper
				 INPUT==> sim_struct: structure containing the simulated data for all
                						cells at their estimated kinetic parameters from
                       					last iteration
           			   	noise_params: Noise parameters, theta in the model
						data: data.Y
           				indexes: cell IDs (so far only those with all data)


				  OUTPUT==> AR: objective function (pooled AR, in log)
				  


		- WLS_gm: Used by get_AR_gm and get_PL_gm.m. Generates the things needed to 
		compute the AR/ PL objective functions
   
 			 	INPUT==> 	f_t: simulated response of the cell
 		   				noise_params: Noise parameters, theta in the model
		   				d: data

  				OUTPUT==>   res: residuals
			  			h:  noise model per cell. 
 			  			gm: log geometric mean noise function of the cell

4. SecondStage_objs: Objective functions used in the SecondStage
			- LGTS_EM.m: Second Stage EM algorithm based on equations 5.12, 5.13 and 5.14 Chapter 5 of
						 Nonlinear Models for Repeated Measurement Data, Chapman & Hall, London
						 1996 Reprint edition, Davidian and Giltinan .
 				INPUT==> C_inv: FIM for all the estimates, 3D array
			 			betahat0: initial parameters of the mean
			 			Dhat0: initial parameters of the covariance
			 			beta_ihat: individual estimates from the first stage,
 			 			conv: structure containing information regarding convergence crietria, 
								tolerance, maximum iteration

				OUTPUT==> Result: Structure with following entries: 
			              	PopCovar: (D) Cov. matrix 
               			  	eBayesEsts: (beta_i) ebayes individual estimates
                          	PopMean: (betahat) mean parameter estimate
                          	Mean_convergence: (betadiffs) convergence of parameter vector: differences
               			  	Var_convergence: (diffs) convergence of D: differences
               			  	Var_allvalues: (allDs) Values of entries of D (covariance) at each
                               iteration
						    Mean_allvalues: (allbs)Values of entries of betahat (mean) at each
                               iteration		
		
				
4. Model/ Data: Contains things needed to run the model. See last section on reproducing the 
		  study. 
		  

5. SAEM_FILES: Contains things to run SAEM in MONOLIX. Look inside for implementation details. 	

6. Spline_fits: gets intial estimates of the error models. 

-----------------------------------------------------------------------------------
Main scripts
-----------------------------------------------------------------------------------
First the model should be converted using the scripts in the Model/ folder 
Then, using Spline_fits folder, generate initial values for the error parameters

Then submit scripts are provided to execute the following in order. 
1. MultiStart_WLS.m/_100WLS.m:
2. FirstStage_NLMD.m:
3. SecondStage.m
4. Plot_images.m 

resubmit_jobs.m: script that checks if all runs in the embarrassingly parallel array has run, 
				 else we resubmit.
				 
-------------------------------------------------------------------------------------
bash scripts
-------------------------------------------------------------------------------------
	These scripts help to execute the code in the SGE grid. 
	and can be submited using qsub
	submit_multistart/multistart100.sh:  to run multistarts. 
	submit_FirstStage : to run First Stage 	
	submit_SecondStage: to run the Second Stage	
	submit_plot		  : to run Second Stage

-------------------------------------------------------------------------------------
Pipeline Script
-------------------------------------------------------------------------------------
First run the Spline_fits/Spline_fits.m and get_bounds/get_bounds.m
GTS (Multi_start+ First+ Second Stage + plotting prediction) can be executed in a pipeline fashion using:
	sh run_GTS
	Read the comments in script run_GTS before executing, and change relevant paths. 
	
Some .err and .out will be created and will contain any execution information. 
