function []= FirstStageNLMD()
%% Run interactively from MATLAB with cluster parallelization. 
%%%------------------------------------------------------------------------
%
%   function FirstStage.m
%   Implements the First stage for the analysis of the data from Llamosi et al, 2016. Reads in 
%   the data used in Data/ and saves the results in output folder.
%   If using an SGE (Sun Grid Engine) cluster, this can be run using the accompanying .sh script
%       qsub submit_FirstStage.sh
%   The resulting mat files store a lot of information, the estimates, function values, return code,
%   data and model simulation for each sell in a structure 'GLS_params'. Followed by information on the convergence of parameters, CPU time 'CPU_end' and 
%   noise parameters ('theta_hat', 'sigma2') and the individual FIMs 'C_inv'. 
%   Tools needed: 
        % a) ODE SOLVER
        % b) Optimizer
%   Funtions, objective functions used: 
        % a) multi start estimates
        % b) WLS
        % c) POOLED GLS 
        % d) Model integration, and evaluating the error model
        % e) Auxilliary functions to calculate the fisher information matrix.      
%   
%   Lines that have to modified:
%       % a) the paths to ODE integrator, and Optimizer. 
        % b) Output directory
        % c) parallellization options

%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------

% parallelization, define cluster profile, Can also be done with local profile
addpath('/usr/local/stelling/grid/matlab')
myCluster = parcluster('local');
myCluster.NumWorkers = 100;
parpool(myCluster,myCluster.NumWorkers);

%%
% First add the paths in the master node
addpath('/usr/local/stelling/grid/matlab')
% Add optimizer paths
addpath(genpath('/usr/local/stelling/el6/nlopt-2.4.2/lib'))
addpath(genpath('/usr/local/stelling/el6/nlopt-2.4.2/matlab'))
addpath(genpath('/usr/local/stelling/el6/nlopt-2.4.2/include'))
loadlibrary('libnlopt.so','nlopt.h');
% Paths related to First Stage
addpath(genpath('./Model')) 
addpath(genpath('~/Repos/AMICI'))
addpath(genpath('./Auxilliary')) 
addpath(genpath('./FirstStage_objs')) 

% add the paths in the workers, if not using local parpool but rather a cluster
% else comment out. 
parfor i=1:myCluster.NumWorkers 
% give the name and directory 
addpath('/usr/local/stelling/grid/matlab')
addpath(genpath('/usr/local/stelling/el6/nlopt-2.4.2/lib'))
addpath(genpath('/usr/local/stelling/el6/nlopt-2.4.2/matlab'))
addpath(genpath('/usr/local/stelling/el6/nlopt-2.4.2/include'))
loadlibrary('libnlopt.so','nlopt.h');
addpath(genpath('./Model')) 
addpath(genpath('~/Repos/AMICI'))
addpath(genpath('./Auxilliary')) 
addpath(genpath('./FirstStage_objs')) 
end


%% Read the individual data
t1=tic();
% directory where the results of the multi starts are stored. 
dirname='./MultiStart_Results/Multistart_ResultsNLMD_100/'; 
files=dir(dirname);

tmp_params=struct;
MS_comp_times=[];
% concatenate structures 
for i=1:(length(files)-2)
load(sprintf('%s/%s',dirname,files(i+2).name))
f = fieldnames(MS_params);
    tmp_params(i).params =  MS_params.params;
    tmp_params(i).observables = MS_params.observables;
    tmp_params(i).ydata =  MS_params.ydata;
    tmp_params(i).time= MS_params.time;
    tmp_params(i).err =  MS_params.err;
    tmp_params(i).cellID= MS_params.cellID;
    tmp_params(i).CVODE_flag=MS_params.CVODE_flag;
    MS_comp_times=cat(1,MS_comp_times,comp_time);
end

spmd, cpu0 = cputime(); end

%% load noise parameters
load('Spline_fits/noise_bounds.mat')
int_opts=struct;
int_opts.atol=1e-6;
int_opts.rtol=1e-3;

tol_thetahat=1e-5;
tol_betahat=1e-5;
maxeval_betahat=1e5;
maxiter=5;                         % maximum iteration in GLS-POOL, usually things converge very fast given the amount of data. 
betai00=cat(1,tmp_params.params);  % initial estimate 
niter=1;                           % iteration counter in GLS-POOL
conv=100;                          % initial value for convergence criteria
conv_tol=0.01;
nids=length(tmp_params);
%%
theta_00=log(tmp_params(1).err);    % initial estimate of the error 

% convergence criteria
 while niter<maxiter
% while max(conv(:))>conv_tol
% Calculate optimal theta
theta_hat=log(zeros(6,2));

%For each observable pass data to AR
Arg_to_pass=struct();
for kk=1:6 
    all_ydata=cat(1,tmp_params.ydata);
    all_fits=cat(1,tmp_params.observables);
	Arg_to_pass.observables=all_fits(:,kk);
	Arg_to_pass.ydata=all_ydata(:,kk);
    opt_theta=struct();
    if kk==1||kk==2 % not for volumes only Additive noise is used. theta_1 is zero. 
        opt_theta.upper_bounds=log(theta0s_bounds(kk,3));
        opt_theta.lower_bounds=log(theta0s_bounds(kk,1));
        fun=@(pp) get_AR_gm(Arg_to_pass,[pp,log(0)]);
    else
    opt_theta.lowerbounds=log([theta0s_bounds(kk,1),theta1s_bounds(kk,1)]);
    opt_theta.upper_bounds=log([theta0s_bounds(kk,3),theta1s_bounds(kk,3)]);
    fun=@(pp) get_AR_gm(Arg_to_pass,pp);
    end
    opt_theta.algorithm = NLOPT_LN_NELDERMEAD; % unconstrained estimation lead to massive noise in noise estimate of spots
    opt_theta.min_objective = fun;

    % uncomment if you want to estimate with default parameters. Can take long since it is very stringent. 
    opt_theta.ftol_rel = tol_thetahat;
    opt_theta.maxeval=maxeval_betahat;

    if kk==1||kk==2
    [theta_hat(kk,1),ff,~]=nlopt_optimize(opt_theta,theta_00(kk,1));
    else
    [theta_hat(kk,:),ff,~]=nlopt_optimize(opt_theta,theta_00(kk,:));
    end
end

disp(theta_hat)
% With new theta_hat estimate the GLS Again... But now only with the previous starting point
% calculating the sigma2
% get the new weights
[all_theta0s,all_theta1s]=get_sigma(tmp_params,theta_hat);
   
betai_hat=zeros(size(betai00)); 
retcode=zeros(size(betai00,1));
fval=zeros(size(betai00,1));

parfor i = 1:nids 
% construct weights based on previous estimates
weights=var_fun(all_fits,[all_theta0s,all_theta1s]);

% define problem structure 
problem=struct();
problem.x_L=[log([10^-9,10^-9,10^-9,10^-9,10^-9,10^-9,1,1,10^-9,10^-6,10^-6,1,1,1,800,800]),1e-10,10^-10];
problem.x_U=[log([100,100,100,100,10,1,200,200,0.01,10^-1,10^-1,1000,500,1000,1e8,1e8]),3,3];
fmin_objWOLS = @(pp) WOLS(pp',tmp_params(i).time,weights,tmp_params(i).ydata,int_opts);

% define  optimization structure 
optim=struct();
optim.algorithm = NLOPT_LN_NELDERMEAD;
optim.ftol_rel = tol_betahat;
optim.xtol_rel=tol_betahat;
optim.maxeval= maxeval_betahat;
optim.min_objective=fmin_objWOLS;
problem.x_0=betai00(i,:);
optim.lower_bounds=problem.x_L;
optim.upper_bounds=problem.x_U;
[betai_hat(i,:), fval(i),retcode(i)] = nlopt_optimize(optim, problem.x_0);

end

% Convergence on theta_hat, reset theta_00, update the iteration counter. 
conv=abs(theta_00-theta_hat);
theta_00=theta_hat;
betai00=betai_hat;
niter=niter+1;

% get the simulated trajectories at the optimum
parfor i=1:nids
[f,~,simulated_tmp,~,~]=Endo_eq(betai_hat(i,:)',tmp_params(i).time,[],[],int_opts);
tmp_params(i).observables=simulated_tmp;
tmp_params(i).CVODE_flag=f;            % flag showing was there an issue in integrating at optimum?
end

end

%%
tmp_params2=struct();
for i=1:nids
% Simulate  at optimal trajectory for the ith cell 
x=betai_hat(i,:); 

% Prepare for the next
% Save the final stuff  
tmp_params2(i).params = betai_hat(i,:);     % kinetic parameters
tmp_params2(i).retcode=retcode(i);          % return code of optimization
tmp_params2(i).fval=fval(i);                % function value WOLS
tmp_params2(i).observables =tmp_params(i).observables;      % simulated values
tmp_params2(i).ydata = tmp_params(i).ydata; % data
tmp_params2(i).time=tmp_params(i).time; % time of measurements
tmp_params2(i).err = exp(theta_hat);    % theta_hat
tmp_params2(i).cellID = tmp_params(i).cellID; % cell ID
tmp_params2(i).dof=(sum(~isnan(tmp_params(i).ydata))); % degree of freedom
tmp_params2(i).CVODE_flag=tmp_params(i).CVODE_flag;            % flag showing was there an issue in integrating at optimum?
tmp_params2(i).res=zeros(1,6); % to store residuals
tmp_params2(i).eij=zeros(size(tmp_params2(i).ydata)); % to store absolute residuals

end 

% calculating the sigma2, and the scaling eta in the AR likelihood. 
[all_theta0s,all_theta1s, sigmasq,scaling, tmp_params2]=get_sigma(tmp_params2,theta_hat);


%% get the FIM and final result
for i=1:nids
% Simulate 
x=betai_hat(i,:); 
[f,~,simulated,sens_observations,FIM]=Endo_eq(x',tmp_params2(i).time,tmp_params2(i).ydata,[all_theta0s,all_theta1s],int_opts);

% Save the final estimates!
tmp_params2(i).FIM =FIM;
tmp_params2(i).err = [all_theta0s,all_theta1s];
end  

% get time elapsed
t2=toc(t1);
time_elapsed=t2;
GLS_params=tmp_params2;
spmd, cpu_end = cputime() - cpu0, end
spmd, cptime = gplus(cpu_end), end
cpu_end = cell2mat(cpu_end(: ));
cptime = cell2mat(cptime(: ));

%% save 
mkdir('Pub_Results')
save('Pub_Results/FirstStageNLMD100_iter5.mat','cptime', 'theta_hat','time_elapsed','GLS_params','scaling','sigmasq','all_theta0s','all_theta1s','niter','conv')

%% delete parpool
delete(gcp)
