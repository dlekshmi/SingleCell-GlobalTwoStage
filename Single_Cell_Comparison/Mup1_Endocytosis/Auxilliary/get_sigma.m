function [all_theta0s,all_theta1s,sigmasq, scaling,result] =get_sigma(result, theta_hat)
% Helper script that calculates the final sigmasqfrom the fitted
% data.The thetas after taking account of the sigmasq followed by the
% sigmaq, the scaling eta (that appears in the AR likelihood function)
% and result, the structure containing the residuals. 

for i=1:length(result)
if(result(i).CVODE_flag~=1)
	for kk=1:size(theta_hat,1)
g_model=exp(theta_hat(kk,1))+result(i).observables(:,kk).*exp(theta_hat(kk,2));
result(i).res(kk)=nansum(((result(i).ydata(:,kk)-result(i).observables(:,kk))./g_model).^2);
result(i).abs=nansum((abs(result(i).ydata(:,kk)-result(i).observables(:,kk))./g_model));
   end
else
result(i).res(k)=NaN;
end
result(i).sigmasqcell= result(i).res;
end

all_sigmas=cat(1,result.sigmasqcell);
all_dof=cat(1,result.dof);
sigmasq=sum(all_sigmas)./sum(all_dof);

all_theta0s=sqrt(sigmasq)'.*exp(theta_hat(:,1));
all_theta1s=sqrt(sigmasq)'.*exp(theta_hat(:,2));

% Get the scaling parameter
for i=1:length(result)
if(result(i).CVODE_flag~=1)
	for kk=1:size(theta_hat,1)
g_model=exp(theta_hat(kk,1))+result(i).observables(:,kk).*exp(theta_hat(kk,2));
result(i).eij(:,kk)=abs((result(i).ydata(:,kk)-result(i).observables(:,kk))./(sqrt(sigmasq(kk)).*g_model));
	end
end
end
E_eij=nanmean(cat(1,result.eij));
scaling=E_eij.*sqrt(sigmasq);    

end
