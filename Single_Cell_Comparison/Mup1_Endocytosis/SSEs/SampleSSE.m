clear all


%% Calculates the Weighted SSEs
% Add paths
addpath(genpath('/usr/local/stelling/grid/matlab'))
addpath(genpath('../Model'))
addpath(genpath('~/Repos/AMICI'))
addpath(genpath('../Auxilliary'))

% For the ith cell
load('../SAEM_FILES/Mup1_7/results.mat')
SAEM_IDS=IDs;

% data and cell information
load('../Pub_Results/FirstStageNLMD50_iter5.mat')

%% Mup1 -SAEM 3
ids=1:1907;
cell_ids=zeros(1,1907);
SSE_SAEM3=zeros(1,1907);

load('../SAEM_FILES/Mup1_3/Mup1_3.mat')
betahat=struct2array(betahat);
all_theta0s=err(:,1);
all_theta1s=err(:,2);

int_opts.atol=1e-6;
int_opts.rtol=1e-4;
Res=zeros(1907,6);
%
for id=1:1907
    
    
    % Then calulate the SSEs for the cell
    time=GLS_params(id).time;
    
    % ensure that the cell and the data are correct
    ydata_response=GLS_params(id).ydata;
    cell_id=GLS_params(id).cellID;
    
    if cell_id==str2num(cell2mat(IDs(cell_id)))
        [ind_lls,Res(id,:)]=ind_SSEs(betahati(cell_id,:)',time,[all_theta0s,all_theta1s],ydata_response,int_opts);
    else
        disp('Error')
    end
    
    SSE_SAEM3(id)=(ind_lls);
end
Res_SAEM3=Res;

%% Mup1 -SAEM 1
SSE_SAEM1=zeros(1,1907);
load('../SAEM_FILES/Mup1_1/Mup1_1.mat')
betahat=struct2array(betahat);
all_theta0s=err(:,1);
all_theta1s=err(:,2);

int_opts.atol=1e-6;
int_opts.rtol=1e-4;
Res=zeros(1907,6);
%
for id=1:1907
    

    
    % Then calulate the likelihood for the cell
    time=GLS_params(id).time;
    ydata_response=GLS_params(id).ydata;
    
    cell_id=GLS_params(id).cellID;
    if cell_id==str2num(cell2mat(IDs(cell_id)))
        
        [ind_lls,Res(id,:)]=ind_SSEs(betahati(cell_id,:)',time,[all_theta0s,all_theta1s],ydata_response,int_opts);
        
    else
        disp('Error')
    end
    
    SSE_SAEM1(id)=(ind_lls);
end
Res_SAEM1=Res;

%% Mup1 -SAEM 4
SSE_SAEM4=zeros(1,1907);
load('../SAEM_FILES/Mup1_4/Mup1_4.mat')
betahat=struct2array(betahat);
all_theta0s=err(:,1);
all_theta1s=err(:,2);

int_opts.atol=1e-6;
int_opts.rtol=1e-4;
Res=zeros(1907,6);
%
for id=1:1907
    

    % Then calulate the likelihood for the cell
    time=GLS_params(id).time;
    ydata_response=GLS_params(id).ydata;
    
    cell_id=GLS_params(id).cellID;
    if cell_id==str2num(cell2mat(IDs(cell_id)))
        
        [ind_lls,Res(id,:)]=ind_SSEs(betahati(cell_id,:)',time,[all_theta0s,all_theta1s],ydata_response,int_opts);
        
    else
        disp('Error')
    end
    
    SSE_SAEM4(id)=(ind_lls);
end
Res_SAEM4=Res;

%%  Mup1 -SAEM 2
SSE_SAEM2=zeros(1,1907);
load('../SAEM_FILES/Mup1_2/Mup1_2.mat')
betahat=struct2array(betahat);
all_theta0s=err(:,1);
all_theta1s=err(:,2);

int_opts.atol=1e-6;
int_opts.rtol=1e-4;
Res=zeros(1907,6);
%
for id=1:1907
    
 
    % Then calulate the likelihood for the cell
    time=GLS_params(id).time;
    ydata_response=GLS_params(id).ydata;
    
    cell_id=GLS_params(id).cellID;
    if cell_id==str2num(cell2mat(IDs(cell_id)))

        [ind_lls,Res(id,:)]=ind_SSEs(betahati(cell_id,:)',time,[all_theta0s,all_theta1s],ydata_response,int_opts);
 
    else
        disp('Error')
end

SSE_SAEM2(id)=(ind_lls);
end
Res_SAEM2=Res;

%% %% Mup1 -SAEM 3
SSE_SAEM=zeros(1,1907);
load('../SAEM_FILES/Mup1_7/Mup1_GTS_new.mat')
betahat=struct2array(betahat);
all_theta0s=err(:,1);
all_theta1s=err(:,2);

int_opts.atol=1e-6;
int_opts.rtol=1e-4;
Res=zeros(1907,6);
%
for id=1:1907
    
    num_samples=1;
    
    % Then calulate the likelihood for the cell
    time=GLS_params(id).time;
    ydata_response=GLS_params(id).ydata;
    
    cell_id=GLS_params(id).cellID;
    if cell_id==str2num(cell2mat(IDs(cell_id)))
        
        
        % Sample M samples of the individual estimates.
        ind_lls=zeros(1,num_samples);
        
        
        [ind_lls(ns),Res(id,:)]=ind_SSEs(betahati(cell_id,:)',time,[all_theta0s,all_theta1s],ydata_response,int_opts);
        
    else
        disp('Error')
    end
    
    SSE_SAEM(id)=(ind_lls);
end
Res_SAEM=Res;

%% GTS: 50 
load('../Pub_Results_new/NLMD50_5.mat')
load('../Pub_Results_new/FirstStageNLMD50_iter5.mat')
SSE_GLS_50=zeros(1,1907);
SSE_GTS_50=zeros(1,1907);
for id=1:1907
    
    
    
    % Then calulate the likelihood for the cell
    time=GLS_params(id).time;
    ydata_response=GLS_params(id).ydata;
    cell_id=GLS_params(id).cellID;
    
    
    ind_lls_shrunk=ind_SSEs(betahati(id,:)',time,[all_theta0s,all_theta1s],ydata_response,int_opts);
    [ind_lls, Res(id,:)]=ind_SSEs(GLS_params(id).params',time,[all_theta0s,all_theta1s],ydata_response,int_opts);
    
    
    
    SSE_GLS_50(id)=(ind_lls);
    SSE_GTS_50(id)=ind_lls_shrunk;
end
Res_GLS=Res;

%% GTS 100
load('../Pub_Results_new/NLMD100_5.mat')
load('../Pub_Results_new/FirstStageNLMD100_iter5.mat')
SSE_GLS_100=zeros(1,1907);
SSE_GTS_100=zeros(1,1907);
dps=zeros(1907,6);
for id=1:1907
    

    
    % Then calulate the likelihood for the cell
    time=GLS_params(id).time;
    ydata_response=GLS_params(id).ydata;
    cell_id=GLS_params(id).cellID;
    
    
    
    ind_lls_shrunk=ind_likelihoods(betahati(id,:)',time,[all_theta0s,all_theta1s],ydata_response,int_opts);
    [ind_lls, Res(id,:)]=ind_likelihoods(GLS_params(id).params',time,[all_theta0s,all_theta1s],ydata_response,int_opts);
    
    
    dps(id,:)=GLS_params(id).dof;
    SSE_GLS_100(id)=(ind_lls);
    SSE_GTS_100(id)=ind_lls_shrunk;
end
Res_GLS_100=Res;

data_points=dps;
save('SSE_wls.mat','SSE_SAEM','SSE_SAEM2','SSE_SAEM3','SSE_SAEM4','Res_SAEM1','Res_SAEM2','Res_SAEM3','Res_SAEM4','SSE_GLS_50','SSE_GTS_50','Res_GLS','SSE_GLS_100','SSE_GTS_100','Res_GLS_100','Res_SAEM','data_points')