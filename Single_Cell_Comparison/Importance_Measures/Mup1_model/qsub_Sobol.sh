#!/bin/bash
#$ -V
#$ -q sc02.q
#$ -cwd
#$ -l mem_free=4g
#$ -pe make 10
#$ -M 'lekshmi.dharmarajan@bsse.ethz.ch'

for i in $(seq $SGE_TASK_STEPSIZE);  do
 index=$(($SGE_TASK_ID+$i))
 echo "Submitted $index"
 matlab -nosplash  -r "Sobol_endo($var,$(($SGE_TASK_ID+$i))-1,$run); exit"
done
