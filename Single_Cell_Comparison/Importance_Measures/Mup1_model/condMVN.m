function [cMu,cVar]= condMVN(Mean, Sigma, dependent_ind, given_ind, X_given)
% Returns conditional mean and variance of 
% X[dependent.ind] | X[given.ind] = X.given
% where X is multivariateNormal
 
B= Sigma(dependent_ind, dependent_ind);
C =Sigma(dependent_ind, given_ind);
D = Sigma(given_ind, given_ind);
CDinv =C * inv(D);
cMu = Mean(dependent_ind) + CDinv*(X_given - Mean(given_ind));
cVar = B - CDinv * (C)';

 end


