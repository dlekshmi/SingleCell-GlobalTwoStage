function outputs = get_outputs(params,time,int_tol,observation_id)
outputs=zeros(size(params,1),length(time));
for i=1:size(params,1) % parallelize this loop... The method depends on how well we accurately had
                       % sampled. 
[f,T,obs_y,~,~]=Endo_eq_states(params(i,:)',[0,time],[],[],int_tol);

for j=observation_id
    if any(obs_y<0)
        disp('negative states')
    end
    if isempty(obs_y) % integration had failed.
        c1=NaN;
    else
c1=obs_y(2:end,j);
    end
end
outputs(i,:)=c1';

end