function[ V_Xi, flag]= get_var_condExp(given_id,num_samples,p_joint,betahat, Dhat, model_opts,myseed)
%%
% given_id: id pf the parameter n which the Y is conditioned on
% num_samples: number of samples
% p_joint: Samples from the joint probability distribution
% betahat: Mean of the full distribution
% Dhat" Variation of the full parameter distribution
% model_opts: Options related to the model integration: 
    % time: time we are interested in
    % int_tol: tolerance
    % observation_id: Id of what output we are interested in. 
    
% Returns: V_Xi{E_X_i[Y|Xi=xi]}, where X_i is everything except X. 

%%
% Get the conditional expectation 
flag=zeros(1,num_samples);
Exp_con_Y=zeros(1,num_samples);
time=model_opts.time;
int_tol=model_opts.int_tol;
observation_id=model_opts.observation_id;   
dependent_id=setdiff(1:length(betahat),given_id);

parfor i=1:num_samples                  
x_given=p_joint(i,given_id); % j th sample

% Generate the conditional mean and variance
[cMu,cVar]= condMVN(betahat, Dhat, dependent_id, given_id, x_given')
rng(i+num_samples*myseed) % different seed for each worker.
try 
p_deps=mvnrnd(cMu,cVar,num_samples);
p_cond=zeros(num_samples,length(betahat));
p_cond(:,given_id)=repmat(x_given,num_samples,1);
p_cond(:,dependent_id)=p_deps;

con_Y=get_outputs(p_cond,time,int_tol,observation_id);
Exp_con_Y(i)=nanmean(con_Y);
catch
    try
    cVar=cVar+eye(size(cVar)).*1e-5;
    p_deps=mvnrnd(cMu,cVar,num_samples);
    p_cond=zeros(num_samples,length(betahat));
    p_cond(:,given_id)=repmat(x_given,num_samples,1);
    p_cond(:,dependent_id)=p_deps;

    con_Y=get_outputs(p_cond,time,int_tol,observation_id);
    Exp_con_Y(i)=nanmean(con_Y);
    catch
    disp(cVar)
    disp(cMu)
    Exp_con_Y(i)=NaN;
    flag(i)=1;
    end

end
% p_cond=zeros(num_samples,length(betahat));
% p_cond(:,given_id)=repmat(x_given,num_samples,1);
% p_cond(:,dependent_id)=p_deps; 
% 
% con_Y=get_outputs(p_cond,time,int_tol,observation_id);
% Exp_con_Y(i)=nanmean(con_Y);
end

% get variation of condiotonal expectation
V_Xi=nanvar(Exp_con_Y);
flag=sum(flag);
end
