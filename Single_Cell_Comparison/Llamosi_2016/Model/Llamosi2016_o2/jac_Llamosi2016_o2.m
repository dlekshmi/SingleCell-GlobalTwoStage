function [Jfx,Jgx,Jfp,Jgp] = jac_Llamosi2016_o2(varargin)

    time = varargin{1};
    x = varargin{2};

    if nargout > 1
        % odeSD
        if nargin > 3
            p = varargin{4};
            
        else
            p = [4.47;0.008;0.008;30];
        end
        if nargout > 1
            f = varargin{3};
        end
    else
        % MATLAB ODE integrators
        if nargin > 2
            p = varargin{3};
            
        else
            p = [4.47;0.008;0.008;30];
        end
    end


    if nargout<1
        error('Too many output arguments');
    end

    Jfx = zeros(12, 12);
	Jfx(1) = -p(2);
	Jfx(2) = 947/1000;
	Jfx(7) = -1;
	Jfx(14) = -p(3);
	Jfx(23) = -1;
	Jfx(25) = p(1);
	Jfx(27) = -369/400;
	Jfx(28) = 1;
	Jfx(40) = -p(2);
	Jfx(41) = 947/1000;
	Jfx(53) = -p(3);
	Jfx(64) = p(1);
	Jfx(66) = -369/400;
	Jfx(79) = -p(2);
	Jfx(80) = 947/1000;
	Jfx(92) = -p(3);
	Jfx(103) = p(1);
	Jfx(105) = -369/400;
	Jfx(118) = -p(2);
	Jfx(119) = 947/1000;
	Jfx(131) = -p(3);
	Jfx(142) = p(1);
	Jfx(144) = -369/400;


    if nargout == 1
        return
    end

    Jgx = Jfx*Jfx;
	

    if nargout == 2
        return
    end

    Jfp = zeros(12, 4);
	Jfp(1) = x(3);
	Jfp(4) = x(6);
	Jfp(7) = x(9);
	Jfp(10) = x(12);
	Jfp(13) = -x(1);
	Jfp(16) = -x(4);
	Jfp(19) = -x(7);
	Jfp(22) = -x(10);
	Jfp(26) = -x(2);
	Jfp(29) = -x(5);
	Jfp(32) = -x(8);
	Jfp(35) = -x(11);


    if nargout == 3
        return
    end

    Jgp = Jfx*Jfp;
	Jgp(1) = Jgp(1) + f(3);
	Jgp(4) = Jgp(4) + f(6);
	Jgp(7) = Jgp(7) + f(9);
	Jgp(10) = Jgp(10) + f(12);
	Jgp(13) = Jgp(13) + -f(1);
	Jgp(16) = Jgp(16) + -f(4);
	Jgp(19) = Jgp(19) + -f(7);
	Jgp(22) = Jgp(22) + -f(10);
	Jgp(26) = Jgp(26) + -f(2);
	Jgp(29) = Jgp(29) + -f(5);
	Jgp(32) = Jgp(32) + -f(8);
	Jgp(35) = Jgp(35) + -f(11);


end

