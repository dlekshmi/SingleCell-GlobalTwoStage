/******************************
* This file is part of odeSD. *
******************************/ 

/* Standard header files. */ 
#include <stdlib.h> 
#include <string.h> 
#include <math.h> 

/* Library header files. */ 
#include <lapack.h> 
#include <blas.h>
#include <mex.h> 

/* Initial conditions. */ 
const int Llamosi2016_o2_Nstates = 12; 
const int Llamosi2016_o2_Nparams = 4;
mwSignedIndex mwLlamosi2016_o2_Nstates = 12;
mwSignedIndex mwLlamosi2016_o2_Nparams = 4;
double done = 1.0; 
const double Llamosi2016_o2_x0[12] = {0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.};
const double Llamosi2016_o2_p[4] = {4., 0.008, 0.008, 3.e+01};
const char *Llamosi2016_o2_name = "Llamosi2016_o2";

/*****************************************
* Jacobian Right-hand side for ODE model *
*****************************************/

int Llamosi2016_o2_jac ( double time , const double *x , const double *f , const double *p , void *varargin , double *J , double *dJ , double *dfdp , double *d2fdtdp ) 
{ 
	/*******************************
	* STATE JACOBIAN EQUATIONS (J) *
	*******************************/ 
	
	if ( J != NULL || dJ != NULL || d2fdtdp != NULL ) 
	{ 
		memset( J , 0.0, sizeof(double) * Llamosi2016_o2_Nstates * Llamosi2016_o2_Nstates );
		
		J[0] = p[1]*-1.0;
		J[1] = 9.47E-1;
		J[6] = -1.0;
		J[13] = p[2]*-1.0;
		J[22] = -1.0;
		J[24] = p[0];
		J[26] = -9.225E-1;
		J[27] = 1.0;
		J[39] = p[1]*-1.0;
		J[40] = 9.47E-1;
		J[52] = p[2]*-1.0;
		J[63] = p[0];
		J[65] = -9.225E-1;
		J[78] = p[1]*-1.0;
		J[79] = 9.47E-1;
		J[91] = p[2]*-1.0;
		J[102] = p[0];
		J[104] = -9.225E-1;
		J[117] = p[1]*-1.0;
		J[118] = 9.47E-1;
		J[130] = p[2]*-1.0;
		J[141] = p[0];
		J[143] = -9.225E-1;

	} 

	/*********************************************
	* JACOBIAN OF 2nd DERIVATIVE EQUATIONS (Jgx) *
	*********************************************/

	if ( dJ != NULL )
    { 
		memset( dJ , 0.0, sizeof(double) * Llamosi2016_o2_Nstates * Llamosi2016_o2_Nstates ); 
        


		dgemm("N", "N", &mwLlamosi2016_o2_Nstates, &mwLlamosi2016_o2_Nstates, &mwLlamosi2016_o2_Nstates, &done, J, &mwLlamosi2016_o2_Nstates, J, &mwLlamosi2016_o2_Nstates, &done, dJ, &mwLlamosi2016_o2_Nstates);  
	 } 


    /************************************** 
    * PARAMETER JACOBIAN EQUATIONS (dfdp) * 
    **************************************/

    if ( dfdp != NULL || d2fdtdp != NULL )
    { 
        memset( dfdp , 0.0, sizeof(double) * Llamosi2016_o2_Nstates * Llamosi2016_o2_Nparams ); 
		dfdp[0] = x[2];
		dfdp[3] = x[5];
		dfdp[6] = x[8];
		dfdp[9] = x[11];
		dfdp[12] = x[0]*-1.0;
		dfdp[15] = x[3]*-1.0;
		dfdp[18] = x[6]*-1.0;
		dfdp[21] = x[9]*-1.0;
		dfdp[25] = x[1]*-1.0;
		dfdp[28] = x[4]*-1.0;
		dfdp[31] = x[7]*-1.0;
		dfdp[34] = x[10]*-1.0;

    } 

    /****************************************************
    * 2nd derivative PARAMETER JACOBIAN EQUATIONS (Jgp) *
    ****************************************************/ 

    if ( d2fdtdp != NULL )
    { 
        memset( d2fdtdp , 0.0, sizeof(double) * Llamosi2016_o2_Nstates * Llamosi2016_o2_Nparams ); 

		d2fdtdp[0] += f[2];
		d2fdtdp[3] += f[5];
		d2fdtdp[6] += f[8];
		d2fdtdp[9] += f[11];
		d2fdtdp[12] += f[0]*-1.0;
		d2fdtdp[15] += f[3]*-1.0;
		d2fdtdp[18] += f[6]*-1.0;
		d2fdtdp[21] += f[9]*-1.0;
		d2fdtdp[25] += f[1]*-1.0;
		d2fdtdp[28] += f[4]*-1.0;
		d2fdtdp[31] += f[7]*-1.0;
		d2fdtdp[34] += f[10]*-1.0;


        dgemm("N", "N", &mwLlamosi2016_o2_Nstates, &mwLlamosi2016_o2_Nparams, &mwLlamosi2016_o2_Nstates, &done, J, &mwLlamosi2016_o2_Nstates, dfdp, &mwLlamosi2016_o2_Nstates, &done, d2fdtdp, &mwLlamosi2016_o2_Nstates);  
    } 


 /* All is well that ends well... */ 
 return 0; 

} 


/********************************
* Right-hand side for ODE model *
********************************/

int Llamosi2016_o2_f ( double time , const double *x , const double *p , void *varargin , double *f , double *dfdt , double *J , double *dJ , double *dfdp , double *d2fdtdp )
{ 
	int res = 0;

	/*******************************
	* 1st DERIVATIVE EQUATIONS (f) *
	*******************************/

		f[0] = p[1]*x[0]*-1.0+p[0]*x[2];
		f[1] = x[0]*9.47E-1-p[2]*x[1]*1.0;
		f[2] = x[2]*-9.225E-1+((tanh(p[3]*1.0E2-time*1.0E2+3.0E2)*5.0E-1+5.0E-1)*(p[3]-time*1.0+2.0)+(tanh(p[3]*1.0E2-time*1.0E2+3.0E2)*5.0E-1-5.0E-1)*(tanh(p[3]*1.0E2-time*1.0E2+1.0E3)*5.0E-1-(tanh(p[3]*1.0E2-time*1.0E2+1.4E3)*5.0E-1+5.0E-1)*(tanh(p[3]*1.0E2-time*1.0E2+1.0E3)*5.0E-1-5.0E-1)*(p[3]*2.5E-1-time*2.5E-1+3.5)*1.0+5.0E-1))*(tanh(p[3]*1.0E2-time*1.0E2+2.0E2)*5.0E-1-5.0E-1)*3.968E-1;
		f[3] = x[2]-p[1]*x[3]*1.0+p[0]*x[5];
		f[4] = x[3]*9.47E-1-p[2]*x[4]*1.0;
		f[5] = x[5]*-9.225E-1;
		f[6] = x[0]*-1.0-p[1]*x[6]*1.0+p[0]*x[8];
		f[7] = x[6]*9.47E-1-p[2]*x[7]*1.0;
		f[8] = x[8]*-9.225E-1;
		f[9] = p[1]*x[9]*-1.0+p[0]*x[11];
		f[10] = x[1]*-1.0+x[9]*9.47E-1-p[2]*x[10]*1.0;
		f[11] = x[11]*-9.225E-1;


	/***************************************
	* 2nd DERIVATIVE EQUATIONS (Jfx*f = g) *  
	****************************************/

	if ( dfdt != NULL ) 
	{ 
		dfdt[0] = f[0]*p[1]*-1.0+f[2]*p[0];
		dfdt[1] = f[0]*9.47E-1-f[1]*p[2]*1.0;
		dfdt[2] = f[2]*-9.225E-1;
		dfdt[3] = f[2]-f[3]*p[1]*1.0+f[5]*p[0];
		dfdt[4] = f[3]*9.47E-1-f[4]*p[2]*1.0;
		dfdt[5] = f[5]*-9.225E-1;
		dfdt[6] = f[0]*-1.0-f[6]*p[1]*1.0+f[8]*p[0];
		dfdt[7] = f[6]*9.47E-1-f[7]*p[2]*1.0;
		dfdt[8] = f[8]*-9.225E-1;
		dfdt[9] = f[9]*p[1]*-1.0+p[0]*f[11];
		dfdt[10] = f[1]*-1.0+f[9]*9.47E-1-f[10]*p[2]*1.0;
		dfdt[11] = f[11]*-9.225E-1;

	} 

	/* Jacobians and stuff. */ 
	if ( ( res = Llamosi2016_o2_jac(time , x , f , p , varargin , J , dJ , dfdp , d2fdtdp ) ) < 0 ) 
	return res; 

	/* All is well... */ 
	return 0; 
 } 

