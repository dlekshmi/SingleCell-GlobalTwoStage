/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * jac_Llamosi2016_o2_terminate.c
 *
 * Code generation for function 'jac_Llamosi2016_o2_terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "jac_Llamosi2016_o2.h"
#include "jac_Llamosi2016_o2_terminate.h"
#include "_coder_jac_Llamosi2016_o2_mex.h"
#include "jac_Llamosi2016_o2_data.h"

/* Function Definitions */
void jac_Llamosi2016_o2_atexit(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtEnterRtStackR2012b(&st);
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

void jac_Llamosi2016_o2_terminate(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

/* End of code generation (jac_Llamosi2016_o2_terminate.c) */
