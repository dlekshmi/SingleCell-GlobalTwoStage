/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * jac_Llamosi2016_o2_initialize.c
 *
 * Code generation for function 'jac_Llamosi2016_o2_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "jac_Llamosi2016_o2.h"
#include "jac_Llamosi2016_o2_initialize.h"
#include "_coder_jac_Llamosi2016_o2_mex.h"
#include "jac_Llamosi2016_o2_data.h"

/* Function Definitions */
void jac_Llamosi2016_o2_initialize(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  emlrtBreakCheckR2012bFlagVar = emlrtGetBreakCheckFlagAddressR2012b();
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  emlrtFirstTimeR2012b(emlrtRootTLSGlobal);
}

/* End of code generation (jac_Llamosi2016_o2_initialize.c) */
