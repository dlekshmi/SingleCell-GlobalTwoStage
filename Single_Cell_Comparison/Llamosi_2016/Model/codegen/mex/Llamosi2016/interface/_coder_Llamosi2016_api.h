/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_Llamosi2016_api.h
 *
 * Code generation for function '_coder_Llamosi2016_api'
 *
 */

#ifndef _CODER_LLAMOSI2016_API_H
#define _CODER_LLAMOSI2016_API_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "Llamosi2016_types.h"

/* Function Declarations */
extern void Llamosi2016_api(const mxArray * const prhs[3], const mxArray *plhs[6]);

#endif

/* End of code generation (_coder_Llamosi2016_api.h) */
