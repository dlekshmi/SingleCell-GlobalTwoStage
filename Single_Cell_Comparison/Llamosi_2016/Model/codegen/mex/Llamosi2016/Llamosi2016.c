/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Llamosi2016.c
 *
 * Code generation for function 'Llamosi2016'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Llamosi2016.h"

/* Function Definitions */
void Llamosi2016(real_T varargin_1, const real_T varargin_2[3], const real_T
                 varargin_3[4], real_T f[3], real_T g[3], real_T Jfx[9], real_T
                 Jgx[9], real_T Jfp[12], real_T Jgp[12])
{
  int32_T i0;
  int32_T i1;
  int32_T i2;
  f[0] = varargin_3[0] * varargin_2[2] - varargin_3[1] * varargin_2[0];
  f[1] = 0.947 * varargin_2[0] - varargin_3[2] * varargin_2[1];
  f[2] = 0.3968 * ((0.5 * muDoubleScalarTanh((100.0 * varargin_3[3] - 100.0 *
    varargin_1) + 300.0) + 0.5) * ((varargin_3[3] - varargin_1) + 2.0) + (0.5 *
    muDoubleScalarTanh((100.0 * varargin_3[3] - 100.0 * varargin_1) + 300.0) -
    0.5) * ((0.5 * muDoubleScalarTanh((100.0 * varargin_3[3] - 100.0 *
    varargin_1) + 1000.0) - (0.5 * muDoubleScalarTanh((100.0 * varargin_3[3] -
    100.0 * varargin_1) + 1400.0) + 0.5) * (0.5 * muDoubleScalarTanh((100.0 *
    varargin_3[3] - 100.0 * varargin_1) + 1000.0) - 0.5) * ((0.25 * varargin_3[3]
    - 0.25 * varargin_1) + 3.5)) + 0.5)) * (0.5 * muDoubleScalarTanh((100.0 *
    varargin_3[3] - 100.0 * varargin_1) + 200.0) - 0.5) - 0.9225 * varargin_2[2];
  g[0] = f[2] * varargin_3[0] - f[0] * varargin_3[1];
  g[1] = 0.947 * f[0] - f[1] * varargin_3[2];
  g[2] = -0.9225 * f[2];

  /*  odeSD */
  memset(&Jfx[0], 0, 9U * sizeof(real_T));
  Jfx[0] = -varargin_3[1];
  Jfx[1] = 0.947;
  Jfx[4] = -varargin_3[2];
  Jfx[6] = varargin_3[0];
  Jfx[8] = -0.9225;
  memset(&Jfp[0], 0, 12U * sizeof(real_T));
  Jfp[0] = varargin_2[2];
  Jfp[3] = -varargin_2[0];
  Jfp[7] = -varargin_2[1];
  for (i0 = 0; i0 < 3; i0++) {
    for (i1 = 0; i1 < 4; i1++) {
      Jgp[i0 + 3 * i1] = 0.0;
      for (i2 = 0; i2 < 3; i2++) {
        Jgp[i0 + 3 * i1] += Jfx[i0 + 3 * i2] * Jfp[i2 + 3 * i1];
      }
    }

    for (i1 = 0; i1 < 3; i1++) {
      Jgx[i0 + 3 * i1] = 0.0;
      for (i2 = 0; i2 < 3; i2++) {
        Jgx[i0 + 3 * i1] += Jfx[i0 + 3 * i2] * Jfx[i2 + 3 * i1];
      }
    }
  }

  Jgp[0] += f[2];
  Jgp[3] += -f[0];
  Jgp[7] += -f[1];
}

/* End of code generation (Llamosi2016.c) */
