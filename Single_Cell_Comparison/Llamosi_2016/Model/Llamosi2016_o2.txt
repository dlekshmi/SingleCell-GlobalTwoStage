********** MODEL NAME
HOG  osmosis model txt format: Does not include the observation model
Including sensitivities, directly integrating... 
to get second order information

********** MODEL NOTES
Implementation of the ODE model in Llamos et al, 2016 (Plos Comp. Bio)

********** MODEL STATES
d/dt(m) = -gm*m + km*inp
d/dt(pr) = kp*m - gp*pr
d/dt(inp) = -0.9225*inp+ 0.3968*u_c
d/dt(S11) = inp-gm*S11+km*S13
d/dt(S12) = kp*S11-gp*S12
d/dt(S13) = -0.9225*S13
d/dt(S21) = -m-gm*S21+km*S23
d/dt(S22) = kp*S21-gp*S22
d/dt(S23) = (-0.9225)*S23
d/dt(S31) = -gm*S31+km*S33
d/dt(S32) = -pr+kp*S31-gp*S32
d/dt(S33) = S33*-0.9225


m(0)=0
pr(0)=0
inp(0)=0
S11(0)=0
S12(0)=0
S13(0)=0
S21(0)=0
S22(0)=0
S23(0)=0
S31(0)=0
S32(0)=0
S33(0)=0

********** MODEL PARAMETERS

km=4.47
gm=0.008
gp=0.008
t_in=30

********** MODEL VARIABLES
kp=0.947 
tau=29.96
u_c=piecewiseIQM(0,le(time,t_in+2),piecewiseIQM((time-t_in-2),lt(time,t_in+3),piecewiseIQM(1,le(time,t_in+10),piecewiseIQM(0,gt(time,t_in+14),-0.25*(time-t_in-14)))))

********** MODEL REACTIONS


********** MODEL FUNCTIONS


********** MODEL EVENTS


********** MODEL MATLAB FUNCTIONS