% This file compiles the c file to mex model that
% is called by ODESD, after getting the c files from 
%  .txt file

% Models: Llamosi.txt: The model without second order sensitivities
%         Llamosi_o2.txt: The model with states augmented to 'cheat; the
%         integtaor to integrate second order sensititivites. 

% Checked on MATLAB 2016b 

%Add path
%addpath(genpath('/home/dlekshmi/TOOLBOXES/IQMtools V1.2.2.2')) 		% IQM TOOLS to read the .txt model
%addpath(genpath('/home/dlekshmi/TOOLBOXES/libSBML-5.11.4-matlab')) 	% LIBSBML to convert model
addpath(genpath('~/Repos/odeSD'))									    % FOR ODE INTEGRATION	

%% Model without second order sensitivities
mkdir('Llamosi2016')  % FOLDER TO SAVE ALL THE FILES

% Convert from .txt model to .m and .c models
%generateRhs('Llamosi2016.txt',{'C', 'Cpp', 'Matlab'},'./Llamosi2016',struct( 'doNotSubstitute', {{'u_c','k_p','tau'}}) )

% Directly compiling ODESDHybrid solver
odeSD_compile_hybrid('./Llamosi2016/Llamosi2016.c','Llamosi2016','./Llamosi2016')

%% Model with second order sensititivites
mkdir('Llamosi2016_o2')  % FOLDER TO SAVE ALL THE FILES

% Convert from .txt model to .m and .c models
%generateRhs('Llamosi2016_o2.txt',{'C', 'Cpp', 'Matlab'},'./Llamosi2016_o2',struct( 'doNotSubstitute', {{'u_c','k_p','tau'}}) )

% Directly compiling ODESDHybrid solver
odeSD_compile_hybrid('./Llamosi2016_o2/Llamosi2016_o2.c','Llamosi2016_o2','./Llamosi2016_o2')

