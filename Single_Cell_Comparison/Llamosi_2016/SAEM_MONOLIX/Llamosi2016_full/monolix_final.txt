;model definition
[LONGITUDINAL]
INPUT:
parameter={km, kp, gp, gm, tau}



EQUATION:
t0=0
m_0=0
Pp_0=0
u_0=0

if t>=30&t<=44
Inp=30
elseif t>=60&t<=74
Inp=60
elseif t>=90&t<=104
Inp=90
elseif t>=120&t<=134
Inp=120
elseif t>=150  &t<=164
Inp=150
elseif t>=180 &t<=194
Inp=180
elseif t>=210 &t<=224
Inp=210
elseif t>=241.9064 &t<=254.9064
Inp=241.9064
elseif t>=290.4352 &t<=304.4352
Inp=290.4352
elseif t>=372.1597 &t<=386.1597
Inp=372.1597
elseif t>=455.319&t<=469.319
Inp=455.319
elseif t>=510.8225&t<=524.8225
Inp=510.8225
elseif t>=546.0478&t<=560.0478
Inp=546.0478
else
Inp=0
end



if Inp>0
if t<(Inp+2)
x=0
elseif t>(Inp+2) & t<=(Inp+3)
x=1*(t-Inp-2)
elseif t>(Inp+3) & t<=(Inp+10)
x=1
elseif t>(Inp+10) & t<=(Inp+14)
x=-0.25*(t-Inp-14)
else
x=0
end
else
x=0
end



odeType=stiff

ddt_u = 0.3968*x- 0.9225*u  ;Actual input  
ddt_m = exp(km)*delay(u,exp(tau)) - exp(gm)*m    ;mRNA
ddt_Pp =exp(kp)*m -exp(gp)*Pp    

if t>exp(tau) 
 f=exp(-exp(gp)*exp(tau))*Pp
else 
 f=0
end




OUTPUT:
output = {f}
