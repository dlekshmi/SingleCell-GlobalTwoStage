function []=plot_SAEMPreds()
%%%------------------------------------------------------------------------
% Plots SAEM predictions of the fitted model to Lamosi_2016 data
% The results files are generated from MONOLIX version 2016 
% Lekshmi D, June 2017
%%%-----------------------------------------------------------------------

addpath(genpath('~/Repos/odeSD/')) 		% path to ODE integrator
addpath(genpath('../Model'))			% path to model
fnames={'Llamosi2016_full'} 			% Name of folder
estimates=readtable(sprintf('%s/estimates.txt',fnames{1}));
% Get the estimates
Mean=estimates.parameter([1,4,3]);
tau=estimates.parameter(5);
Dhat=estimates.parameter([6,9,8]);
Correl=estimates.parameter([12,11,13]);
Corr=[1,Correl(1),Correl(2),Correl(1),1,Correl(3),Correl(2),Correl(3),1];
Corr=reshape(Corr,3,3);

% Noise parameters
theta_1=estimates.parameter(15);
theta_0=estimates.parameter(14);

% Get the mean and the random effects covariance matrix
Model_prediction=[];
params0=[];
Dhat=corr2cov(Dhat, Corr);
betahat=Mean;
int_opts=struct();
int_opts.abstol=1e-6;
int_opts.reltol=1e-3;

% For storing error incase sampling from the distribution fails because the parameters are HIGHLY correlated.
ME=[] ;

% Sample 
for i=1:10000
try
paras0= exp(mvnrnd((betahat),(Dhat+Dhat)./2,1)); 
catch ME
disp(ME)
end
params0=cat(1,params0,paras0);
[f_t]=Run_Llamosi2016(0:600,paras0,int_opts);
f_t=f_t+(theta_0+f_t.*theta_1).*randn(size(f_t));
Model_prediction=cat(1,Model_prediction,f_t);
end

% Save the data to plot. 
save(sprintf('./%s_SAEMPreds.mat',fnames{1}),'Model_prediction','Dhat','betahat','theta_1','theta_0')
end

