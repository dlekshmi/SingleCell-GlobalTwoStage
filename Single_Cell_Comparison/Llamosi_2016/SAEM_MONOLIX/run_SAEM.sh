#!/bin/bash

# either add the path in the environment or go to the folder where MONOLIX is installed.
cd /usr/local/stelling/el6/Lixoft/MonolixSuite2016R1/bin

# Run in bash mode
./monolix.sh -nowin -p ~/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/SAEM_MONOLIX/Llamosi2016_full.mlxtran -f run
