function vals=calc_PL(f_t,theta_0,theta_1,data)
%%%---------------------------------------------------------------------------------------------------------
%
%   function calc_PL.m
% 
%   Calculate the likeilhood using pseudolikelihood. 
%   Result not used in Publication, as I just used the laplace based AR
%   likelihood.

%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------------------------------------------
%% 


vals=0;
for i =1:length(f_t)
d=data.Y(data.ID==i);
nonans=~isnan(d); 

h=(theta_0+exp(theta_1).*f_t(i).f_t);

if (any(h==Inf)||sum(log(h)==-Inf)||~any(isreal(log(h))))
res=Inf;
else
res=-(-nansum(((d-f_t(i).f_t)./h).^2)-sum(log(h(nonans).^2)));
end
vals=vals+res;

end