/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * jac_Llamosi2016_terminate.c
 *
 * Code generation for function 'jac_Llamosi2016_terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "jac_Llamosi2016.h"
#include "jac_Llamosi2016_terminate.h"
#include "_coder_jac_Llamosi2016_mex.h"
#include "jac_Llamosi2016_data.h"

/* Function Definitions */
void jac_Llamosi2016_atexit(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtEnterRtStackR2012b(&st);
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

void jac_Llamosi2016_terminate(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

/* End of code generation (jac_Llamosi2016_terminate.c) */
