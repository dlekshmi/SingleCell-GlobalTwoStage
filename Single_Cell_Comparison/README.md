This is the accompanying GIT folder for the analyses used in 
manuscript link: ##$##

## Folders: 
1. ``Llamosi2016``: Osmo-responsive model 
				To generate Figure 2: 
                
                  * Convert model from .c (or txt) files to mex files needed for simultion
    			  * carry out the first stage followed by second stage
    			  * get the predictions 
			    This can be done run ``sh run_GTS`` or ``sh run_GTS_desktop``
									 
	Note: We tried using CVODES/ odeSD for integration, both give similar results and conclusions as expected. 
		  To use CVODES in the first stage use FirtStage_CVODES.m. 	
		  		
2. ``Mup1 endocytosis:`` 
			To generate Figure 4:   

                 * Convert model from model files to mex files needed for simultion
				 * Get bounds for the noise parameters
				 * Carry out multi-start to get the first estimates using multi-start scripts
				 		( embarrassingly parrallellized)
				 * Do first sgae followed by second stage
				 * Get the predictions 
			Refer to ``run_GTS`` script. May need modifications.  

3. ``Simulation Studies``: Contains the simulation datasets/ figures and code based on:
        * Llamosi2016 model: TODO explain. 

4. ``Imporatance measures``: Contains script to reproduce the variance based analysis (Sobol)
					    for correlated parameters from a normal distribution. 
				 				 
                * ``Toy model``: Sobol sensitivity analysis (first order) A small test case
                * ``Llamosi2016``: Sobol sensitivity analysis (first order) for Llamosi model
                * ``Mup1_model``: Sobol sensitivity analysis (first order) for Mup1 endocytosis model

5. ``native_nlmefit``: Contains script to use the native matlab implementation  of SAEM on the osmo-shock model.

## Dependencies: 
1. ODE integrator: 
    a)  ODESD: to be cloned from : GITLAB Repo: ``git@git.bsse.ethz.ch:csb/odeSD.git``. In additon, 
                check the dependencies needed by ODESD to convert the model from a .txt format or IQM model. The .m files and .c model files are provided already. Therfore
                the model only may need to be compiled in the user environment. 

                    Merged version used: 
                    commit 057329dd14f5f660b5fedb0692e36682ccef1f6c
                    Author: luwidmer <lukas.widmer@bsse.ethz.ch>
                    Date:   Wed Feb 14 12:36:30 2018 +0100
                    - Added support for initial sensitivities to odeSD_hybrid
                    - Fixed up example


    b) AMICI: Can be cloned from: ``https://github.com/ICB-DCM/AMICI`` The merged version used here: 
                       
                    Merge: 7104d6c 6bf4b95
                    Author: Daniel Weindl <dweindl@users.noreply.github.com>
                    Date:   Mon May 29 16:35:29 2017 +0200
                    Merge pull request #103 from ICB-DCM/fixes_dw
                    Some small fixes



2. NLOPT: To be downloaded from http://ab-initio.mit.edu/wiki/index.php/NLopt#Download_and_installation
Version used: NLOPT 2.4.2 was used. Note that the development version was NOT used. 
Refer to the documentation in the link above. 
We executed   ``./configure --prefix=/palce/to/install/NLOPT --enable-shared MEX_INSTALL_DIR=/place/to/store/mex/NLOPT``
followed by ``make`` and ``make install``. In addition we had to append the path of the NLOPT libraries to ``$LD_LIBRARY_PATH`` before opening MATLAB. Make
If you find difficulties in installing in a linux environment please see:
http://mavis-wong.blogspot.ch/2012/04/install-nlopt-on-linux-computer-you-do.html.
A common issue is that sometimes you may get an error saying that MATLAB is uable to find the nlopt functions, if so make sure you added the path, 
and installed with shared libraries, and pointed the location of the MEX compiler while installing (i.e. the command above). 

3. Mex compiler for MATLAB (gcc or any thing that is compatible with MATLAB2016b and above)

4. In addition, for using parrallelization, kindly install the parallel toolbox in MATLAB. If using a Linux server or cluster, 
    kindly load the relevant modules: 
```
    module load matlab/R2016b 
    module load gcc/5.3.0 (or any version compatible with MATLAB)
    module load openmpi/xxx (any version to enable parallelization if using a cluster)
```

## Organisation: 
The folders consist of following parts for each model. 
1. Model: Model files, conversion scripts, simulation functions
2. FirstStage_objs: objective functions used in First Stage
3. SecondStage_objs: objective functions used in First Stage
4. Data: Data used for estimation
5. Auxilliary: helper files like taking inverse etc.  


and contains three main scripts with base names: 
1. FirstStage.m
2. SecondStage.m
3. Plot_predictions.m to simulate the predictions of the population model.
4. Readme

## Possible issues
1.  Note that many a times, due to strong correlations in the parameters of BOTH the 
models we used, inverting the matrix gave rise to warnings indicating 
very low condition number. This means you may observe some minor inconsistencies in
some estimates of the correlations/ covariances based on the version/ archictecture
you used. 

2.  Sadly at the time I made this repo, I was unaware of GIT-submodules. However,
all toolboxes used are under active improvement and have evolved from the version I used (see above for the 
commit from which I had cloned). 


Contact ``lekshmi.dharmarajan@bsse.ethz.ch`` in case you find any issues related to running the scripts
(though not regarding issues related to installation of toolboxes in Windows or Mac).
