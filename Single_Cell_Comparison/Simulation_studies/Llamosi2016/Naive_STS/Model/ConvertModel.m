% This file compiles the c file to mex model that
% is called by ODESD, after getting the c files from 
%  .txt file

% Checked on MATLAB 2016b 

%Add path
addpath(genpath('/home/dlekshmi/TOOLBOXES/IQM Tools V1.2.1')) 		% IQM TOOLS to read the .txt model
addpath(genpath('/home/dlekshmi/TOOLBOXES/libSBML-5.11.4-matlab')) 	% LIBSBML to convert model
addpath(genpath('~/Repos/odeSD'))									% FOR ODE INTEGRATION	
mkdir('Llamosi2016')  % FOLDER TO SAVE ALL THE FILES

% Convert from .txt model to .m and .c models
generateRhs('Llamosi2016.txt',{'C', 'Cpp', 'MatlabMex', 'Matlab'},'./Llamosi2016',struct( 'doNotSubstitute', {{'u_c','k_p','tau'}}) )

% Directly compiling ODESDHybrid solver
odeSD_compile_hybrid('./Llamosi2016/Llamosi2016.c','Llamosi2016','./Llamosi2016')
