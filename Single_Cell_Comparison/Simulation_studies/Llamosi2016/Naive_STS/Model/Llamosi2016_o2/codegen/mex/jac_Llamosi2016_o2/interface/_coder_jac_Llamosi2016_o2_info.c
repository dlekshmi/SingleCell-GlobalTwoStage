/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_jac_Llamosi2016_o2_info.c
 *
 * Code generation for function '_coder_jac_Llamosi2016_o2_info'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "jac_Llamosi2016_o2.h"
#include "_coder_jac_Llamosi2016_o2_info.h"

/* Function Definitions */
mxArray *emlrtMexFcnProperties(void)
{
  mxArray *xResult;
  mxArray *xEntryPoints;
  const char * fldNames[4] = { "Name", "NumberOfInputs", "NumberOfOutputs",
    "ConstantInputs" };

  mxArray *xInputs;
  const char * b_fldNames[4] = { "Version", "ResolvedFunctions", "EntryPoints",
    "CoverageInfo" };

  xEntryPoints = emlrtCreateStructMatrix(1, 1, 4, fldNames);
  xInputs = emlrtCreateLogicalMatrix(1, 4);
  emlrtSetField(xEntryPoints, 0, "Name", mxCreateString("jac_Llamosi2016_o2"));
  emlrtSetField(xEntryPoints, 0, "NumberOfInputs", mxCreateDoubleScalar(4.0));
  emlrtSetField(xEntryPoints, 0, "NumberOfOutputs", mxCreateDoubleScalar(4.0));
  emlrtSetField(xEntryPoints, 0, "ConstantInputs", xInputs);
  xResult = emlrtCreateStructMatrix(1, 1, 4, b_fldNames);
  emlrtSetField(xResult, 0, "Version", mxCreateString("9.1.0.441655 (R2016b)"));
  emlrtSetField(xResult, 0, "ResolvedFunctions", (mxArray *)
                emlrtMexFcnResolvedFunctionsInfo());
  emlrtSetField(xResult, 0, "EntryPoints", xEntryPoints);
  return xResult;
}

const mxArray *emlrtMexFcnResolvedFunctionsInfo(void)
{
  const mxArray *nameCaptureInfo;
  const char * data[19] = {
    "789ced5ccd6fe3c615a78b8d9ba2489016e847fa91747b280a2cd6942d592bdfa22f4bb2ad2f53b2b472365c8a1c49230e3f445296ecd31e53f4d01441d1f6d0"
    "62d143b1a75e9aa2c8b997f6d05b0af40fe8a9d7fe03e58892ad9d654c99a46479490284f428bdd1ef3dce6fe6bd3743511b8522651e6f9be71851d4a6f9faa6",
    "797e85b28e37a6f28679be3f7db5aedfa3de9acabf304f5e910d3036ac0f654e02d4ec101409ca9c6cd4ce5540694057d01910269f74200235288123654ec843"
    "5390f6e73eba14f047f87dba077891194a94d6d32f7f8642f3c2c49e17d4953df76cec19cdd9f3ce543ecd3ea17b8a0468010151ef49903e429ca4e8acaad149",
    "84581e20a4d38581443350ee22c0a6cd0b6c5a91544e83ba224fbf0ed99dc8769c2e2a0240b34bf80aabecd07d8e675fbeb4254df0261cf0be49e0c5b2a409f0"
    "0c0a6011fd4d427f737277866d042c7f7deca05f27f4b17c5a386a9a2e938141b7f5875d0d0afa88d6470f35a02ae6151dd0c564ed2899628fb1ad6dda5014d4",
    "56c63490108d609b963803716d5a51757a66ca963abd7ff3f66cdae0d998c3f3b5e9758a7af6d7dffde75f49f7fad6e141ffb7a977fdf9fdbcc3fdf836713fb0"
    "3c8a9e68e7a5529d17956452eb32c3544268a4adf67e3ad7de864d7bd4dcab9befaf73ffb1c3f355020f96a76d2ca4ef954f3542bfe6af3f66ee9066f7c7259f",
    "3eddacde2a9f7cd377e2d3b788fb81e5c4a8daa93763f147f93374bc9749248e943a4fad079f56dd7f2a0e78de27f060993767406d0b9ac1812673680beaa921"
    "4446412e0d25a041de179efdc6419f27f4798f7ec2e7838961f4839965346999e5362ffdf6b3b7fff945207867378f6510dce61299ee7eacbd374c0865598aa7",
    "2af9907776787e44e0c132c13b9de71018ab9338d580267356c13b81d0173cfac99677af58e675bea33ef52b7ebb6d7d37bc6b8f1f978e3b9c78522d1e960e8e"
    "81c14498724079e7e4bf6f1278b04cf0ce6cf2cabe65c7932784fe8947ffd8f2cdb4087bc8433cf9f3a0c49376fc2ad68dc8e3422329cbf56358d1aba5ae5e1b",
    "6457c3af170e78d7ad1ee2e4df6f1078b16cf6595632a00474b607900ab439fb23d7b4373be6db9be97de280e323420fcb3e8e4baf98e4917f5ffcf9f3e0f2af"
    "d53dec24e1451654ba99ec5e55aa1e44f45c6e35fc7bee80171178b1bcdc7e749f57244991591e5754f519ce30cf0bf3bc20e479eb3aae3bf9f13b042e2cdbe4",
    "7b9c96edfac23b273f9d12faa71efdf4a5791eb6c83bdfdef97739b8f35f39974b0f62654647db8dc6202eb47558852b5a1f58d77ef48103aeb7085c5886ba6c"
    "8dff065ed6f427bf73f24f8bd06f79f40f1e8f3a700c045531dd43bf64d2d43d5efae9af825c47c945c57366d085285d4b8bc78354b726d6b39960f3cccdfa36",
    "d43bd09c337b77757dfb257e59a64c3c12f2cadd7adcd90064d5335edf6b8ea28a52d199ede8e316b51a5ead45fe71c5abfb1da8e946075ee273cadbde23f061"
    "1907a166db6c47d190a2a8ac7206b40e5246564ae8ad6ef2dc018f48e8891efd35175f4fdd778d699ee3c867bf0e0a0fede637253d1e543bc556b2a729192d7b",
    "643446c5d68ae2c8bf3be0fd258117cb2bec57f7afffc2cdea921bd36b57f6dc9bcc893ee5895016c0b8201b0bcd8f55a2bdaa0f7e25e6c759d819ce8f943b5e"
    "56766a918bcab9dc1c0ffad1f3837cbfc6b45bf9f558bf5b75fff9c001cfa279dd4df9c9f7382dcce75ed1f7fafbbee9dfb57cee759befdce47166fb1237f6c6",
    "c7dbc8ff667e44e61bda32c26bfe17e8b8138de32d10e9972375988ad44617db5c0cad6abd20e4a1c54328bf063c8472c843ca3d0f770b4551154b606ce48b4c"
    "b7923bace4222815d661bce473cbae6f86f9dbfaf3ea36f3b7755d0f77aa6bbe4be0c23251e768234edf1a778134df3f9795d7f9ed27dbf1e70136899e98e471",
    "3fd89f7ef24970f9d69350a29388a54471af0a3a7c6d186d8264f6f5e4dba2fdc8896fdf237061d98e6f5046500657f62e6bff254be8b1cbf3936592cdf3ba37"
    "e8afeffdef5970f77ba5da5c524f9562953394a8ca8691688883c18af65fde55be2db0df72c2b7a10e580d74f07b4f7c5b8be77a2c3fcd99e4f9b99e40f34e4b",
    "16634cb2932ef3595def732785de5e465ad17add7307bc328117cbcbe5ddfd36c07511a3a701bda72061311efe90c089653b1ece35eb29ce74e2619bc0d35e9e"
    "df2e4df2611d211df95b70e3cda270b2572e2586b9fd03a13450ca957ae6221af078d3c98fe17ee770bfb35bbeadf37ee7758d377f40e0c232c1b7594c765952",
    "59e63cc71178b8a5f8696ad295ab2eefa3cbbacad79f89c1e55d07a4467b8abadd1c545523c148bdd17e721470def930cf4deb2ac652eb291f127a1f2ecf3fa6"
    "e8f9ff89fefbd91f82cbb3da49a32164627cb9ce6564d83f90b3fc236645f514a7711b1078b17c5a68a65d7423dd9c2a80405b9d68da95ac97ecd8ea5019a002",
    "5900327f7eb9aedb74c0f763021f9609be59cb1369f36217c8794e16d05c3dd3cdff8b0d65515646f24279719fd0ef53160fdd38d0c673575cb4b172f2276a36"
    "76de80971ffdfe76ff87cf377d37bcac54c5f8e39cca5d1c0928b1cb1cc6dbb5137d4dea9c4f09bc5876dbaf1cc6f7d4519249aa704b5ac48fdf257061d9e2e3",
    "ab24f7837feb3e7eb9e45df71f3f0b469dd3eef9a0c62e8a70c35eb90b61ab941d9c1b652d8fa8d5f0ee8503de218117cb4b19cf0f0a3566a8aa8a6600c1a67f"
    "a954383f86f3a37ffae1fce8cffcf8d401d743021796093e5ecf7cca1f5edeb571ce253f9f04859fb6ff7b9bc9f7414e2ec6b2dd6adfa88c3ad526a351ebc1cf",
    "dbaa4f38d54dbf4fe0c2b25dfd664afa4b7b9755c7794ae83da556338eb9ecaf1ffff12fc1e09bedf3b44ab35fcc5622cca0c75df4760f237c8429afe8f9a2e7"
    "0e78bf2cce5ae2fa3c0fce38c4ce6dd8f469dd62c23f1d5e005635b459bbcbda27c3117adcf2fc3633c98ffdd781de27a316b61b7abc5cded62ee2fbfdded10e",
    "8ac50ff3210fe779e8269e2078c8b33ca71bd462f1e81b447b58365b8aee508bcd834f08fd271efc766d3c6a5915c69d947bfe4512c347c5e28e2635d25d692c"
    "225e8a1eee86f3e08df817ee9709f7cbdcb5fd32ff0704d604fe", "" };

  nameCaptureInfo = NULL;
  emlrtNameCaptureMxArrayR2016a(data, 27784U, &nameCaptureInfo);
  return nameCaptureInfo;
}

/* End of code generation (_coder_jac_Llamosi2016_o2_info.c) */
