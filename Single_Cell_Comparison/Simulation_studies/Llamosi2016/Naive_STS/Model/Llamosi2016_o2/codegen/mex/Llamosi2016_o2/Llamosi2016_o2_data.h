/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Llamosi2016_o2_data.h
 *
 * Code generation for function 'Llamosi2016_o2_data'
 *
 */

#ifndef LLAMOSI2016_O2_DATA_H
#define LLAMOSI2016_O2_DATA_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "Llamosi2016_o2_types.h"

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern const volatile char_T *emlrtBreakCheckR2012bFlagVar;
extern emlrtContext emlrtContextGlobal;
extern emlrtRSInfo emlrtRSI;
extern emlrtRSInfo b_emlrtRSI;
extern emlrtRSInfo c_emlrtRSI;
extern emlrtRSInfo d_emlrtRSI;
extern emlrtRSInfo e_emlrtRSI;
extern emlrtRSInfo f_emlrtRSI;
extern emlrtRSInfo g_emlrtRSI;
extern emlrtRSInfo h_emlrtRSI;
extern emlrtRSInfo i_emlrtRSI;
extern emlrtRSInfo j_emlrtRSI;

#endif

/* End of code generation (Llamosi2016_o2_data.h) */
