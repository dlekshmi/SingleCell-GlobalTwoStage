/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_jac_Llamosi2016_o2_api.h
 *
 * Code generation for function '_coder_jac_Llamosi2016_o2_api'
 *
 */

#ifndef _CODER_JAC_LLAMOSI2016_O2_API_H
#define _CODER_JAC_LLAMOSI2016_O2_API_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "jac_Llamosi2016_o2_types.h"

/* Function Declarations */
extern void jac_Llamosi2016_o2_api(const mxArray * const prhs[4], const mxArray *
  plhs[4]);

#endif

/* End of code generation (_coder_jac_Llamosi2016_o2_api.h) */
