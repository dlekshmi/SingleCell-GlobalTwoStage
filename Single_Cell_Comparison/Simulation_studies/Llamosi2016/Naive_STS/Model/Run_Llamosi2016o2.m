function [f_t] = Run_Llamosi2016o2(t,para,int_opts)

ME=[]; % Store error


try
% Parameters passed 
k_m=para(1); %m production
g_m=para(2); %m degradation
g_p=para(3); %p degradation

tau=exp(3.4);

% May need change
f_name='Llamosi2016_o2';
options=struct('AbsTol',int_opts.abstol,'RelTol',int_opts.reltol);

addon=[0 2 3 10 14];% defines the time where the integration input changes

% times when input is added
t_in=[30  60.0000   90.0000  120.0000  150.0000  180.0000  210.0000  241.9064  290.4352  372.1597  455.3190 510.8225  546.0478];

% intial time, states, first addiion of input
t0=0;
x0=[1.0e-10 1.0e-10 0 repmat(1e-10,1,9)]';
times=t0;
xs=x0';
inp=30;

% iterate over points when input is changed
for i=1:length(t_in)
        for j=1:length(addon)
     
[T1 ,X1,S1]=odeSD_wrapper_hybrid(f_name,linspace(t0, t_in(i)+addon(j)+tau),x0,options,[k_m,g_m,g_p,inp+tau]');
t0=t_in(i)+addon(j)+tau;
inp=t_in(i);
x0=X1(end,:)';
times=cat(2,times,T1(2:end));
xs=cat(1,xs,X1(2:end,:));
s_x0=squeeze(S1(:,:,end));

% reset new intial conditions
options=struct('AbsTol',int_opts.abstol,'RelTol',int_opts.reltol,'initialSensitivities',s_x0);
  
	 end
end

[T1 ,X1, S1]=odeSD_wrapper_hybrid(f_name,linspace(546.0478+14+tau, 650),x0,options,[k_m,g_m,g_p,inp+tau]');
times=cat(2,times,T1(2:end));
xs=cat(1,xs,X1(2:end,:));
 


% Observation model lag
ydel=interp1(times,xs(:,2),t);
f_t=exp(-g_p*tau)*ydel;

catch ME
disp(ME);
end

if isempty(ME)==false
   f_t=zeros(size(t))+Inf;
f_t=f_t';
end
end


