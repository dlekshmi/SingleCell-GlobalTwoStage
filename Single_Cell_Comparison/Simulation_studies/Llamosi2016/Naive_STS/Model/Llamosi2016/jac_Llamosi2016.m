function [Jfx,Jgx,Jfp,Jgp] = jac_Llamosi2016(varargin)

    time = varargin{1};
    x = varargin{2};

    if nargout > 1
        % odeSD
        if nargin > 3
            p = varargin{4};
            
        else
            p = [4.47;0.008;0.008;30];
        end
        if nargout > 1
            f = varargin{3};
        end
    else
        % MATLAB ODE integrators
        if nargin > 2
            p = varargin{3};
            
        else
            p = [4.47;0.008;0.008;30];
        end
    end


    if nargout<1
        error('Too many output arguments');
    end

    Jfx = zeros(3, 3);
	Jfx(1) = -p(2);
	Jfx(2) = 947/1000;
	Jfx(5) = -p(3);
	Jfx(7) = p(1);
	Jfx(9) = -369/400;


    if nargout == 1
        return
    end

    Jgx = Jfx*Jfx;
	

    if nargout == 2
        return
    end

    Jfp = zeros(3, 4);
	Jfp(1) = x(3);
	Jfp(4) = -x(1);
	Jfp(8) = -x(2);


    if nargout == 3
        return
    end

    Jgp = Jfx*Jfp;
	Jgp(1) = Jgp(1) + f(3);
	Jgp(4) = Jgp(4) + -f(1);
	Jgp(8) = Jgp(8) + -f(2);


end

