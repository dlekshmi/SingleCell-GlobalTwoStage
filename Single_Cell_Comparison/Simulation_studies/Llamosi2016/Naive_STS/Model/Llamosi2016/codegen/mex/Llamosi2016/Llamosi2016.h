/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Llamosi2016.h
 *
 * Code generation for function 'Llamosi2016'
 *
 */

#ifndef LLAMOSI2016_H
#define LLAMOSI2016_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "Llamosi2016_types.h"

/* Function Declarations */
extern void Llamosi2016(real_T varargin_1, const real_T varargin_2[3], const
  real_T varargin_3[4], real_T f[3], real_T g[3], real_T Jfx[9], real_T Jgx[9],
  real_T Jfp[12], real_T Jgp[12]);

#endif

/* End of code generation (Llamosi2016.h) */
