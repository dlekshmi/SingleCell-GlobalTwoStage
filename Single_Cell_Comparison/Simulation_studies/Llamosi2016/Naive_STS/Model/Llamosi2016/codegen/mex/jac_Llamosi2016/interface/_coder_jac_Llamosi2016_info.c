/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_jac_Llamosi2016_info.c
 *
 * Code generation for function '_coder_jac_Llamosi2016_info'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "jac_Llamosi2016.h"
#include "_coder_jac_Llamosi2016_info.h"

/* Function Definitions */
mxArray *emlrtMexFcnProperties(void)
{
  mxArray *xResult;
  mxArray *xEntryPoints;
  const char * fldNames[4] = { "Name", "NumberOfInputs", "NumberOfOutputs",
    "ConstantInputs" };

  mxArray *xInputs;
  const char * b_fldNames[4] = { "Version", "ResolvedFunctions", "EntryPoints",
    "CoverageInfo" };

  xEntryPoints = emlrtCreateStructMatrix(1, 1, 4, fldNames);
  xInputs = emlrtCreateLogicalMatrix(1, 3);
  emlrtSetField(xEntryPoints, 0, "Name", mxCreateString("jac_Llamosi2016"));
  emlrtSetField(xEntryPoints, 0, "NumberOfInputs", mxCreateDoubleScalar(3.0));
  emlrtSetField(xEntryPoints, 0, "NumberOfOutputs", mxCreateDoubleScalar(4.0));
  emlrtSetField(xEntryPoints, 0, "ConstantInputs", xInputs);
  xResult = emlrtCreateStructMatrix(1, 1, 4, b_fldNames);
  emlrtSetField(xResult, 0, "Version", mxCreateString("9.1.0.441655 (R2016b)"));
  emlrtSetField(xResult, 0, "ResolvedFunctions", (mxArray *)
                emlrtMexFcnResolvedFunctionsInfo());
  emlrtSetField(xResult, 0, "EntryPoints", xEntryPoints);
  return xResult;
}

const mxArray *emlrtMexFcnResolvedFunctionsInfo(void)
{
  const mxArray *nameCaptureInfo;
  const char * data[15] = {
    "789ced5b4d6fe3441876d15216a15d2d488016c1422f0869b575ba0d25b96dbe9ab6db7cd549db4d05913fc6c9d4634fe2713eda538f481c58b45a0107a4fd05"
    "5c40429cb9ec85db22f103f807fc013cf968bd53b3cec6499aae63c94d5e3beff8791fcfe379df19975bd8cc70f6769dfeb1bf2dda1f57edfd35aeb7bddeb717",
    "ecfdbdfe67eff815ee5adffed6de656c58a063f54e1aa20eb8c1a6601d1aa261158fea803301c1a80594ee19152250843ad8c60e6303da86beee38756ad053f4"
    "7ba206644d68ea9c5923a797e190d3e8c6f3943b8be78a4b3c8f1df1dce8db07a92ff91ad601af20a0919a0ef91d50c78417a051b5af0c10ba9346581251b18d",
    "054bac82fe990a3d554960bd2e9a9060c33eac379168416c5488d5542020fc3612754ce0ddd0ca1a9f15610b5484a2c067b002d073e70e45b9e2b097f57e3c11"
    "8f78ae32f1505b3715d8820a18c67f91f15fecdebda68440effa5f7bf897187f6a1f6c6eefdb941ac0e22572a76a4285b479d2be63daacda4708e033b1e2762c",
    "5ed9a1a14abc8531927087073ae21194785db49028f1b84ef84128cb75173e165df02c38f0bcd93fce7127bffdf4cf5fb1d1fd7b9b0fff1fe337c773fd0d8ffb"
    "e1d4eb8dbedd5edd358fb2d992ace158ccac0acd7844d94bf4dafbccd1de824b7b9ce37394dfcf72ff71c3f3068387dafd3686f2f7aba722e35f1c2f1f033af4",
    "c1fd19514f8f160b17aaa7b1f97be9e95de67e503bd22ea8a5fdf0da171b2db4134d4622dbb82473b3a1a769f79fbc079e8f193cd496edd1cf5c8676f2601a22"
    "5a8624de84c8da34b24d1d98501e8bce7ef0f097197fd9274f74bfdd0d8cbf3d888c6723ebd1e6a7dffe7afdcf6781d09ddb38964470458c24abeb6129da8c28",
    "39435f8be737e6ba73c3f3098387da8cee882c22d0a977f3570bdaca9986ee14c65ff1c993abeece45e677bce31e8d2b7fbb68ff517427751e64775451db2d64"
    "ee67b776802584845c4075e7c5df3b0c1e6a33bab39b3c8b6fd2f9e42ee3bfeb931f57bdd91151867ce493df04259f74d357a664851e6ceec50ca3b403f3a490",
    "ad926223351d7d3df5c07bd9e64bbcf87f9b8987da769faee816d401a9d400aa03d3c14fe805ed0d36677b03bf871e38be62fca83dc6e7d6b9907ceaf3d92fbf"
    "07579fe5ea7d35068f53205f4da6a205bdb01522e9f474f4f9c4032f62f0527bb2fd6849c6ba6e2b56a633b26480735e07ceebc020d481b3fa5cf7e2f17d0617",
    "b55dea41d14c55c7a23b2f9e0e18ff039f3cfd6f1d4823f2afb71b7fe7823bfee5d2e944239c13085ad9db6bac2912810538a5f58359ed47f73c705d6370511b"
    "12a3f7fcb7e8b2e878ea3f2f7eca8c7fd9273ff479a4c20e50ead8a6877f2ea43e3d7efae9e320cfb3a457b523a1518528514c683b8d78b5a89552c960eb6c94",
    "f56f4854688f99b5cbbafefd9cbe7aa1b8be0f30d7d5797fb7f5ba5603a4ea2d9944f7dbab18e789b0b2faa0cc4d475733517f9ce96a498526b154788acfab6e"
    "bbc5e0a3364d42edb62b2a3611c6f50a6e015345b8dd2b09fdcd9b3cf1c0a3317e9a4fbe1cf9759fbe1784e63b8f3cf93e283a741bdf70a2d328a89972ac66e2",
    "a499dab6f6da99f294f2c8a71e78bf63f0527b8afd6ae9c53f78b979c985feb1b378ae74c7c431d589d0504067d3b0861a1f0b4c7b8531f0ca8c8f83b4733e3e"
    "72a3e9327fb7183ace1f19fb9dc6e1ead1d6c6615190ca1bb3b1be37edfe73cf03cfb075ddcbea53ae89e6bc9e3be7eff7fa63f3bf6cf5dcab36de8d52c7d9ed",
    "eb62c79f1e2fa2fe1bf088ec2f7c2f08bff55fa0f34ed4592b83d0612e5482f150b17dbc2286d1b4d60be63aece9101aaf800ea131d72137ba0e3fdfcc68752d"
    "0b3ad64646a8e6d3f7f3e9108acfe761fcd473939edf9cd76fb3afab8bacdf66753ddc6b5ef326838bdacc3c878444b2dca902ddd93f2755d78d9b27d7e7cf6d",
    "1a12df0dc9e7fb603f7ffa30b87aabe928a246c2714d8b16802a179babfb20967a35f5366c3ff2d2db070c2e6abbe90d1a081ae02cde49bd7f5961fc2a93e3a9"
    "17924d948ffe7aebdf93e0beef1597c4188967c3f9168a140ccb8aec698dc694debfbcac7a1be27dcbaede9a04544ca0d2efbef43613fff7d3e3c91192effffb",
    "09b4eecc58262cc4d4444e4e117228ee6ed6a2497d4aeb754f3cf01a0c5e6a4f56774b12a0f32256cd04a48691329c0e3f627052db4d878e667de5995e3a9418"
    "3cd2e4783b0d690ceb0889d01fc1cd3733ca6e34978d34d3eb5b4ab68173f952f27835e0f9a6178ff3f79de7ef3b8faab7597edf7956f3cd0f195cd466f436c8",
    "c94ea7542639ce890c1e71223cf5433aa3eaf43e8e38aff2d689165cdda920de8ee2faca7ea350b722825e6bafc7da53d0dd7fafae8449",
    "" };

  nameCaptureInfo = NULL;
  emlrtNameCaptureMxArrayR2016a(data, 19728U, &nameCaptureInfo);
  return nameCaptureInfo;
}

/* End of code generation (_coder_jac_Llamosi2016_info.c) */
