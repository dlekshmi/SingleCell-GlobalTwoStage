/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_Llamosi2016_info.c
 *
 * Code generation for function '_coder_Llamosi2016_info'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Llamosi2016.h"
#include "_coder_Llamosi2016_info.h"

/* Function Definitions */
mxArray *emlrtMexFcnProperties(void)
{
  mxArray *xResult;
  mxArray *xEntryPoints;
  const char * fldNames[4] = { "Name", "NumberOfInputs", "NumberOfOutputs",
    "ConstantInputs" };

  mxArray *xInputs;
  const char * b_fldNames[4] = { "Version", "ResolvedFunctions", "EntryPoints",
    "CoverageInfo" };

  xEntryPoints = emlrtCreateStructMatrix(1, 1, 4, fldNames);
  xInputs = emlrtCreateLogicalMatrix(1, 3);
  emlrtSetField(xEntryPoints, 0, "Name", mxCreateString("Llamosi2016"));
  emlrtSetField(xEntryPoints, 0, "NumberOfInputs", mxCreateDoubleScalar(3.0));
  emlrtSetField(xEntryPoints, 0, "NumberOfOutputs", mxCreateDoubleScalar(6.0));
  emlrtSetField(xEntryPoints, 0, "ConstantInputs", xInputs);
  xResult = emlrtCreateStructMatrix(1, 1, 4, b_fldNames);
  emlrtSetField(xResult, 0, "Version", mxCreateString("9.1.0.441655 (R2016b)"));
  emlrtSetField(xResult, 0, "ResolvedFunctions", (mxArray *)
                emlrtMexFcnResolvedFunctionsInfo());
  emlrtSetField(xResult, 0, "EntryPoints", xEntryPoints);
  return xResult;
}

const mxArray *emlrtMexFcnResolvedFunctionsInfo(void)
{
  const mxArray *nameCaptureInfo;
  const char * data[17] = {
    "789ced5ccb6fe3c619a78b8d9ba2c8a301da227da4d94bd062b1a6bcd23ad2cd7a59d6da7a9992edd5a251f9184a630e3914877ad8a73d16e8a1298245d24381"
    "bd16057a6981a2b7a2bdf4d25b5af4964b02b4e7fc03e1e861cbb3ccd22b4a34d7140142fc447dc3eff7717efce69bf9286ead58e29ced7567ffdf071cb7ee7c",
    "beeaecdfe0c6db2b1379cdd97f38f91c7f7f8b7b6d22ffdad9656cd860688f4f1aa20eb8e9a6601d1aa261d74f4dc0598060d407cae88c0a11a8431dece31961"
    "173a82be3373ea5ca0a7e871b603644de8e99cd521e797e1d0ac30c2f34fee02cf2d173cbf99c1f3e6447e94ff39dfc13ae0150434d2d1217f004c4c78011a6d",
    "e7ca00a1bb05842511d50758b0c536989c69d153ad2cd64dd182041bced77a0f8936c4468bd83d0502c2ef2351c704de8b6d6ef16511f6414ba80b7c092b005d"
    "3a3773bca1cfe08979e0b97509cf2dce168d0ed54b7ae8ad337e581fddb59e84c0f8babff4d017187d2a3f2aee1f3bae3480cd4be46edb820a19f06470d772bc",
    "e97c43005f4ad7f7d399d6018529f136c648c2431ee8884750e275d146a2c403a4f60c9e02997882c5b3ee62cfda8c3ddf9a7cef6cbfdf7eefdf691ffa7eafbf"
    "30fd5deef9f7e37bdce5fb41e52d58e8c77395a491354bef03f1a897895b673be3f67e3ad3de9a4b7bdccce73cbf0f5bffa97ad8f303c61e2acb0e4bad0de83c",
    "e42c43441b441691686dd0862f703e8f9fd36db6dda9de871ef6b418bd964fffd0fdce08107f678a88bf338634f5d5fcfdf4f11ffefff7e8f2ccdc33f249b0d7"
    "1485bdf2564aeb837bc35e311b4d9efdc2c39e9f31f65099e199689ae85418f5cc9d9e21d3705a34aa4894a7e30b7a9d6d8febbcc15c87caeaa4b55647349c50",
    "3f6ae7a9473b3ad38eeed37fae3cfc7ac423cffae8d7cdcfe2d1e565c26818f761621023a280a501a86b4931970b86974f3dec0d4bbf5a565cf43bfe5cc5c79b"
    "c3c3eb8c8f372d1fdcf6c0e316f74e44b935d3de42f8e9e5d7278cfe9390f895f185cff8fab7fe2b5ffc27b2bc3e33131d9cd1544462754ddf29e5ccb67c3f24",
    "bc7e99fb9f1b9e57193c54d62d05f6a1028298ef6930fa545e501e814dc24fa16c982efe788138fb97df7d7ead71f6b799b71773fd79f838881f5aa7e57243d6"
    "703a6db5855e26a91c85240fbdcefee366cf37197ba83c696321f1d1cb1f7546bfbe587f4cdda14fefcf9c7cfa68bd168d71eb7799fb41e5e4a0a6368e135bef",
    "eff6d1412a974ceee386cc85834f41f71faf3cf1278c3d5466f24448323d88eca251eee9c082f24278f68987becce8cb3efde49a27b2c8fcaf5bfcf9f57f7d1a"
    "09deb9c5b11c829b6232d7de4948a95e52a918fa56a6babbe29d9b3def32f650d9657e060ccdd1f8d5860e7382e09dc2e82b3efde4cabb6790f98d77dc478b1a",
    "bf5db7fe3cbc93860fcb07aaa81dd64a7be50707c016624225a2bcf3f2df5b8c3d546678e73479816fd9e3c94346ffd0a77f5cf9e620a21ef2319efc5554c693"
    "6efc2a35ecd8c3e251da301a07b04a6ae536a977f3abf99279e64bbcfcff1d060f959d3eddd26da803d2ea0064026bc63fcb5ac7ff80d1a3f2029f5bcf40f2c9",
    "cf4ffff4d7e8f2b3d9de53d3f02c0faaed5c3e55d36b0f62a45008c77a2162eca5f272fbd16d19ebbac3589956de91a99dab3c70950746210f0ceb73ddcb8fdf"
    "67eca2b2eb7a7dbe1dc83afd2346ff914f3f7d6d1e4811f9e7db9bffad4437fe550a856c37511108da3c3aea6e2912813518d0fa4158fbd1b6875daf31765119",
    "1263fcfcb769f97b30f5304d46bfe9d33ff479a4c221504cecb887bf04695a90e0a39f3e89f23c4b21ae9d0add3644d97a563be866da75ad910fa82e2dac3c9b"
    "67fd1b12153a313390f71d96b17e79895f63287eeb3d23c32bb7f5ba7e17e4cdbe4c52c78338c655226cc61f36b96078158afce38257b75568115b85e7f679e5",
    "6def30f651990e429db65b2ab610c6660bf781a5223c18a784fee64d9e7ad8a3317a9a4f7fcdd6ad8fddf71c68bec7918f3f8e0a0fdde21bce0ebb35b5d44c77"
    "2c9cb3f2fbf6d1a0d40c495d986bbd6770fdeaf6f37ff062f3926bdcb3efffd140b2a03c111a0a18160dfb4af1b1c6b4575b805f99f878a90e76151f5f9c97d5",
    "7bf5d859f5d4381e764fe2a70f764fea82d4dc0dc7fa5ed0fd67dbc39eabe6752fca4fb9235aab7cee197dbfd75f98fecb96cfddb478374f1ee7b4af8b437f7c"
    "bc8efceffc3d4ae7801f83f09bff457adc89865b4d103ba9c41a3013ab0fce36c5040a6abd60c5c3310fa1710378088d150fb9f97978bf58d24cad0c86f66e49",
    "68570b7bd5420c6556f3307ef2b965cf6faef2b7f0f3ea3af3b7b0ae877bcd6bbecdd84565669e434222d918b681eee77f97ae9ad72dda4feeefad5348fc0892"
    "cf7ab03fbef76174f9d6d151524d26329a96aa0155aef7e2c7209dbf997cbb6a3f5ac0ff458cf8060d040d70817759f597c1fc4fc4c84f6348befe27827be7cb",
    "c7d1adf7ca48629a64ca896a1f256b866d278fb46e37a0facb97956f57a8b71cf1ad4740cb022a3df6c5b750bcf733f6d30c24dfeffd449a7756ba9410d26ab6"
    "22e70939110f8b9d544e0f68bdeea987bd06632f9597cbbbdb12a0f32276c702a4839172351efe98b193ca6e3c9c69d6d738d38b8712638fb43cbf9d435ac03a",
    "4236f68fe88e374bca61aa524ef60a3b0f94721757aa8ddc593ce2e34d2f3faeea9d57f5cef3f22dccf5ce611d6ffe88b18bca0cdfa663b2f3299565c63991b1"
    "475c8a9f26902e5c757e1fe79c57f9f6632dbabc53416690c2e6e671b766da4941ef0c76d2830078f715b813a1b7",
    "" };

  nameCaptureInfo = NULL;
  emlrtNameCaptureMxArrayR2016a(data, 24312U, &nameCaptureInfo);
  return nameCaptureInfo;
}

/* End of code generation (_coder_Llamosi2016_info.c) */
