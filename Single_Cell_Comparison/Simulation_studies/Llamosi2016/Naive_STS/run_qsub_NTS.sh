for i in 50 150 250 350 500
do 
qsub -N Simulations_a -t 1-5 submit_script_Figa.sh $i 
sleep 10
done

#### Figure b
for i in 2 4 8 16 32
do
qsub -t 1-5 -hold_jid Simulations_a -N Simulations_b submit_script_Figb.sh $i 
sleep 10
done



######### Figure c
for i in 20 40 60 80 100
do
qsub -t 1-5 -hold_jid Simulations_b -N Simulations_c submit_script_Figc.sh $i 
sleep 10
done



######### Figure d
for i in 25 50 75 100
do
qsub -t 1-5 -hold_jid Simulations_c -N Simulations_d submit_script_Figd.sh $i 
sleep 10
done


