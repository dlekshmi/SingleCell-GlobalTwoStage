function []=get_images_scaled2(index)

if index==1

% data
data_folder='../Simulations/Simulated_Data/Data_Fig3a/';
folder_names={'500','350','250','150','50'};
% Results to NTS
dirname_NTS='./Pubs_Results/Data_Fig3a/';
dirname_GTS='../Simulations/SecondStage_Results/Data_Fig3a';
elseif index==2
data_folder='../Simulations/Simulated_Data/Data_Fig3b/'; 
folder_names={'2','4','8','16','32'};  
dirname_NTS='./Pubs_Results/Data_Fig3b/';
dirname_GTS='../Simulations/SecondStage_Results/Data_Fig3b';

elseif index==3
 data_folder='../Simulations/Simulated_Data/Data_Fig3c/';   
folder_names={'20','40','60','80','100'};
dirname_NTS='./Pubs_Results/Data_Fig3c/';
dirname_GTS='../Simulations/SecondStage_Results/Data_Fig3c';

else
data_folder='../Simulations/Simulated_Data/Data_Fig3d/';   
folder_names={'25','50','75','100'};%
dirname_NTS='./Pubs_Results/Data_Fig3d/';
dirname_GTS='../Simulations/SecondStage_Results/Data_Fig3d';
end
times=0:600;

if index==1
label_names=[500,350,250,150,50];
else
label_names=repmat(350,1,5);
end


med_adiffs=[];
q25_adiffs=[];
q75_adiffs=[];

for j=1:length(folder_names)
    folder_name=folder_names{j};
for i=1:5



% load FirstStage Reuslts
load(sprintf('%s/%s/plots_data/FirstStage-Result_%d.mat.mat',dirname_NTS,folder_name,i))
fm0=quantile(Model_prediction,0.025);
fm100=quantile(Model_prediction, 0.975);
fMedians=quantile(Model_prediction,0.5);

load(sprintf('%s/%s/plots_data/Robust-FirstStage-Result_%d.mat.mat',dirname_NTS,folder_name,i))
rfm0=quantile(Model_prediction,0.025);
rfm100=quantile(Model_prediction, 0.975);
rfMedians=quantile(Model_prediction,0.5);



% load GTS Reuslts
load(sprintf('%s/%s/plots/AR_plot_data_%d.mat',dirname_GTS,folder_name,i))
gm0=quantile(Model_prediction,0.025);
gm100=quantile(Model_prediction, 0.975);
gMedians=quantile(Model_prediction,0.5);


% Also do the same with the data
load(sprintf('%s/%s/Simulated_%d.mat',data_folder,folder_name,i))
data=reshape(table2array(Data(:,2)),101,label_names(j))';
dtime=unique(Data.TIME)';
dm0=quantile(data,0.025);
dm100=quantile(data, 0.975);
dMedians=quantile(data,0.5);




% SAVE FirstStage
h3=fill([0:600 fliplr(0:600)]./60,[fm0 fliplr(fm100)],[0,0,0]);
alpha 1
hold on
set(h3,'EdgeColor','None');
ylim([min([dm0,fm0,gm0]),max([gm100,fm100,dm100])]);
set(gca,'ytick',[],'xtick',[])
mkdir(sprintf('%s/%s/images_scaled-firststage',dirname_NTS,folder_name))
set(gcf, 'Units', 'normalized', 'Position', [0 0 1 1])
saveas(gcf,sprintf('%s/%s/images_scaled-firststage/predictions_%d.png',dirname_NTS,folder_name,i))

close 

% SAVE GTS
h3=fill([0:600 fliplr(0:600)]./60,[gm0 fliplr(gm100)],[0,0,0]);
alpha 1
hold on
set(h3,'EdgeColor','None');
ylim([min([dm0,fm0,gm0]),max([gm100,fm100,dm100])]);
set(gca,'ytick',[],'xtick',[])
mkdir(sprintf('%s/%s/images_scaled-secondstage',dirname_NTS,folder_name))
set(gcf, 'Units', 'normalized', 'Position', [0 0 1 1])
saveas(gcf,sprintf('%s/%s/images_scaled-secondstage/predictions_%d.png',dirname_NTS,folder_name,i))
close 

% SAVE FirstStage
h3=fill([0:600 fliplr(0:600)]./60,[rfm0 fliplr(rfm100)],[0,0,0]);
alpha 1
hold on
set(h3,'EdgeColor','None');
ylim([min([dm0,fm0,gm0]),max([gm100,fm100,dm100])]);
set(gca,'ytick',[],'xtick',[])
mkdir(sprintf('%s/%s/images_scaled-robustfirststage',dirname_NTS,folder_name))
set(gcf, 'Units', 'normalized', 'Position', [0 0 1 1])
saveas(gcf,sprintf('%s/%s/images_scaled-robustfirststage/predictions_%d.png',dirname_NTS,folder_name,i))

close 


% Save data
figure
h3=fill([dtime fliplr(dtime)]./60,[dm0 fliplr(dm100)],[0,0,0]);
alpha 1
hold on
set(h3,'EdgeColor','None');
ylim([min([dm0,fm0,gm0]),max([gm100,fm100,dm100])]);
set(gca,'ytick',[],'xtick',[])
mkdir(sprintf('%s/%s/images_scaled',dirname_NTS,folder_name))

set(gcf, 'Units', 'normalized', 'Position', [0 0 1 1])
saveas(gcf,sprintf('%s/%s/images_scaled/data_%d.png',dirname_NTS,folder_name,i))
close 


% absolute differences 
% med_adiffs=cat(1,med_adiffs,[nansum(abs(gMedians(ismember(times,dtime))-dMedians)),nansum(abs(fMedians(ismember(times,dtime))-dMedians)),nansum(abs(rfMedians(ismember(times,dtime))-dMedians)),nansum(abs(rnMedians(ismember(times,dtime))-dMedians)),nansum(abs(nMedians(ismember(times,dtime))-dMedians))]);
% 
% q75_adiffs=cat(1,q75_adiffs,[nansum(abs(gm100(ismember(times,dtime))-dm100)),nansum(abs(fm100(ismember(times,dtime))-dm100)),nansum(abs(rfm100(ismember(times,dtime))-dm100)),nansum(abs(rnm100(ismember(times,dtime))-dm100)),nansum(abs(nm100(ismember(times,dtime))-dm100))]);
% 
% q25_adiffs=cat(1,q25_adiffs,[nansum(abs(gm0(ismember(times,dtime))-dm0)),nansum(abs(fm0(ismember(times,dtime))-dm0)),nansum(abs(rfm0(ismember(times,dtime))-dm0)),nansum(abs(rnm0(ismember(times,dtime))-dm0)),nansum(abs(nm0(ismember(times,dtime))-dm0))]);


end

end
%save(sprintf('meds_%d.mat',index),'med_adiffs','q25_adiffs','q75_adiffs')
