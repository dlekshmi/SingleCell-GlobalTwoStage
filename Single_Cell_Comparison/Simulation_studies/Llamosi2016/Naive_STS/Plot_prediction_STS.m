%function []=Plot_prediction(folder,subfolder,residual_type0,trial)
function []=Plot_prediction(folder,subfolder,trial)

%%%------------------------------------------------------------------------
%
%   function Plot_prediction.m
%   Generates the predictions for the results from GTS. And saves it as an array, 
%   where each coloumn is the time and the rows are independent simulated trajectories. 

%   Tools needed: 
        % a) ODE SOLVER

%   Funtions, objective functions used: 
        % c) Model integration, and evaluating the error model  
%   
%   Lines that have to modified:
%       % a) the paths to ODE integrator, and model
        % b) Output directory
		% c) Path to GTS results
		% Number of simulations 
%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------

rng('shuffle') 										% Set seed
addpath(genpath('~/Repos/odeSD/'))					% USER MUST MODIFY
addpath(genpath('../Simulations/Model'))
%addpath(genpath('./Model/'))
% Output directory
                           % path to store the results (USER MUST MODIFY)
fname=sprintf('Result_%d.mat',trial);
%load(sprintf('Pubs_Results/%s/%s/%s',folder,subfolder,fname))
   

% Input file name
residual_type0='AR'
filename=sprintf('../Simulations/SecondStage_Results/%s/%s/%s_Result_%d.mat',folder,subfolder,residual_type0,trial)		% USER MUST MODIFY

% Number of simulated trajectories					
nsims=10000; 										% USER MUST MODIFY									
load(sprintf('./%s',filename));
int_opts=struct();
int_opts.abstol=1e-4;
int_opts.reltol=1e-4;

Model_prediction=[];

%% PLot GTS predictions
Dhat=D_nts;
betahat=beta_nts;
Dhat=round(Dhat,4) % Incase the matrix is too stiff to sample from. 
[Dhat,betahat]=robustcov(beta_i)
time=0:600;

for i=1:nsims
paras0=exp(mvnrnd(betahat,Dhat));

[f_t]=Run_Llamosi2016wosens(time,[paras0(1:2),paras0(3)],int_opts);
% noise model
f_t=f_t+sqrt(sigma2).*(theta_hat(1)+exp(theta_hat(2)).*f_t).*randn(size(f_t));
if(~isnan(f_t))
Model_prediction=cat(1,Model_prediction,f_t);
end
end

%% Save results, 										USER MUST MODIFY
outdir=sprintf('Pubs_Results/%s/%s/plots_data',folder,subfolder);
mkdir(sprintf(outdir))
save(sprintf('%s/Robust-FirstStage-%s.mat',outdir,fname),'time','Model_prediction','Dhat','betahat')
end

