index=1
if index==1

% data
data_folder='../Simulations/Simulated_Data/Data_Fig3a/';
folder_names={'500','350','250','150','50'};
label_names=[500,350,250,150,50];
% Results to NTS
dirname_NTS='./Pubs_Results/Data_Fig3a/';
dirname_GTS='../Simulations/SecondStage_Results/Data_Fig3a';
elseif index==2
data_folder='../Simulations/Simulated_Data/Data_Fig3b/'; 
folder_names={'2','4','8','16','32'};  
dirname_NTS='./Pubs_Results/Data_Fig3b/';
dirname_GTS='../Simulations/SecondStage_Results/Data_Fig3b';

elseif index==3
 data_folder='../Simulations/Simulated_Data/Data_Fig3c/';   
folder_names={'20','40','60','80','100'};
dirname_NTS='./Pubs_Results/Data_Fig3c/';
dirname_GTS='../Simulations/SecondStage_Results/Data_Fig3c';

else
data_folder='../Simulations/Simulated_Data/Data_Fig3d/';   
folder_names={'25','50','75','100'};%
dirname_NTS='./Pubs_Results/Data_Fig3d/';
dirname_GTS='../Simulations/SecondStage_Results/Data_Fig3d';
end



tosave=[];
% GTS
for j=1:length(folder_names)
    folder_name=folder_names{j};
for i=1:5
dat=imread(sprintf('%s/%s/images_scaled/data_%d.png',dirname_NTS,folder_name,i));
pred=imread(sprintf('%s/%s/images_scaled-secondstage/predictions_%d.png',dirname_NTS,folder_name,i));
dat=im2bw(imresize(dat,[400,400]));
pred=im2bw(imresize(pred,[400,400]));

intersectImg = dat & pred;
unionImg = pred | dat;
Jac_index=sum(intersectImg(:))/sum(unionImg(:));
tosave=cat(1,tosave,[label_names(j), i, Jac_index]);
end
end
tosave_secondstage=tosave;

% firststage
tosave=[];
for j=1:length(folder_names)
folder_name=folder_names{j};
for i=1:5
dat=imread(sprintf('%s/%s/images_scaled/data_%d.png',dirname_NTS,folder_name,i));
pred=imread(sprintf('%s/%s/images_scaled-firststage/predictions_%d.png',dirname_NTS,folder_name,i));
dat=im2bw(imresize(dat,[400,400]));
pred=im2bw(imresize(pred,[400,400]));
intersectImg = pred & dat;
unionImg = pred | dat;
Jac_index=sum(intersectImg(:))/sum(unionImg(:));
tosave=cat(1,tosave,[label_names(j), i, Jac_index]);
end
end
tosave_firststage=tosave;

% robust firststage
tosave=[];
for j=1:length(folder_names)
folder_name=folder_names{j};
for i=1:5
dat=imread(sprintf('%s/%s/images_scaled/data_%d.png',dirname_NTS,folder_name,i));
pred=imread(sprintf('%s/%s/images_scaled-robustfirststage/predictions_%d.png',dirname_NTS,folder_name,i));
dat=im2bw(imresize(dat,[400,400]));
pred=im2bw(imresize(pred,[400,400]));
intersectImg = pred & dat;
unionImg = pred | dat;
Jac_index=sum(intersectImg(:))/sum(unionImg(:));
tosave=cat(1,tosave,[label_names(j), i, Jac_index]);
end
end
tosave_robustfirststage=tosave;

% NTS-robust

tosave=[];
for j=1:length(folder_names)
folder_name=folder_names{j};
for i=1:5
dat=imread(sprintf('%s/%s/images_scaled/data_%d.png',dirname_NTS,folder_name,i));
pred=imread(sprintf('%s/%s/images_scaled-robust/predictions_%d.png',dirname_NTS,folder_name,i));
dat=im2bw(imresize(dat,[400,400]));
pred=im2bw(imresize(pred,[400,400]));
intersectImg = pred & dat;
unionImg = pred | dat;
Jac_index=sum(intersectImg(:))/sum(unionImg(:));
tosave=cat(1,tosave,[label_names(j), i, Jac_index]);
end
end
tosave_robust=tosave;

% NTS
tosave=[];
for j=1:length(folder_names)
folder_name=folder_names{j};
for i=1:5
dat=imread(sprintf('%s/%s/images_scaled/data_%d.png',dirname_NTS,folder_name,i));
pred=imread(sprintf('%s/%s/images_scaled/predictions_%d.png',dirname_NTS,folder_name,i));
dat=im2bw(imresize(dat,[400,400]));
pred=im2bw(imresize(pred,[400,400]));
intersectImg = pred & dat;
unionImg = pred | dat;
Jac_index=sum(intersectImg(:))/sum(unionImg(:));
tosave=cat(1,tosave,[label_names(j), i, Jac_index]);
end
end
tosave_NTS=tosave;


save('comp_pred-a.mat','tosave_NTS','tosave_secondstage','tosave_firststage','tosave_robust','tosave_robustfirststage');
