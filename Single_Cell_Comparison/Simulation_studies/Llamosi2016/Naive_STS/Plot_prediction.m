function []=Plot_prediction(folder,subfolder,trial)
%clear all 

rng('shuffle') %For reproducibility
addpath(genpath('~/PLOTS/'))
addpath(genpath('~/Repos/odeSD/'))
addpath(genpath('../Simulations/Model'))

fname=sprintf('Result_%d.mat',trial);
load(sprintf('Pubs_Results/%s/%s/%s',folder,subfolder,fname))

%% Plot NTS/ STS
params2=[];
Model_prediction=[];

Dhat_sts=Dhat_sts(1:3,1:3);
noise=betahat_sts(4:5);
betahat_sts=betahat_sts(1:3);
[Dhat_sts,betahat_sts]=robustcov(betai_sts(:,1:3));

int_opts=struct();
int_opts.abstol=1e-8;
int_opts.reltol=1e-4;

% simulate
time=0:600;
for i=1:10000
paras0=(mvnrnd(betahat_sts,Dhat_sts));
paras0=[exp(paras0(1:3))];
[f_t]=Run_Llamosi2016wosens(time,paras0,int_opts);
h=(noise(1)+exp(noise(2))*f_t).*randn(size(f_t));
f_t=f_t+h;
%[f_t]=Run_Hogmodelode(0:600,[paras0,exp(tau)]);
Model_prediction=cat(1,Model_prediction,f_t);
end

outdir=sprintf('Pubs_Results/%s/%s/plots_data',folder,subfolder);
mkdir(sprintf(outdir))
save(sprintf('%s/Robust-%s.mat',outdir,fname),'time','Model_prediction')
end

