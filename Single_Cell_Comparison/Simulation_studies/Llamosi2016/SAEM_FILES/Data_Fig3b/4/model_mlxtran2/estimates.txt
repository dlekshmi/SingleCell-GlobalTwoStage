          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.43917;   0.03810;      1.56;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.33594;   0.03500;      0.66;        NaN
    gm_pop;  -3.46510;   0.05421;      1.56;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.66783;   0.02849;      4.27;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.56791;   0.02795;      4.92;        NaN
  omega_gm;   0.91381;   0.04247;      4.65;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.22328;   0.06288;     28.16;        NaN
corr_km_gm;   0.79525;   0.02379;      2.99;        NaN
corr_gp_gm;  -0.74328;   0.02981;      4.01;        NaN
         a;  44.54627;   0.48083;      1.08;        NaN
         b;   0.35106;   0.00151;      0.43;        NaN
