          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.83814;   0.04467;      1.57;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.19860;   0.05139;      0.99;        NaN
    gm_pop;  -3.77206;   0.07087;      1.88;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.68981;   0.03724;      5.40;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.59982;   0.05450;      9.09;        NaN
  omega_gm;   0.92408;   0.07201;      7.79;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.05399;   0.10794;    199.95;        NaN
corr_km_gm;   0.75168;   0.04311;      5.74;        NaN
corr_gp_gm;  -0.66950;   0.06167;      9.21;        NaN
         a;  43.87185;   0.48979;      1.12;        NaN
         b;   1.14588;   0.00470;      0.41;        NaN
