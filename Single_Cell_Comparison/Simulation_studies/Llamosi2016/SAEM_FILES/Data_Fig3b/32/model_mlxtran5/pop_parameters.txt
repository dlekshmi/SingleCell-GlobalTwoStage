******************************************************************
*      model_mlxtran5.mlxtran
*      March 25, 2018 at 01:34:09
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.84        0.045           2    
kp_pop     :  -0.0545          -           -      
gp_pop     :     -5.2        0.051           1    
gm_pop     :    -3.77        0.071           2    
ltau_pop   :      3.4          -           -      

omega_km   :     0.69        0.037           5    
omega_kp   :        0          -           -      
omega_gp   :      0.6        0.054           9    
omega_gm   :    0.924        0.072           8    
omega_ltau :        0          -           -      
corr_km_gp :   -0.054         0.11         200    
corr_km_gm :    0.752        0.043           6    
corr_gp_gm :   -0.669        0.062           9    

a          :     43.9         0.49           1    
b          :     1.15       0.0047           0    

correlation matrix (IIV)
km      1       
gp  -0.05       1    
gm   0.75   -0.67       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.08       1    
gm_pop   0.68   -0.76       1 

Eigenvalues (min, max, max/min): 0.021  2.1  97

omega_km      1             
omega_gp  -0.02       1          
omega_gm   0.42    0.54       1       
a            -0      -0      -0       1    
b         -0.03   -0.03   -0.03   -0.04       1 

Eigenvalues (min, max, max/min): 0.3  1.7  5.5

corr_km_gp      1       
corr_km_gm  -0.67       1    
corr_gp_gm   0.78   -0.06       1 

Eigenvalues (min, max, max/min): 0.0027  2.1  7.7e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 1.47e+03 seconds. 
CPU time is 1.08e+04 seconds. 
