          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.63787;   0.04574;      1.73;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.24567;   0.04566;      0.87;        NaN
    gm_pop;  -3.54025;   0.06798;      1.92;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.72348;   0.03722;      5.14;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.59262;   0.04389;      7.41;        NaN
  omega_gm;   0.95288;   0.06415;      6.73;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;   0.01321;   0.09451;    715.20;        NaN
corr_km_gm;   0.74669;   0.03753;      5.03;        NaN
corr_gp_gm;  -0.63221;   0.05706;      9.03;        NaN
         a;  43.88333;   0.48629;      1.11;        NaN
         b;   0.92528;   0.00383;      0.41;        NaN
