          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.52825;   0.04356;      1.72;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.28150;   0.04489;      0.85;        NaN
    gm_pop;  -3.59704;   0.06420;      1.78;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.68865;   0.03565;      5.18;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.52883;   0.04688;      8.86;        NaN
  omega_gm;   0.83674;   0.06525;      7.80;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;   0.06547;   0.10614;    162.11;        NaN
corr_km_gm;   0.76013;   0.03900;      5.13;        NaN
corr_gp_gm;  -0.56200;   0.07510;     13.36;        NaN
         a;  43.72154;   0.48388;      1.11;        NaN
         b;   0.91694;   0.00380;      0.41;        NaN
