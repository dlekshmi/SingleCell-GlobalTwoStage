          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.38821;   0.03848;      1.61;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.29504;   0.03574;      0.67;        NaN
    gm_pop;  -3.55706;   0.05541;      1.56;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.63500;   0.03039;      4.79;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.45905;   0.03496;      7.62;        NaN
  omega_gm;   0.80523;   0.05042;      6.26;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.12395;   0.08816;     71.13;        NaN
corr_km_gm;   0.79341;   0.02971;      3.74;        NaN
corr_gp_gm;  -0.66497;   0.05165;      7.77;        NaN
         a;  43.15717;   0.47301;      1.10;        NaN
         b;   0.63297;   0.00266;      0.42;        NaN
