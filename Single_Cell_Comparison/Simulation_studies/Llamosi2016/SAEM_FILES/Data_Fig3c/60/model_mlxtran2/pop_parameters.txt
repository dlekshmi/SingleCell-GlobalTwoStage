******************************************************************
*      model_mlxtran2.mlxtran
*      March 06, 2018 at 15:53:14
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.39         0.034          1    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.33         0.034          1    
gm_pop     :    -3.54         0.046          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.626         0.025          4    
omega_kp   :        0           -          -      
omega_gp   :    0.601         0.025          4    
omega_gm   :    0.828         0.034          4    
omega_ltau :        0           -          -      
corr_km_gp :   0.0103         0.057        559    
corr_km_gm :    0.728         0.027          4    
corr_gp_gm :   -0.632         0.034          5    

a          :     64.5          0.64          1    
b          :    0.101       0.00054          1    

correlation matrix (IIV)
km      1       
gp   0.01       1    
gm   0.73   -0.63       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.01       1    
gm_pop   0.71   -0.65       1 

Eigenvalues (min, max, max/min): 0.032  2  62

omega_km      1             
omega_gp     -0       1          
omega_gm    0.5    0.42       1       
a            -0      -0      -0       1    
b            -0      -0      -0   -0.37       1 

Eigenvalues (min, max, max/min): 0.34  1.7  4.8

corr_km_gp      1       
corr_km_gm  -0.64       1    
corr_gp_gm   0.72    0.06       1 

Eigenvalues (min, max, max/min): 0.0054  1.9  3.6e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 4.6e+03 seconds. 
CPU time is 1.96e+04 seconds. 
