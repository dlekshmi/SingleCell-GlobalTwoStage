******************************************************************
*      model_mlxtran4.mlxtran
*      March 06, 2018 at 18:47:34
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.41         0.039          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.27          0.03          1    
gm_pop     :    -3.55         0.046          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.726         0.028          4    
omega_kp   :        0           -          -      
omega_gp   :     0.52         0.022          4    
omega_gm   :    0.831         0.034          4    
omega_ltau :        0           -          -      
corr_km_gp :     0.15         0.057         38    
corr_km_gm :    0.794         0.022          3    
corr_gp_gm :   -0.439         0.047         11    

a          :       68          0.68          1    
b          :    0.105       0.00056          1    

correlation matrix (IIV)
km      1       
gp   0.15       1    
gm   0.79   -0.44       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.14       1    
gm_pop   0.77   -0.48       1 

Eigenvalues (min, max, max/min): 0.025  1.9  75

omega_km      1             
omega_gp   0.02       1          
omega_gm   0.59    0.22       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.37       1 

Eigenvalues (min, max, max/min): 0.37  1.6  4.4

corr_km_gp      1       
corr_km_gm  -0.46       1    
corr_gp_gm   0.79    0.17       1 

Eigenvalues (min, max, max/min): 0.0032  1.8  5.7e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 4.89e+03 seconds. 
CPU time is 2.12e+04 seconds. 
