******************************************************************
*      model_mlxtran1.mlxtran
*      March 06, 2018 at 14:34:01
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.37         0.035          1    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.31         0.036          1    
gm_pop     :    -3.61         0.048          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.648         0.025          4    
omega_kp   :        0           -          -      
omega_gp   :    0.641         0.027          4    
omega_gm   :    0.856         0.035          4    
omega_ltau :        0           -          -      
corr_km_gp :   0.0299         0.057        192    
corr_km_gm :    0.752         0.025          3    
corr_gp_gm :   -0.597         0.037          6    

a          :     63.6          0.64          1    
b          :    0.112       0.00058          1    

correlation matrix (IIV)
km      1       
gp   0.03       1    
gm   0.75    -0.6       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.03       1    
gm_pop   0.73   -0.62       1 

Eigenvalues (min, max, max/min): 0.027  1.9  72

omega_km      1             
omega_gp     -0       1          
omega_gm   0.53    0.38       1       
a            -0      -0      -0       1    
b            -0      -0      -0   -0.34       1 

Eigenvalues (min, max, max/min): 0.35  1.7  4.8

corr_km_gp      1       
corr_km_gm  -0.61       1    
corr_gp_gm   0.74    0.08       1 

Eigenvalues (min, max, max/min): 0.0038  1.9  5.1e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 5.38e+03 seconds. 
CPU time is 2.38e+04 seconds. 
