          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.38666;   0.03631;      1.52;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.32048;   0.03536;      0.66;        NaN
    gm_pop;  -3.55457;   0.04780;      1.34;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.66744;   0.02607;      3.91;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.62797;   0.02605;      4.15;        NaN
  omega_gm;   0.85882;   0.03510;      4.09;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;   0.05180;   0.05720;    110.43;        NaN
corr_km_gm;   0.76788;   0.02387;      3.11;        NaN
corr_gp_gm;  -0.56296;   0.03906;      6.94;        NaN
         a;  63.64827;   0.63816;      1.00;        NaN
         b;   0.11127;   0.00058;      0.52;        NaN
