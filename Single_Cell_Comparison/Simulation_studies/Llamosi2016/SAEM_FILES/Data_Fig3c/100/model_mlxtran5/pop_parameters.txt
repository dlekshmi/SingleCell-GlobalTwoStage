******************************************************************
*      model_mlxtran5.mlxtran
*      March 07, 2018 at 10:03:29
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.37        0.036           2    
kp_pop     :  -0.0545          -           -      
gp_pop     :    -5.29        0.033           1    
gm_pop     :    -3.61        0.047           1    
ltau_pop   :      3.4          -           -      

omega_km   :    0.656        0.026           4    
omega_kp   :        0          -           -      
omega_gp   :    0.573        0.024           4    
omega_gm   :    0.842        0.035           4    
omega_ltau :        0          -           -      
corr_km_gp :   0.0529        0.058         110    
corr_km_gm :    0.775        0.024           3    
corr_gp_gm :   -0.545        0.041           8    

a          :     65.3         0.66           1    
b          :    0.117       0.0006           1    

correlation matrix (IIV)
km      1       
gp   0.05       1    
gm   0.77   -0.54       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.05       1    
gm_pop   0.75   -0.58       1 

Eigenvalues (min, max, max/min): 0.029  1.9  67

omega_km      1             
omega_gp      0       1          
omega_gm   0.56    0.33       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.33       1 

Eigenvalues (min, max, max/min): 0.35  1.6  4.7

corr_km_gp      1       
corr_km_gm  -0.56       1    
corr_gp_gm   0.77    0.09       1 

Eigenvalues (min, max, max/min): 0.0043  1.9  4.4e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 4.78e+03 seconds. 
CPU time is 1.76e+04 seconds. 
