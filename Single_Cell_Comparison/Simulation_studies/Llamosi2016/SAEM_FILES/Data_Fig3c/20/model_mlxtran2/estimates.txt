          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.37205;   0.03544;      1.49;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.30371;   0.03056;      0.58;        NaN
    gm_pop;  -3.55321;   0.04542;      1.28;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.65281;   0.02543;      3.90;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.54134;   0.02252;      4.16;        NaN
  omega_gm;   0.81857;   0.03326;      4.06;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.04409;   0.05722;    129.76;        NaN
corr_km_gm;   0.75261;   0.02490;      3.31;        NaN
corr_gp_gm;  -0.65455;   0.03271;      5.00;        NaN
         a;  62.67463;   0.62087;      0.99;        NaN
         b;   0.09093;   0.00049;      0.54;        NaN
