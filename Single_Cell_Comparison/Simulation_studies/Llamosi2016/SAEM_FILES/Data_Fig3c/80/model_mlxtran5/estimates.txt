          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.36548;   0.03617;      1.53;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.28664;   0.03248;      0.61;        NaN
    gm_pop;  -3.61686;   0.04683;      1.29;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.66531;   0.02590;      3.89;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.56802;   0.02418;      4.26;        NaN
  omega_gm;   0.83729;   0.03442;      4.11;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;   0.03486;   0.05812;    166.73;        NaN
corr_km_gm;   0.78967;   0.02210;      2.80;        NaN
corr_gp_gm;  -0.53023;   0.04175;      7.87;        NaN
         a;  66.18473;   0.66192;      1.00;        NaN
         b;   0.11013;   0.00057;      0.52;        NaN
