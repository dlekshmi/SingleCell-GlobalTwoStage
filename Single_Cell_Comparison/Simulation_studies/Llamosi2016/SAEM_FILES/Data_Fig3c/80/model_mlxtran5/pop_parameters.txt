******************************************************************
*      model_mlxtran5.mlxtran
*      March 07, 2018 at 02:58:07
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.37         0.036          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.29         0.032          1    
gm_pop     :    -3.62         0.047          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.665         0.026          4    
omega_kp   :        0           -          -      
omega_gp   :    0.568         0.024          4    
omega_gm   :    0.837         0.034          4    
omega_ltau :        0           -          -      
corr_km_gp :   0.0349         0.058        167    
corr_km_gm :     0.79         0.022          3    
corr_gp_gm :    -0.53         0.042          8    

a          :     66.2          0.66          1    
b          :     0.11       0.00057          1    

correlation matrix (IIV)
km      1       
gp   0.03       1    
gm   0.79   -0.53       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.03       1    
gm_pop   0.76   -0.57       1 

Eigenvalues (min, max, max/min): 0.034  1.9  58

omega_km      1             
omega_gp     -0       1          
omega_gm   0.58    0.31       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.35       1 

Eigenvalues (min, max, max/min): 0.34  1.7  4.8

corr_km_gp      1       
corr_km_gm  -0.55       1    
corr_gp_gm   0.78    0.09       1 

Eigenvalues (min, max, max/min): 0.0063  1.9  3.1e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 4.56e+03 seconds. 
CPU time is 1.94e+04 seconds. 
