;model definition
[LONGITUDINAL]
INPUT:
parameter={km, kp, gp, gm, ltau}



EQUATION:
t0=0
m_0=0
Pp_0=0
u_0=1e-4

tau=exp(ltau)

if t>=30+tau&t<=44+tau
Inp=30+tau
elseif t>=60+tau&t<=74+tau
Inp=60+tau
elseif t>=90+tau&t<=104+tau
Inp=90+tau
elseif t>=120+tau&t<=134+tau
Inp=120+tau
elseif t>=150+tau &t<=164+tau
Inp=150+tau
elseif t>=180+tau &t<=194+tau
Inp=180+tau
elseif t>=210+tau &t<=224+tau
Inp=210+tau
elseif t>=241.9064+tau &t<=255.9064+tau
Inp=241.9064+tau
elseif t>=290.4352+tau&t<=304.4352+tau
Inp=290.4352+tau
elseif t>=372.1597+tau &t<=386.1597+tau
Inp=372.1597+tau
elseif t>=455.319+tau&t<=469.319+tau
Inp=455.319+tau
elseif t>=510.8225+tau&t<=524.8225+tau
Inp=510.8225+tau
elseif t>=546.0478+tau&t<=560.0478+tau
Inp=546.0478+tau
else
Inp=0
end



if Inp>0
if t<(Inp+2)
x=0
elseif t>(Inp+2) & t<=(Inp+3)
x=1*(t-Inp-2)
elseif t>(Inp+3) & t<=(Inp+10)
x=1
elseif t>(Inp+10) & t<=(Inp+14)
x=-0.25*(t-Inp-14)
else
x=0
end
else
x=0
end



odeType=stiff
ddt_u = 0.3968*x- 0.9225*u  ;Actual input
ddt_m = exp(km)*u - exp(gm)*m    ;mRNA
ddt_Pp =exp(kp)*m -exp(gp)*Pp

if t>tau
 f=exp(-exp(gp)*tau)*Pp
else
 f=0
end




OUTPUT:
output = {f}
