******************************************************************
*      model_mlxtran5.mlxtran
*      March 06, 2018 at 13:01:47
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.33         0.037          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.28          0.03          1    
gm_pop     :    -3.65         0.045          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.682         0.026          4    
omega_kp   :        0           -          -      
omega_gp   :    0.529         0.022          4    
omega_gm   :    0.817         0.033          4    
omega_ltau :        0           -          -      
corr_km_gp :   0.0487         0.058        119    
corr_km_gm :    0.793         0.022          3    
corr_gp_gm :   -0.526         0.042          8    

a          :     63.9          0.64          1    
b          :    0.103       0.00054          1    

correlation matrix (IIV)
km      1       
gp   0.05       1    
gm   0.79   -0.53       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.05       1    
gm_pop   0.77   -0.56       1 

Eigenvalues (min, max, max/min): 0.026  1.9  74

omega_km      1             
omega_gp     -0       1          
omega_gm   0.59    0.31       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.36       1 

Eigenvalues (min, max, max/min): 0.34  1.7  5

corr_km_gp      1       
corr_km_gm  -0.54       1    
corr_gp_gm   0.79    0.09       1 

Eigenvalues (min, max, max/min): 0.0038  1.9  5e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 5.33e+03 seconds. 
CPU time is 2.36e+04 seconds. 
