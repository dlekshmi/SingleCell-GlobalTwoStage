******************************************************************
*      model_mlxtran1.mlxtran
*      March 02, 2018 at 19:40:51
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.36         0.036          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.26          0.03          1    
gm_pop     :    -3.63         0.049          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.663         0.026          4    
omega_kp   :        0           -          -      
omega_gp   :    0.528         0.022          4    
omega_gm   :    0.892         0.036          4    
omega_ltau :        0           -          -      
corr_km_gp :   -0.191         0.056         29    
corr_km_gm :     0.79         0.022          3    
corr_gp_gm :   -0.722         0.028          4    

a          :     62.6          0.62          1    
b          :   0.0869       0.00047          1    

correlation matrix (IIV)
km      1       
gp  -0.19       1    
gm   0.79   -0.72       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.17       1    
gm_pop   0.77   -0.74       1 

Eigenvalues (min, max, max/min): 0.02  2.1  1.1e+02

omega_km      1             
omega_gp   0.02       1          
omega_gm   0.58    0.54       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.39       1 

Eigenvalues (min, max, max/min): 0.22  1.8  8.3

corr_km_gp      1       
corr_km_gm  -0.72       1    
corr_gp_gm   0.78   -0.14       1 

Eigenvalues (min, max, max/min): 0.003  2.1  7e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 8.82e+03 seconds. 
CPU time is 2.47e+04 seconds. 
