******************************************************************
*      model_mlxtran4.mlxtran
*      March 03, 2018 at 02:12:41
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :      2.4         0.039          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.28         0.029          1    
gm_pop     :    -3.54         0.046          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.716         0.028          4    
omega_kp   :        0           -          -      
omega_gp   :     0.51         0.021          4    
omega_gm   :    0.828         0.033          4    
omega_ltau :        0           -          -      
corr_km_gp :   0.0448         0.057        127    
corr_km_gm :    0.768         0.023          3    
corr_gp_gm :   -0.574         0.038          7    

a          :     62.2          0.61          1    
b          :   0.0867       0.00047          1    

correlation matrix (IIV)
km      1       
gp   0.04       1    
gm   0.77   -0.57       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.04       1    
gm_pop   0.75    -0.6       1 

Eigenvalues (min, max, max/min): 0.02  1.9  99

omega_km      1             
omega_gp      0       1          
omega_gm   0.57    0.35       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.39       1 

Eigenvalues (min, max, max/min): 0.33  1.7  5

corr_km_gp      1       
corr_km_gm  -0.58       1    
corr_gp_gm   0.77    0.07       1 

Eigenvalues (min, max, max/min): 0.0022  1.9  8.9e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 7.22e+03 seconds. 
CPU time is 1.92e+04 seconds. 
