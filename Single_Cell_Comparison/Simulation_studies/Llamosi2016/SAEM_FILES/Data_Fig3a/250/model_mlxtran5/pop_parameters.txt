******************************************************************
*      model_mlxtran5.mlxtran
*      March 02, 2018 at 17:11:54
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :      2.3         0.044          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.28         0.031          1    
gm_pop     :    -3.64         0.055          2    
ltau_pop   :      3.4           -          -      

omega_km   :    0.688         0.031          5    
omega_kp   :        0           -          -      
omega_gp   :    0.449         0.023          5    
omega_gm   :    0.837          0.04          5    
omega_ltau :        0           -          -      
corr_km_gp :  -0.0908         0.069         76    
corr_km_gm :    0.799         0.025          3    
corr_gp_gm :   -0.625         0.042          7    

a          :     62.3          0.72          1    
b          :   0.0863       0.00056          1    

correlation matrix (IIV)
km      1       
gp  -0.09       1    
gm    0.8   -0.62       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.08       1    
gm_pop   0.78   -0.65       1 

Eigenvalues (min, max, max/min): 0.028  2.1  74

omega_km      1             
omega_gp   0.01       1          
omega_gm    0.6    0.42       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.39       1 

Eigenvalues (min, max, max/min): 0.27  1.7  6.4

corr_km_gp      1       
corr_km_gm  -0.63       1    
corr_gp_gm    0.8   -0.04       1 

Eigenvalues (min, max, max/min): 0.0051  2  4e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 5.74e+03 seconds. 
CPU time is 1.54e+04 seconds. 
