******************************************************************
*      model_mlxtran4.mlxtran
*      March 02, 2018 at 15:34:30
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.35         0.048          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.32         0.035          1    
gm_pop     :    -3.55         0.061          2    
ltau_pop   :      3.4           -          -      

omega_km   :    0.748         0.034          5    
omega_kp   :        0           -          -      
omega_gp   :    0.516         0.026          5    
omega_gm   :     0.94         0.045          5    
omega_ltau :        0           -          -      
corr_km_gp :  -0.0994         0.068         68    
corr_km_gm :    0.813         0.023          3    
corr_gp_gm :   -0.632         0.041          6    

a          :     62.5          0.73          1    
b          :   0.0861       0.00056          1    

correlation matrix (IIV)
km      1       
gp   -0.1       1    
gm   0.81   -0.63       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.09       1    
gm_pop    0.8   -0.65       1 

Eigenvalues (min, max, max/min): 0.017  2.1  1.2e+02

omega_km      1             
omega_gp   0.01       1          
omega_gm   0.63    0.42       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0    -0.4       1 

Eigenvalues (min, max, max/min): 0.25  1.8  7.2

corr_km_gp      1       
corr_km_gm  -0.63       1    
corr_gp_gm   0.81   -0.07       1 

Eigenvalues (min, max, max/min): 0.002  2.1  1.1e+03


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 5.46e+03 seconds. 
CPU time is 1.46e+04 seconds. 
