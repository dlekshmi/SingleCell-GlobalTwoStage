******************************************************************
*      model_mlxtran1.mlxtran
*      March 02, 2018 at 10:40:04
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.38         0.043          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.27         0.034          1    
gm_pop     :     -3.6         0.053          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.677         0.031          5    
omega_kp   :        0           -          -      
omega_gp   :      0.5         0.025          5    
omega_gm   :    0.812         0.039          5    
omega_ltau :        0           -          -      
corr_km_gp :   0.0403         0.068        169    
corr_km_gm :    0.759         0.029          4    
corr_gp_gm :   -0.586         0.045          8    

a          :     63.2          0.74          1    
b          :    0.086       0.00056          1    

correlation matrix (IIV)
km      1       
gp   0.04       1    
gm   0.76   -0.59       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.04       1    
gm_pop   0.74   -0.61       1 

Eigenvalues (min, max, max/min): 0.021  1.9  92

omega_km      1             
omega_gp      0       1          
omega_gm   0.55    0.37       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.39       1 

Eigenvalues (min, max, max/min): 0.34  1.7  4.9

corr_km_gp      1       
corr_km_gm  -0.59       1    
corr_gp_gm   0.76    0.07       1 

Eigenvalues (min, max, max/min): 0.0025  1.9  7.8e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 5.05e+03 seconds. 
CPU time is 1.33e+04 seconds. 
