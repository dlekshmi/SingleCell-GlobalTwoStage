******************************************************************
*      model_mlxtran5.mlxtran
*      March 01, 2018 at 16:53:11
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.31         0.053          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.25         0.045          1    
gm_pop     :    -3.66         0.068          2    
ltau_pop   :      3.4           -          -      

omega_km   :    0.644         0.038          6    
omega_kp   :        0           -          -      
omega_gp   :    0.519         0.034          6    
omega_gm   :    0.805          0.05          6    
omega_ltau :        0           -          -      
corr_km_gp :   0.0201         0.088        439    
corr_km_gm :     0.73         0.041          6    
corr_gp_gm :   -0.638         0.052          8    

a          :     63.3          0.95          2    
b          :   0.0855       0.00072          1    

correlation matrix (IIV)
km      1       
gp   0.02       1    
gm   0.73   -0.64       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.03       1    
gm_pop    0.7   -0.66       1 

Eigenvalues (min, max, max/min): 0.02  2  96

omega_km      1             
omega_gp     -0       1          
omega_gm   0.49    0.43       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0    -0.4       1 

Eigenvalues (min, max, max/min): 0.34  1.7  4.8

corr_km_gp      1       
corr_km_gm  -0.64       1    
corr_gp_gm   0.73    0.05       1 

Eigenvalues (min, max, max/min): 0.0023  1.9  8.4e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 3.19e+03 seconds. 
CPU time is 8.09e+03 seconds. 
