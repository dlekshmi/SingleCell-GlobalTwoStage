          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.35674;   0.05422;      2.30;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.30726;   0.04689;      0.88;        NaN
    gm_pop;  -3.55972;   0.06441;      1.81;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.65558;   0.03882;      5.92;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.54511;   0.03445;      6.32;        NaN
  omega_gm;   0.75712;   0.04728;      6.24;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;   0.13086;   0.08580;     65.57;        NaN
corr_km_gm;   0.67814;   0.04699;      6.93;        NaN
corr_gp_gm;  -0.59505;   0.05628;      9.46;        NaN
         a;  63.36007;   0.95067;      1.50;        NaN
         b;   0.08525;   0.00072;      0.85;        NaN
