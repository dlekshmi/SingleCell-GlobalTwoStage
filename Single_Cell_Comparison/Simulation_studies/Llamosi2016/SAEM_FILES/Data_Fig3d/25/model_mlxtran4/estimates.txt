          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.38795;   0.03835;      1.61;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.26938;   0.02950;      0.56;        NaN
    gm_pop;  -3.56192;   0.04546;      1.28;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.70058;   0.02765;      3.95;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.51162;   0.02201;      4.30;        NaN
  omega_gm;   0.80888;   0.03364;      4.16;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;   0.06693;   0.05887;     87.96;        NaN
corr_km_gm;   0.75033;   0.02542;      3.39;        NaN
corr_gp_gm;  -0.57867;   0.03930;      6.79;        NaN
         a;  62.15914;   0.66093;      1.06;        NaN
         b;   0.08665;   0.00051;      0.58;        NaN
