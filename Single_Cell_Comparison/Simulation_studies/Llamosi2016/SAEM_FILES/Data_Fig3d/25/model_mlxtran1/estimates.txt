          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.33773;   0.03691;      1.58;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.29397;   0.02999;      0.57;        NaN
    gm_pop;  -3.61449;   0.04829;      1.34;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.65804;   0.02693;      4.09;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.50961;   0.02275;      4.46;        NaN
  omega_gm;   0.85370;   0.03588;      4.20;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.13272;   0.06024;     45.39;        NaN
corr_km_gm;   0.78412;   0.02341;      2.99;        NaN
corr_gp_gm;  -0.68229;   0.03263;      4.78;        NaN
         a;  62.81296;   0.66641;      1.06;        NaN
         b;   0.08700;   0.00051;      0.59;        NaN
