          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.33161;   0.03849;      1.65;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.26060;   0.03010;      0.57;        NaN
    gm_pop;  -3.65752;   0.04757;      1.30;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.68902;   0.02802;      4.07;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.51344;   0.02277;      4.43;        NaN
  omega_gm;   0.83910;   0.03533;      4.21;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.04974;   0.06053;    121.68;        NaN
corr_km_gm;   0.79254;   0.02257;      2.85;        NaN
corr_gp_gm;  -0.61784;   0.03740;      6.05;        NaN
         a;  61.76609;   0.65467;      1.06;        NaN
         b;   0.08739;   0.00051;      0.58;        NaN
