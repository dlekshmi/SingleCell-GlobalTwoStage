******************************************************************
*      model_mlxtran3.mlxtran
*      March 04, 2018 at 19:43:56
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.36         0.037          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.31         0.029          1    
gm_pop     :    -3.55         0.046          1    
ltau_pop   :      3.4           -          -      

omega_km   :     0.67         0.027          4    
omega_kp   :        0           -          -      
omega_gp   :    0.493         0.022          4    
omega_gm   :      0.8         0.034          4    
omega_ltau :        0           -          -      
corr_km_gp :   0.0276         0.062        224    
corr_km_gm :    0.777         0.024          3    
corr_gp_gm :    -0.57         0.042          7    

a          :     61.4           0.7          1    
b          :   0.0866       0.00055          1    

correlation matrix (IIV)
km      1       
gp   0.03       1    
gm   0.78   -0.57       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.01       1    
gm_pop   0.77    -0.6       1 

Eigenvalues (min, max, max/min): 0.024  2  82

omega_km      1             
omega_gp     -0       1          
omega_gm   0.58    0.35       1       
a            -0      -0      -0       1    
b         -0.01   -0.01   -0.01   -0.39       1 

Eigenvalues (min, max, max/min): 0.32  1.7  5.3

corr_km_gp      1       
corr_km_gm  -0.58       1    
corr_gp_gm   0.79    0.04       1 

Eigenvalues (min, max, max/min): 0.0031  2  6.3e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 5.68e+03 seconds. 
CPU time is 1.66e+04 seconds. 
