          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.41591;   0.03987;      1.65;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.27939;   0.03184;      0.60;        NaN
    gm_pop;  -3.52148;   0.04799;      1.36;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.72797;   0.02876;      3.95;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.53314;   0.02402;      4.51;        NaN
  omega_gm;   0.83893;   0.03601;      4.29;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;   0.07111;   0.06108;     85.90;        NaN
corr_km_gm;   0.75547;   0.02566;      3.40;        NaN
corr_gp_gm;  -0.57073;   0.04141;      7.26;        NaN
         a;  62.69271;   0.73447;      1.17;        NaN
         b;   0.08642;   0.00054;      0.62;        NaN
