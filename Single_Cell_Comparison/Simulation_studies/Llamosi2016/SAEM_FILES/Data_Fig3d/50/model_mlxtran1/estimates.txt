          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.32832;   0.03862;      1.66;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.31768;   0.03008;      0.57;        NaN
    gm_pop;  -3.60013;   0.04725;      1.31;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.66505;   0.02868;      4.31;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.49902;   0.02327;      4.66;        NaN
  omega_gm;   0.81081;   0.03581;      4.42;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.02573;   0.06428;    249.81;        NaN
corr_km_gm;   0.76888;   0.02594;      3.37;        NaN
corr_gp_gm;  -0.62591;   0.03892;      6.22;        NaN
         a;  62.66904;   0.71852;      1.15;        NaN
         b;   0.08676;   0.00055;      0.63;        NaN
