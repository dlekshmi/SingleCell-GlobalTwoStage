          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.34679;   0.03642;      1.55;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.29974;   0.03149;      0.59;        NaN
    gm_pop;  -3.57679;   0.04674;      1.31;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.64852;   0.02679;      4.13;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.50747;   0.02441;      4.81;        NaN
  omega_gm;   0.80104;   0.03585;      4.48;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.01641;   0.06571;    400.41;        NaN
corr_km_gm;   0.76767;   0.02546;      3.32;        NaN
corr_gp_gm;  -0.61177;   0.04117;      6.73;        NaN
         a;  61.79606;   0.76581;      1.24;        NaN
         b;   0.08596;   0.00060;      0.70;        NaN
