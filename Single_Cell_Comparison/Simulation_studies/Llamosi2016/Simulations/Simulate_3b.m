%% Simulate data for 3b


clear all
addpath(genpath('~/Repos/odeSD/'));% ode integrator
addpath(genpath('./Model/'))
% Load the data 
seeds=123456009;

for J=1:5
load(sprintf('./Simulated_Data/Data_Fig3a/350/Simulated_%d.mat',J))

tau=3.4;
cellnum=350;
sigmas=[2,4,8,16,32];
labels={'2','4','8','16','32'};
int_opts.abstol=1e-6;
int_opts.reltol=1e-3;
for ii=1:length(sigmas)
sigma=sigmas(ii);  

Model_prediction2=[];
TIME=[];
ID=[];
Y=[];

for i=1:cellnum

paras0=p(i,:);
[f_t]=Run_Llamosi2016(0:6:600,[paras0(1:2),paras0(3)],int_opts);
ID=cat(1,ID,repmat(i,length(f_t),1));
TIME=cat(1,TIME,[0:6:600]');

s_2=seeds;
rng(s_2)
Y=cat(1,Y,[f_t'+(e_hat(1)+sigma.*exp(e_hat(2))*f_t').*randn(size(f_t'))]);
Model_prediction2=cat(1,Model_prediction2,f_t');
seeds=seeds+1;

end
Y(Y<0)=0;                         % added to avoid negative values in data!
Data=table(ID,Y,TIME);            


mkdir(sprintf('Simulated_Data/Data_Fig3b/%s',labels{ii}))


save(sprintf('./Simulated_Data/Data_Fig3b/%s/Simulated_%d.mat',labels{ii},J),'p','Data','s_2','s_2','sigma','e_hat')%end
writetable(Data,sprintf('./Simulated_Data/Data_Fig3b/%s/Simulated%d.txt',labels{ii},J))

end
%% PLOTS
end

