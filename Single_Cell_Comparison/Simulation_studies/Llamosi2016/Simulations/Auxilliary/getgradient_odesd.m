function [dydp,d2ydp2,noisefuns,C_inv,Hessian,f_t, gradients_states]=getgradient_odesd(t,p0,ehat,d,sigma2,int_opts)
%%%------------------------------------------------------------------------
%
%   function getgradient_odesd.m
% 	Gets sensitivity of the ODE model and calculates the gradients of the response
% 	with respect to the kinetic parameters. Note we are not caring about the parameters
% 	that are assumed fixed. Then the sensitivity is used to get the FIM. 
%  	y=exp(-gp*tau)*f_t_tau, then based on chain rule
% 	dy_dp=exp(-gp*tau)*df_dp+d{exp(gp*tau)}*f

%   Tools needed: 
        % a) ODE SOLVER

%  INPUT==> t: Time values in experiment,
%		  	p0: kinetic parameters
% 		  ehat: Noise parameters, theta in the model
%			d: data
% 		sigma2: sigma^2
%		int_opts: Options passed to integrator, structure.

%  OUTPUT==> gradients: the gradients wrt parameters
%			 noisefuns:	the 'g' in manuscript
%			 C_inv:		the FIM
%            Hessian: The Hessin using second order sensitivities.

%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------


% Parameters passed 
k_m=p0(1); %mRNA production
g_m=p0(2); %mRNA degradation
g_p=p0(3); %protein degradation
tau=exp(3.4);


f_name='Llamosi2016_o2';

% Same options as used for calculating the ODE trajectories
options=odeset('AbsTol',int_opts.abstol,'RelTol',int_opts.reltol);

addon=[0 2 3 10 14];
t_in=[30  60.0000   90.0000  120.0000  150.0000  180.0000  210.0000  241.9064  290.4352  372.1597  455.3190 510.8225  546.0478];
t0=0;
x0=[1.0e-10 1.0e-10 0 repmat(1e-10,1,9)]';
times=t0;
xs=[0,0,0,0];
xs2=zeros(1,9);
ss=x0';
inp=30;

% Iterate over the input times 

for i=1:length(t_in)

	for j=1:length(addon)

	[T1 ,X1,S1]=odeSD_wrapper_hybrid(f_name,linspace(t0, t_in(i)+addon(j)+tau),x0,options,[k_m,g_m,g_p,inp+tau]');

	t0=t_in(i)+addon(j)+tau;
	inp=t_in(i);
	x0=X1(end,:)'; % Set the intial condition for next input time
    s_x0=squeeze(S1(:,:,end)); % initial conditions for sensititivites 
    
    % concatenate results
	times=cat(1,times,T1(2:end)');  % Concatenate times.
	ss=cat(1,ss,X1(2:end,:));% concatenate states
	sens=squeeze( S1 (2 ,: ,:))';% get state sensitivities of the protein 
    xs2=cat(1,xs2,[squeeze(S1(5,1:3,2:end))',squeeze(S1(8,1:3,2:end))',squeeze(S1(11,1:3,2:end))']); % concatenate second order sensitivities for the second state
	xs=cat(1,xs,sens(2:end,:));% concatenate the state sentitivities for second state
	
    % set options for the next round of integration
	options=struct('AbsTol',int_opts.abstol,'RelTol',int_opts.reltol,'initialSensitivities',s_x0);

	end
end
    	[T1 ,X1,S1]=odeSD_wrapper_hybrid(f_name,linspace(546.0478+14+tau, 650),x0,options,[k_m,g_m,g_p,inp+tau]');
    	
        % concatenate results
   	    times=cat(1,times,T1(2:end)'); % times
    	ss=cat(1,ss,X1(2:end,:)); % states (second state)
    	sens=squeeze( S1 (2 ,: ,:))';%  first order sensitivities of the second state
        xs2=cat(1,xs2,[squeeze(S1(5,1:3,2:end))',squeeze(S1(8,1:3,2:end))',squeeze(S1(11,1:3,2:end))']); % second order  sensitivities. 
    	xs=cat(1,xs,sens(2:end,:));  % concatenate the first order sesntititivties 

% Observation sensitivities
sensitivities_1=interp1(times,xs(:,1),t);
sensitivities_2=interp1(times,xs(:,2),t);
sensitivities_3=interp1(times,xs(:,3),t);
pr=interp1(times,ss(:,2),t);

% First order sensitivities
gradients_states=[sensitivities_1.*k_m,sensitivities_2.*g_m,sensitivities_3.*g_p];
dydp=[gradients_states(:,1)*exp(-g_p*tau), gradients_states(:,2)*exp(-g_p*tau),gradients_states(:,3)*exp(-g_p*tau)+pr.*(-tau)*exp(-g_p*tau)*g_p];


% Second order sensitivities
d2xdp2=interp1(times,xs2,t);
d2ydp2=zeros(length(p0),length(p0),length(t));

for i=1:length(t)
                 %exp(-gp*tau)*diff(x(km, gm, gp), km, km)
    d2ydp2(1,1,i)=exp(-g_p*tau)*d2xdp2(i,1).*p0(1).*p0(1);  
                % exp(-gp*tau)*diff(x(km, gm, gp), gm, km)
    d2ydp2(1,2,i)=exp(-g_p*tau)*d2xdp2(i,2).*p0(1).*p0(2);  
                % exp(-gp*tau)*diff(x(km, gm, gp), gp, km) - tau*exp(-gp*tau)*diff(x(km, gm, gp), km)
    d2ydp2(1,3,i)=(exp(-g_p*tau)*d2xdp2(i,3)-tau*exp(-g_p*tau)*sensitivities_1(i)).*p0(1).*p0(3);
                % exp(-gp*tau)*diff(x(km, gm, gp), gp, km) - tau*exp(-gp*tau)*diff(x(km, gm, gp), km)
    d2ydp2(3,1,i)=d2ydp2(1,3,i);%(exp(-g_p*tau)*d2xdp2(i,3)-tau*exp(-g_p*tau)*sensitivities_1(i)).*p0(1).*p0(3);
                % exp(-gp*tau)*diff(x(km, gm, gp), gm, gp) - tau*exp(-gp*tau)*diff(x(km, gm, gp), gm)
    d2ydp2(3,2,i)=(exp(-g_p*tau)*d2xdp2(i,6) - tau*exp(-g_p*tau)*sensitivities_2(i)).*p0(2).*p0(3);
                % exp(-gp*tau)*diff(x(km, gm, gp), gm, gp) - tau*exp(-gp*tau)*diff(x(km, gm, gp), gm)
    d2ydp2(2,3,i)=d2ydp2(3,2,i);%(exp(-g_p*tau)*d2xdp2(i,6) - tau*exp(-g_p*tau)*sensitivities_2(i)).*p0(2).*p0(3);
                % exp(-gp*tau)*diff(x(km, gm, gp), gm, km),
    d2ydp2(2,1,i)=d2ydp2(1,2,i);%exp(-g_p*tau)*d2xdp2(i,2).*p0(1).*p0(2);   
                % exp(-gp*tau)*diff(x(km, gm, gp), gm, gm)
    d2ydp2(2,2,i)= exp(-g_p*tau)*d2xdp2(i,5).*p0(2).*p0(2);
                % exp(-gp*tau)*diff(x(km, gm, gp), gp, gp) + tau^2*exp(-gp*tau)*x(km, gm, gp) - 2*tau*exp(-gp*tau)*diff(x(km, gm, gp), gp
    d2ydp2(3,3,i)=(exp(-g_p*tau)*d2xdp2(i,9) + tau^2*exp(-g_p*tau)*pr(i) - 2.*tau*exp(-g_p*tau)*sensitivities_3(i)).*p0(3).*p0(3);
end

ids=1:length(d);
nonans=~isnan(d);


% Noise function
gradient=dydp(ids(nonans),[1,2,3]);
f_t=pr.*exp(-g_p*tau);
noisefuns=sqrt(sigma2)*ehat(1)+sqrt(sigma2)*ehat(2).*f_t;
noisefun=noisefuns(ids(nonans),:);
d2ydp2=d2ydp2(:,:,ids(nonans));

% Asymptotic covariance matrix eqn: 2.33 and page 138 Ci=E_i
% FIM
C_inv=(gradient'*(get_stableinverse(diag(noisefun).^2))*gradient);

% Hessian based on second order sensititivites. 
residuals=d(ids(nonans))-f_t(ids(nonans));
    H2=zeros(3,3);
    for n=1:3
        for m=1:3
            for t=1:length(noisefun)
       H2(m,n)=H2(m,n)-residuals(t).*(1./noisefun(t).^2).*d2ydp2(m,n,t);
            end
        end
    end
    Hessian=(H2+C_inv);

end
