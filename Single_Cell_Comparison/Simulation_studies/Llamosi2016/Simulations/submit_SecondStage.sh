#!/bin/bash
#$ -V
#$ -q sc02.q
#$ -N Simulation_second
##$ -pe openmpi624 8
#$ -cwd

sh bash_second_submit.sh $1 $2 $3 $SGE_TASK_ID


