for i in 50 150 250 350 500
do 
qsub  -t 1-5 submit_plots.sh Data_Fig3a $i AR
done

for i in 50 150 250 350 500
do
qsub  -t 1-5 submit_plots.sh Data_Fig3a $i PL
done

for i in 2 4 8 16 32
do
qsub  -t 1-5 submit_plots.sh Data_Fig3b $i AR
done

for i in 2 4 8 16 32 
do
qsub  -t 1-5 submit_plots.sh Data_Fig3b $i PL
done

for i in 20 40 60 80 100
do
qsub  -t 1-5 submit_plots.sh Data_Fig3c $i AR
done

for i in 20 40 60 80 100
do
qsub  -t 1-5 submit_plots.sh Data_Fig3c $i PL
done

for i in 25 50 75 100
do
qsub  -t 1-5 submit_plots.sh Data_Fig3d $i AR
done

for i in 25 50 75 100
do
qsub  -t 1-5 submit_plots.sh Data_Fig3d $i PL
done



