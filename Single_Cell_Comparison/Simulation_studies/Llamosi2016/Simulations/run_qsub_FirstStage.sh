for i in 50 150 250 350 500
do 
qsub -N Simulations_a -t 1-5 submit_script_Figa.sh $i AR
sleep 10
done

for i in   50 150 250 350 500 
do
qsub -t 1-5 -hold_jid Simulations_a -N Simulations_a_PL submit_script_Figa.sh $i PL
sleep 10
done

#### Figure b
for i in 2 8 32  128
do
qsub -t 1-5 -hold_jid Simulations_a_PL -N Simulations_b_AR submit_script_Figb.sh $i AR
sleep 10
done

for i in 2 8 32 128
do
qsub -t 1-5 -hold_jid Simulations_b_AR -N Simulations_b_PL submit_script_Figb.sh $i PL
sleep 10
done

######### Figure c
for i in 20 40 60 80 100
do
qsub -t 1-5 -hold_jid Simulations_b_PL -N Simulations_c_AR submit_script_Figc.sh $i AR
sleep 10
done

for i in 20 40 60 80 100
do
qsub -t 1-5 -hold_jid Simulations_c_AR -N Simulations_c_PL submit_script_Figc.sh $i PL
sleep 10
done


######### Figure d
for i in 25 50 75 100
do
qsub -t 1-5 -hold_jid Simulations_c_PL -N Simulations_d_AR submit_script_Figd.sh $i AR
sleep 10
done

for i in 25 50 75 100
do
qsub -t 1-5 -hold_jid Simulations_d_AR -N Simulations_d_PL submit_script_Figd.sh $i PL
sleep 10
done

