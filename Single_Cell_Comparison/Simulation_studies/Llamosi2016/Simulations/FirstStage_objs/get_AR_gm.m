function AR=get_AR_gm(sim_struct,noise_params,data,indexes)
%%%------------------------------------------------------------------------
%
%   function get_AR_gm.m
%	Gets the pooled AR (absolute residuals) likelihood. Implementation is based on 
%	"Some Simple Methods for Estimating Intraindividual Variability in Nonlinear Mixed
%	Effects Models"
%	Author(s): Marie Davidian and David M. Giltinan
%	Source: Biometrics, Vol. 49, No. 1 (Mar., 1993), pp. 59-73
%   https://www.jstor.org/stable/2532602?seq=1#page_scan_tab_contents 
%  	Please see the  appendix of the paper
%   OR section 2.4.2 of Nonlinear Models for Repeated Measurement Data, Chapman & Hall, London
%   1996 Reprint edition .
%

%  INPUT==> sim_struct: structure containing the simulated data for all
%                       cells at their estimated kinetic parameters from
%                       last iteration
%           noise_params: Noise parameters, theta_hat in the model
%			data: data.Y
%           indexes: cell IDs (so far only those with all data)


%  OUTPUT==> AR: objective function (pooled AR, in log)

%
%   Lekshmi Dharmarajan, July 2017
%   Checked, Jan 2018
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------

AR_i=zeros(1,length(indexes)); % initialising the individual ARs

% Getting the ingredients needed, residuals, noise model, and geometric mean of log noise model
% for each cell
for_pl=struct();
parfor i = 1: length(indexes)
[for_pl(i).res,for_pl(i).h,for_pl(i).gm]=WLS_gm(sim_struct(i).f_t,noise_params,data.Y(data.ID==indexes(i),:));
end

% If the log geometric mean does not exist for any cell
if any(cell2mat({for_pl.gm})==Inf) 
    AR=Inf;
else
% Calculate the AR objective function for each cell
g_dot=log(geomean(cell2mat({for_pl.h}')));
for i = 1: length(indexes)
    AR_i(i)=sum(sum((abs(for_pl(i).res).*exp(g_dot)./for_pl(i).h)));
end

% Pooled AR over all cells
AR=log(sum(AR_i));
end
end
