function res=WOLS(t,kinetic_params,weights,d,int_opts,model_fun)
%%%------------------------------------------------------------------------
%
%   function WOLS.m
%
%   Standard WOLS (Weighted Ordinary Least Squares) method
%   as in the Davidan book:
%   Nonlinear Models for Repeated Measurement Data, Chapman & Hall, London
%   1996 Reprint edition (Look Chapter 2, step three of GLS,  page 30)
%   Objective function equation 2.17 in book.
%
%   Tools needed: 
        % a) ODE SOLVER

%  INPUT==> t: Time values in experiment,
%		  	kinetic_params: log kinetic parameters
% 		    weights: Weights calculated based on theta_hat & last estimates
%           of kinetic_params using var_fun (g in manuscript)
%			d: data
%           int_opts: Options passed to integrator, structure.

%  OUTPUT==> res: The objective function weighted ordinary least squares. 

%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------
nonans=~isnan(d);
kinetic_exp=exp(kinetic_params);

if any(kinetic_exp>1e9) %If the parameter is too big 
res=Inf;
else
[f_t]=model_fun(t,kinetic_exp,int_opts);

	% Only use the result if ODE integration did not fail. 
	if (any(weights==Inf)||any(f_t==Inf)||~any(isreal(log(weights))))
	res=Inf;
	else
	res=(d(nonans)-f_t(nonans))'*(diag((1./weights(nonans)).^2))*(d(nonans)-f_t(nonans)); 
	end

end
