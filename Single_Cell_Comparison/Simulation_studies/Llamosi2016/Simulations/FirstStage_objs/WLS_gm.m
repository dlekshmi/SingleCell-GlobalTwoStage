function [res,h,gm]=WLS_gm(f_t,noise_params,d)
%%%------------------------------------------------------------------------
%
%   function WLS_gm.m
%
%   Used by get_AR_gm and get_PL_gm.m
%   Generated the things needed to compute the AR/ PL objective functions
%   
%  INPUT==> f_t: simulated response of the cell
% 		  noise_params: Noise parameters, theta in the model
%			d: data


%  OUTPUT==> res: residuals
%			  h:  noise model per cell. 
% 			  gm: log geometric mean noise function of the cell

%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------

nonans=~isnan(d); % NaN values

if any(f_t==Inf) %If integration had failed
res=Inf;
h=Inf;
gm=Inf;
else

% Run MODEL
h=var_fun(noise_params,f_t);

% Return values needed to evaluate the COST function.

if (any(h==Inf)||sum(log(h)==-Inf)||~any(isreal(log(h))))
res=Inf;gm=Inf;h=Inf;
else  
	res= d(nonans)-f_t(nonans);
	h=h(nonans);
	gm=1/length(h)*sum(log(h));
end

end
