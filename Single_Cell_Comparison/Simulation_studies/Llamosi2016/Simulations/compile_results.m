% Compile the results. 
GTS_betahats=zeros(1,3,25);
GTS_Dhats=zeros(3,3,25);

NTS_betahats=zeros(1,3,25);
NTS_Dhats=zeros(3,3,25);

true_betahats=zeros(1,3,25);
true_Dhats=zeros(3,3,25);


data_folder='../Simulations/Simulated_Data/Data_Fig3a/';
folder_names={'500','350','250','150','50'};

% Results to NTS
dirname_NTS='./Pubs_Results/Data_Fig3a/';
dirname_GTS='../Simulations/SecondStage_Results/Data_Fig3a';

for j=1:length(folder_names)
    folder_name=folder_names{j};
for i=1:5
% load GTS
load(sprintf('%s/%s/AR_Result_%d.mat',dirname_GTS,folder_name,i))
GTS_betahats(:,:,i)=betahat;
GTS_Dhats(:,:,i)=Dhat;

% NTS
load(sprintf('%s/%s//Result_%d.mat',dirname_NTS,folder_name,i))
NTS_betahats(:,:,i)=betahat_sts(1:3);
NTS_Dhats(:,:,i)=Dhat_sts(1:3,1:3);

% Data
load(sprintf('%s/%s/Simulated_%d.mat',data_folder,folder_name,i))
true_betahats(:,:,i)=betahat;
true_Dhats(:,:,i)=Dhat;
end
end
