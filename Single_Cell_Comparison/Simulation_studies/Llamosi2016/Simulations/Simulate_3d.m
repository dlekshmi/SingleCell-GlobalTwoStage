 % Simulate data for figure 3d
 % part of the trajectories are removed. 
 
clear all
cellnum=350;
percent=[0.25,0.50,0.75,1.00];
label={'25','50','75','100'};


seed=1;
for kk=1:length(percent)
    pp=percent(kk);
for J=1:5
load(sprintf('./Simulated_Data/Data_Fig3a/350/Simulated_%d.mat',J),'p','Data')%end
Data_orig=Data; 
t=unique(Data.TIME);
time_cuts=[0.25,0.5,0.75].*max(t);
rng(seed);
to_scramble=randperm(cellnum,round(pp*cellnum));
time_cut=randperm(3,1);
%time_cut_beg=0.70*max(t); % remove first 70 percent
%time_cut_last=0.30*max(t); 
seed=seed+1;

for i=1:length(to_scramble)
    rng(seed);
    choice_cut=randperm(2,1);
    if choice_cut==1
    Data.Y(Data.ID==to_scramble(i)&Data.TIME<time_cuts(time_cut))=nan;
    else
     Data.Y(Data.ID==to_scramble(i)&Data.TIME>time_cuts(time_cut))=nan;
    end
    seed=seed+1;
end

%Data.Y(ismember(Data.TIME,t(tt)))=nan;
mkdir('./Simulated_Data/Data_Fig3d/');
mkdir(sprintf('./Simulated_Data/Data_Fig3d/%s',label{kk}));

save(sprintf('./Simulated_Data/Data_Fig3d/%s/Simulated_%d.mat',label{kk},J),'pp','to_scramble','Data_orig','Data')%end
writetable(Data,sprintf('./Simulated_Data/Data_Fig3d/%s/Simulated%d.txt',label{kk},J))
%% PLOTS
end
end
