/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_Llamosi2016_o2_info.c
 *
 * Code generation for function '_coder_Llamosi2016_o2_info'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Llamosi2016_o2.h"
#include "_coder_Llamosi2016_o2_info.h"

/* Function Definitions */
mxArray *emlrtMexFcnProperties(void)
{
  mxArray *xResult;
  mxArray *xEntryPoints;
  const char * fldNames[4] = { "Name", "NumberOfInputs", "NumberOfOutputs",
    "ConstantInputs" };

  mxArray *xInputs;
  const char * b_fldNames[4] = { "Version", "ResolvedFunctions", "EntryPoints",
    "CoverageInfo" };

  xEntryPoints = emlrtCreateStructMatrix(1, 1, 4, fldNames);
  xInputs = emlrtCreateLogicalMatrix(1, 3);
  emlrtSetField(xEntryPoints, 0, "Name", mxCreateString("Llamosi2016_o2"));
  emlrtSetField(xEntryPoints, 0, "NumberOfInputs", mxCreateDoubleScalar(3.0));
  emlrtSetField(xEntryPoints, 0, "NumberOfOutputs", mxCreateDoubleScalar(6.0));
  emlrtSetField(xEntryPoints, 0, "ConstantInputs", xInputs);
  xResult = emlrtCreateStructMatrix(1, 1, 4, b_fldNames);
  emlrtSetField(xResult, 0, "Version", mxCreateString("9.1.0.441655 (R2016b)"));
  emlrtSetField(xResult, 0, "ResolvedFunctions", (mxArray *)
                emlrtMexFcnResolvedFunctionsInfo());
  emlrtSetField(xResult, 0, "EntryPoints", xEntryPoints);
  return xResult;
}

const mxArray *emlrtMexFcnResolvedFunctionsInfo(void)
{
  const mxArray *nameCaptureInfo;
  const char * data[22] = {
    "789ced5c4d8ce34815f6a0996111da6541e267f959188456a0d1b4d39d74267d9bfc7592eece5f3be9f4647636edd895a49272d9b19d4eba0f688e8b38b06885"
    "800368c4012d8bc48545882b5ce0c06d41dcb870812b276eb8e2249da9f6b47b62c79d1e3b52947e4ebff2f75edee757ef55d9cc8d5c9e315eaf19ef57becb30",
    "b7c9a7f1fe0463be6e4de41bc6fb5b934ff3f84de6d589fc03e32dc8580723ddfc12f31260a62f519620e6b15e395100a3024d46c7401c7fd3820854a004f6e4"
    "39210b0d41da9efb6a2690afc8dfc90e107adc4062d48e363b0d83e685b13d1f3067f6dcb4b0479bb3e7f589fc28fd98edc8126045047a5a4782ec1ee225596b",
    "282a1b47a82100843436d797580ee236028da471a09194258557a126e3c9bfc3c646683dcae66511a0e92172a4216f50e29a34c31bb2c17bf319bc37199dc71d"
    "a217b3d1bb4dd9797bfcab0c9a0898e77dd7469fa3f489fc28b77768b80a039d6d6af7da2a14b521ab0defa940918d231a60f3f1ca5e3cd1d8277636595d9651",
    "531eb140422c824d56e275c43759805a03cc1243e6fc306fcf6d0b3c37e6f07c6a72dc78fdeac15b7f8b3bd0777a7ed7f4b3ccc5bfc71798677f0f224761e638"
    "9c2ac67052c9df077c6d9008aba7dbe678df9e1bef86c578ccdce722ffbf6af153b2c1f3650a0f910583a9ea1a342e622ae6d19a26f08857d7c8c067765ec4cf",
    "e96b7edca9de7b36781a945ec3a17fc8fbeed820f6eed422f6ae69d2d4578bc7e9935ffffb8ffee599b28bd331b05be7b9dd4274ab770c3646835cd29f3c3bb2"
    "c1f31d0a0f91299ef18a824eb871646e0fb0a04319e77009f1c274fe40cef3c0e63c9fa1ce43e4d664b44687c7462a1f8ff3d4661c891a4772e83f4b1e3edfe2",
    "b1671dc475fd9f61fff23282ab78134686218de7e4e610547a313e95f286974f6df0ae4a5c2d2b2f3a9d7f06f9f1e5e1e155e6c7eb56efd9f9f773145e227779"
    "a1f1ec8067f63be5a19dff8694fed01bff9d37d9113f1fdcfadf877ff72d3ff3c5e4763d89cbfbca70472d670aadfd7e299e5e0d7eae727c59e17d85c24b6449",
    "15e1311481177d992aa54f6497e6fbb2a2b15353d614c649bc3ef9fdcffe75a5f9f0a78937dc39ff227c1b860fd49342a12af4e4785c6d7383444cacad48bd78"
    "95f16385e793141e224fc67025bfd9f9a342e957dcf5c7d41dd2f4f759904fefdf2efb637ef979eaf720726c586e550f23d1fbd963b4bf958ac5f6e4aac0ac06",
    "9fbc8e1fbb7aeeeb141e2253f51cd4120388f41c2e0c24a042c1159efdc4465fa0f405877eb2ace768cb9caf2f7cf4da5f3ff605efacf2580ac1753e966a6f47"
    "9a5b839858c4523451ca06bcb3c2f30d0a0f912dfa2860a48ce7a93a3498e305ef444a5f74e8274bde9db3cc69be63de776bfe76d5fa8bf0ae397a58d86ff1bd",
    "83727eb7b0b30f742ec4157dca3b3bff59f54b28de19439ed9b7ecf9e401a57fe0d03f967c332c221e72309ffcbe5fe69396fd90aa1e7a98abc531aeeec39256"
    "2eb4b54a3fe88758f643ecfcfb590a2f918d986d483a9480d6e800a40075cefe65ada7bf43e911d9c5ebd239931cf2efe3dffec1bffcabb7775b71789a06a576",
    "2abd5596ca3b212d93598d753b44e125f272e3e88e204b928c1b02d9e1a64d7106755e50e7f9a1ce5bd5ebba9d1fbf48e122b2e5ba79baedc97af9234aff9143"
    "3f3db7ce231639e7dbebff28fa37ff153399643f52e434b45eabf5a362538365e8d1fac0aac6d1031b5caf52b8880c356c5eff75b2cddc9b7d29754abfeed03f",
    "e47ad48223202ab2e11ef6199326ee7112a73ff2731f2513ee9d70fd3644c94ab2b7df4fb42bbd6adaa3fd61abcab345d6b7a1d68246cef4e4be8365ac4f3ec3"
    "2fd314a7fb2e7dc32babf5b8e33e482bc782b675380ccb7249e3d6c30feb8c37bc5a89fae38c57775a50d5f4169ce1b3abdbdea4f011994c428db11b2d5945b2",
    "ac34e463a0b6903c344b42677d93a736787a945ecfa1bfe6f78f9beebbc034c7f3c8273ff60b0fadf29b9c1cf5cbad7c3dde51e5949aded36bc37cdda379e49f"
    "6df0fe90c24b640fe3eacec5fff0627dc91bccf9fbf0482271a94e845804a31cd62f951fcbd4786517fc4ae5c7e9b433c88fcc62bc2c6d5442a7a5137c38ea77",
    "c3273bd96e856bd6b3abb17ee775fc3cb0c173d9baee45f929747835a8e7cee93b3dbf6bfad7ad9e7bd9f2dd22759c31bec48f9cf1f12aeabfd9fd8cc61fac69"
    "84d3facfd7f34e348ad641a85b0c55612254199eaef311e4d57a41c043938710bf043c8438e021b3380f3773f99ed22b80919ecd73ed5266b79409a144d08771",
    "52cf2dbbbf19d46fabcfababacdf56753ddcaeaff906858bc8549fa389786d6dd406d27c7c2eabae73db4fd6f78f1393d8b1490ef783fde6adf7fccbb78e8462"
    "ad5824d1eb6d95414ba80cc287c0abfb5357358e5c786ec3986f102388c199bdcbda7fe9cdf31ac67e324d72743f38f3e67f9ff877bf57a2c9c7b54421523a46",
    "b132d6f558add7ef7bb4fff2baf2ed12fb2dc77c1b68a0a18216f9db11df56e2be1ed34f732639beafc7d7bc53e3f908176f258b425ad3bafc41aeb395923c5a"
    "af7b6a8317537889bc5cdedd6902d217d13b2ad03a32122fc7c3af5138896cc5c3b9611dcd33ed78d8a4f03497e7b799492eac2324437ff2ef7c332f1e6c150b",
    "b141667b472cf4e562a99a3a0dfb7cbe69e7c760bf73b0df7951beadf27ee7559d6f7e95c245648a6fd339d9aca5b2cc3cc75378f8a5f86962d299ab66bfe382"
    "7d954f3fe9f997772d90186ec9cafa61bface8314eea0cb7e3439ff3ce853c37e9abe84beda7bc4de9bdbd3cff18a2e3e713fde7a35ff8976795835a4d4c4584",
    "62954f61d8ddc169e13ee7513fc5eeba0d28bc447e943b4c2e10469a912a80c89a41340925f3233d32032a05148045808593d9baeea10dbe6f52f8884cf1cd5c"
    "9e481a07db0067cd473bcfec5fe4f96203dcc3f2105faa2eee52fa5dc6e4e1220eb4f0dc19172dac1c3f44cdc2ce17e0e53b3fbfdae7f0b9a6bf082f4be55ef4",
    "6146e14ff74414dbe476a3cdca81b6227dce230a2f91178d2b9beb7b622fcec515b8265dc68f5fa27011d9e4e37992bbc1bf55bf7e2dc8bbf65fbee78f3ea7d5"
    "fd41b54d14e2079d621bc27a21dd3fd18b6a1631def0ee031bbc030a2f9197723ddfc955b881a2c8aa0e448bf85298203f06f9d13dfd203fba931f8f6c70dda3",
    "701199e2e3c5cc67dce1e575bbce2dc8cfc77ee1a7e5736f53d92ec8e07c24dd2e77f5d2b0553ee4546635f87955fd09bbbee957285c44b6eadf4c483fb37759"
    "7d9c234aef88f1e63ab660bcbefbcbdff9836f96f7d3ca87dd7cba14e2fa1dfeb4b3b91b12425cd1a3fb8b9edae07dde3c6b89ebf30238e651636ec3a64beb16",
    "63fe69f01434145d9d8ebbac7d323ca5c72fcf6f5393dcd87fedeb7d324a6ebda6458bc575f534baddedec6da04874371bf0709e878bcc27281e0a0d81d774e6"
    "72f3d15bd4784436460a6f3097cb838f29fdc70efc76e17cd4b42a9877328bf32f141bdccfe73754a9966c4ba31e12a4f0ee6690075f887fc17e9960bfcc75db",
    "2ff37f6011fcc0", "" };

  nameCaptureInfo = NULL;
  emlrtNameCaptureMxArrayR2016a(data, 32280U, &nameCaptureInfo);
  return nameCaptureInfo;
}

/* End of code generation (_coder_Llamosi2016_o2_info.c) */
