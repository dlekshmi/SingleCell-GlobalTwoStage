/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * jac_Llamosi2016_o2.h
 *
 * Code generation for function 'jac_Llamosi2016_o2'
 *
 */

#ifndef JAC_LLAMOSI2016_O2_H
#define JAC_LLAMOSI2016_O2_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "Llamosi2016_o2_types.h"

/* Function Declarations */
extern void jac_Llamosi2016_o2(const real_T varargin_2[12], const real_T
  varargin_3[12], const real_T varargin_4[4], real_T Jfx[144], real_T Jgx[144],
  real_T Jfp[48], real_T Jgp[48]);

#endif

/* End of code generation (jac_Llamosi2016_o2.h) */
