%% Make area 
sub=[500,350,250,150,50];
miss=1;
missing=0;
All_data=struct();
All_data.datapnts=[];
All_data.missing=[];
All_data.trial=[];
All_data.cptime=[];
All_data.GTScptime=[];
All_data.SAEMcptime=[];
for ii=1:length(sub)
Datapnts=sub(ii);

for jj=1:length(miss)
missing=miss(jj);

dirnamegl=sprintf('./FirstStage_Results/Data_Fig3a/%d/',Datapnts);

%files=dir(dirname);
for num=1:5
All_data.datapnts=cat(2,All_data.datapnts,Datapnts);
All_data.missing=cat(2,All_data.missing,missing);
All_data.trial=cat(2,All_data.trial,num);

fbasegl=sprintf('AR_Result_%d.mat',num);


load(sprintf('%s/%s',dirnamegl,fbasegl))
All_data.cptime=cat(2,All_data.cptime,sum(cpu_end));

% GTS
dirname=sprintf('SecondStage_Results/Data_Fig3a/%d/',Datapnts);
fbase=sprintf('AR_Result_%d.mat',num);

load(sprintf('%s/%s',dirname,fbase))
All_data.GTScptime=cat(2,All_data.GTScptime,cpout(1));

%% Load Now the SAEM predictions
load(sprintf('../SAEM_FILES/Data_Fig3a/%d/model_mlxtran%d/results.mat',Datapnts,num));

All_data.SAEMcptime=cat(2,All_data.SAEMcptime,cpu_time(1));
end
end
end
save(sprintf('../Time_GLARL_SAEM%s.mat',date()),'All_data')

