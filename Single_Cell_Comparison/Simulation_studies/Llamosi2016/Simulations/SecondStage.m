function []=SecondStage(folder,subfolder,residual_type,trial)
%%%------------------------------------------------------------------------
%
%   function SecondStage.m
% 	Implements the Second stage for the analysis of the data from Llamosi et al, 2016.
% 	If using an SGE cluster, this can be run using the accompanying .sh script. The resulting
%   mat file contains following information:
%  'cpout': CPU TIME taken
%  'Pop_ests':  Result structure containing:
%               PopCovar: (D) Cov. matrix
%               eBayesEsts: (beta_i) ebayes individual estimates
%               PopMean: (betahat) mean parameter estimate
%               Mean_convergence: (betadiffs) convergence of parameter vector: differences
%               Var_convergence: (diffs) convergence of D: differences
%               Var_allvalues: (allDs) Values of entries of D (covariance) at each
%                               iteration
%               Mean_allvalues: (allbs)Values of entries of betahat (mean) at each
%                               iteration
%               Asymcovs: Asym. covariance of betahat
%  'theta_hat','sigma2', 'C_inv', 'C_inv_hessian': noise parameters and the inidividual
%  FIMs/ Hessian
%  'beta_i': initial individual estimates
%  'betahat, Dhat': Final population mean and covarariance of the parameters
%   'inv_flags' : flags (1: for inverting FIM, 2: for Hessian) indicating if inversion using Cholesky worked (0) or
%   not (anything above 0). This means, we should be careful in using that
%   cell's uncertainity estimate.

%   If using an SGE (Sun Grid Engine) cluster, this can be run using the accompanying .sh script
%       qsub submit_SecondStage.sh

%  	Funtions, objective functions used:
% a) LGTS_EM
% b) Auxilliary functions to calculate matrix inverse.
%
%   Lines that have to modified:
%       % a) input file name, result of the first stage
%         b) Output directory

%   Lekshmi Dharmarajan, July 2017
%   Modified/ Checked in: March 2018
%   CSB, ETH Zurich.
%
%%%-----------------------------------------------------------------------
addpath(genpath('Auxilliary'))       % path to helper functions
addpath(genpath('SecondStage_objs')) % path to objective functions used

%%%%%%%%%%%% Tweaking prameters
outdir='SecondStage_Results';                                                                           % Folder to store the results (USER MUST MODIFY)
inpfile=sprintf('FirstStage_Results/%s/%s/%s_Result_%d.mat',folder,subfolder,residual_type,trial);


conv=struct(); 	   % Convergence criteria
conv.maxiter=5000; % maximum iteration
conv.tol=1e-3;	   % absolute tolerance

% Get the parameters of the cells
cp1=cputime();
load(inpfile)
beta_i=betai_hat;

% Initialising the EM with population estimates.
beta_nts=median(beta_i);
D_nts=cov(beta_i);
Covs=zeros(size(C_inv));
n_cells=size(beta_i,1);
inv_flags_2=zeros(1,n_cells);
inv_flags_1=zeros(1,n_cells);
C_inv_estimate=C_inv_hessian; % (USER: modify this if you donot compute second order based Hessian)

% Checking if the C_invs can be inverted to give the covariance of
% estimates
% We observe that the second order based estimates are often unstable. 
% This was because of high correlations in the model and the matrix is impossible to invert.
% We computed the second-order information, so we check both FIM and
% Hessians. 
for i=1:n_cells
[Covs(:,:,i),inv_flags_2(i)]=get_stableinverse(C_inv_hessian(:,:,i));
if inv_flags_2(i)>0
  [Covs(:,:,i),inv_flags_1(i)]=get_stableinverse(C_inv(:,:,i)); 
  C_inv_estimate(:,:,i)=C_inv(:,:,i);
end
end


% The function that does the estimation.
ll_gts=@(betahat0,Dhat0) LGTS_EM(C_inv_estimate,betahat0,Dhat0,beta_i,conv);
Pop_ests=ll_gts(beta_nts,D_nts);
betahat=Pop_ests.PopMean;
Dhat=Pop_ests.PopCovar;
betahati=Pop_ests.eBayesEsts;
sprintf('Second stage converged in %d iterations', Pop_ests.niter)

% Asym. Covariance matrix: Again here, sometimes inversion issues can happen and things may fail.
% NaN values usually mean that the matrix is too stiff. One must look closely why this happened.
% One can ignore the cells having inversion issues and calculate the asym. covariance matrix.
disp('Getting uncertainities of the mean estimates')

% Only using cells where inversion of FIM or Hessian worked. 
asymcovs=zeros(length(betahat),length(betahat));
Covs_chosen=Covs(:,:,inv_flags_1==0);
n_chosen=size(Covs_chosen,3);
for i=1:n_chosen 
    asymcovs=asymcovs+get_stableinverse(Dhat+Covs_chosen(:,:,i));
end
Pop_ests.Asymcov=get_stableinverse(asymcovs);

cpout=cputime()-cp1;

% Save results                                                                                %(USER MUST MODIFY)
mkdir(outdir)
mkdir(sprintf('%s/%s',outdir,folder))
mkdir(sprintf('%s/%s/%s/',outdir,folder,subfolder))

save(sprintf('%s/%s/%s/%s_Result_%d.mat',outdir,folder,subfolder,residual_type,trial),'inv_flags_1','inv_flags_2','cpout','D_nts','beta_nts','beta_i','Dhat','betahat','theta_hat','C_inv','sigma2','betahati','Pop_ests','Covs_chosen')
end
