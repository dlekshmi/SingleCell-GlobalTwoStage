function ssa_example_box1(seeds,rate_protein, rate_mRNA,noise_param,cols,sh,file_to_save)
% Simulate a two-state model of gene expression
% Taken from https://github.com/nvictus/Gillespie
% All licesnses as in license.txt

% modified according to my example


import Gillespie.*
data=struct;
for i=1:length(seeds)
    seeds_to_set=seeds{i};
    for j=1:length(seeds_to_set);
        rng(seeds_to_set(j));
        %% Reaction network:
        %   1. transcription:       0       --kR--> mRNA
        %   2. translation:         mRNA    --kP--> mRNA + protein
        %   3. mRNA decay:          mRNA    --gR--> 0
        %   4. protein decay:       protein --gP--> 0
        
        %% Rate constants
        p.kR = rate_mRNA(i);
        p.kP = rate_protein(i);
        p.gR = 0.03;
        p.gP = 0.002;
        scale_p=0.1;
        
        %% Initial state
        tspan = [0, 10000]; %seconds
        x0    = [0, 0];     %mRNA, protein
        
        %% Specify reaction network
        pfun = @propensities_2state;
        stoich_matrix = [ 1  0    %transcription
            0  1    %translation
            -1  0    %mRNA decay
            0 -1 ]; %protein decay
        
        %% Run simulation
        [t,x] = directMethod(stoich_matrix, pfun, tspan, x0, p);
        %[t,x] = firstReactionMethod(stoich_matrix, pfun, tspan, x0, p);
        
        %% Plot time course
        %figure();
        % stairs(t,x); set(gca,'XLim',tspan);
        % xlabel('time (s)');
        % ylabel('molecules');
        % legend({'mRNA','protein'});
        
        % mRNA
        subplot(1,3,[1])
        time_interp=0:10000;
        mRNA=interp1(t,x(:,1),time_interp);
        stairs(time_interp/3600,mRNA,cols{i});
        xlabel('time (h)');
        xlim([0 1])
        ylabel('mRNA molecules');
        hold on
        
        subplot(1,3,2)
        protein=interp1(t,x(:,2),time_interp);
        stairs(time_interp/3600,protein,cols{i});
        xlabel('time (h)');
        xlim([0,1])
        ylabel('Protein molecules');
        hold on
        
        
        
        %subplot(2,3,3)
        f_t=interp1(t,scale_p*x(:,2),time_interp);
        % additive noise ONLY
        y_noise=f_t+(noise_param.theta0).*normrnd(0,1,1,length(f_t));
        
        
        %hold on
        
        texp=[0:120:10000];
        f_t=interp1(t,scale_p*x(:,2),texp);
        y=f_t+(noise_param.theta0+noise_param.theta1.*f_t).*normrnd(0,1,1,length(f_t));
        
        
        data(i).run(j).mRNA=mRNA;
        data(i).run(j).protein=protein;
        data(i).run(j).measured=y;
        data(i).run(j).noise=y_noise;
        data(i).run(j).time=time_interp;
        data(i).run(j).texp=texp;
        data(i).run(j).seed=seeds_to_set(j);
        data(i).run(j).param=p;
    end
    
    % add avergaes
    mean_mRNA=smooth(mean(cat(1,data(i).run.mRNA)),0.1);
    mean_protein=smooth(mean(cat(1,data(i).run.protein)),0.1);
    mean_noise=mean(cat(1,data(i).run.noise));
    mean_meas=(cat(1,data(i).run(1).measured)); % an instance
    
    % measurement
    subplot(1,3,3)
    scatter(texp/3600,mean_meas,sh{i}, 'MarkerFaceColor',cols{i},'MarkerEdgeColor',cols{i});
    xlabel('time (h)');
    xlim([0,1])
    ylim([0, 1000])
    ylabel('AU');
    hold on
    
    
    
    
    subplot(1,3,1)
    plot(time_interp/3600,mean_mRNA,'-r');
    hold on
end


% save the data
save(sprintf('%s.mat',file_to_save),'data')



end


function a = propensities_2state(x, p)
% Return reaction propensities given current state x
mRNA    = x(1);
protein = x(2);

a = [p.kR;            %transcription
    p.kP*mRNA;       %translation
    p.gR*mRNA;       %mRNA decay
    p.gP*protein];   %protein decay
end


