function [Jfx,Jgx,Jfp,Jgp] = jac_Model_twostate(varargin)

    time = varargin{1};
    x = varargin{2};

    if nargout > 1
        % odeSD
        if nargin > 3
            p = varargin{4};
            
        else
            p = [0;0;0;0];
        end
        if nargout > 1
            f = varargin{3};
        end
    else
        % MATLAB ODE integrators
        if nargin > 2
            p = varargin{3};
            
        else
            p = [0;0;0;0];
        end
    end


    if nargout<1
        error('Too many output arguments');
    end

    Jfx = zeros(2, 2);
	Jfx(1) = -p(3);
	Jfx(2) = p(2);
	Jfx(4) = -p(4);


    if nargout == 1
        return
    end

    Jgx = Jfx*Jfx;
	

    if nargout == 2
        return
    end

    Jfp = zeros(2, 4);
	Jfp(1) = 1;
	Jfp(4) = x(1);
	Jfp(5) = -x(1);
	Jfp(8) = -x(2);


    if nargout == 3
        return
    end

    Jgp = Jfx*Jfp;
	Jgp(4) = Jgp(4) + f(1);
	Jgp(5) = Jgp(5) + -f(1);
	Jgp(8) = Jgp(8) + -f(2);


end

