function [f,g,Jfx,Jgx,Jfp,Jgp] = Model_twostate( varargin )
    additionalArgs = {};

    if nargin<1
        f = [0;0];
        g = [0;0;0;0];
        return
    elseif nargin==2 
        time = varargin{1};
        x = varargin{2};
        p = [0;0;0;0];
    else
        time = varargin{1};
        x = varargin{2};
        p = varargin{3};
        if nargin > 3
            additionalArgs = varargin(4:end);
        end
    end 

    f = [p(1) - 1.0*p(3)*x(1);
		p(2)*x(1) - 1.0*p(4)*x(2)];

    if nargout<2 
        return
    end

    g = [-1.0*f(1)*p(3);
		f(1)*p(2) - 1.0*f(2)*p(4)];

    if nargout<3 
         return 
    elseif nargout==3 
         [Jfx] = jac_Model_twostate(time, x, f, p, additionalArgs{:});
    elseif nargout==4 
         [Jfx,Jgx] = jac_Model_twostate(time, x, f, p, additionalArgs{:});
    elseif nargout==5 
          [Jfx,Jgx,Jfp] = jac_Model_twostate(time, x, f, p, additionalArgs{:});
    elseif nargout==6 
          [Jfx,Jgx,Jfp,Jgp] = jac_Model_twostate(time, x, f, p, additionalArgs{:});
    elseif nargout>6 
         error('Too many output arguments');
    end
end
