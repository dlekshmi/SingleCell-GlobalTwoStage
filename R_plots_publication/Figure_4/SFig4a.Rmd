---
title: 'Mup1 model: Supplementary section '
output:
  html_document:
    toc: true
    toc_float: true
subtitle: GTS parameter estimation
abstract: "Things in the Supplementary Text 4 which are associated with the parameter
  estimation in teh Mup1 model using GTS. \n"
---

## Loading Data
Here, we describe the parameter estimation procedure using the Global two stage approach on the processes Mup1 time series data.
First, we load the results.
```{r,echo=FALSE,message=FALSE}
rm(list=ls())
## Generates things needed for Figure 4 in the Manuscript
## Lekshmi Dharmarajan
## CSB, 2018

# libraries needed

library(R.matlab) # Matlab-> R
library(ggplot2)  # PLot
library(gridExtra) # gridlike
library(grid)  # gridlike
require(tikzDevice) # Tex figures
library(xtable) # Making tables
library(RColorBrewer) # Define my colors
library(reshape2)   # for melt
library(plyr)  # table manipulation
library(data.table) # data table
library(stringr)
library(ggdendro)
source('~/corrplot.R')
source('~/colorlegend.R')

grid_arrange_shared_legend <- function(..., nrow = 1, ncol = length(list(...)), position = c("bottom", "right")) {
  plots <- list(...)
  position <- match.arg(position)
  g <- ggplotGrob(plots[[1]] + theme(legend.position = position))$grobs
  legend <- g[[which(sapply(g, function(x) x$name) == "guide-box")]]
  lheight <- sum(legend$height)
  lwidth <- sum(legend$width)
  gl <- lapply(plots, function(x) x + theme(legend.position = "none"))
  gl <- c(gl, nrow = nrow, ncol = ncol)
  
  combined <- switch(position,
                     "bottom" = arrangeGrob(do.call(arrangeGrob, gl),
                                            legend,
                                            ncol = 1,
                                            heights = unit.c(unit(1, "npc") - lheight, lheight)),
                     "right" = arrangeGrob(do.call(arrangeGrob, gl),
                                           legend,
                                           ncol = 2,
                                           widths = unit.c(unit(1, "npc") - lwidth, lwidth)))
  #grid.newpage()
  grid.draw(combined)
  return(combined)
}
# 

setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
direc='../../Single_Cell_Comparison'
setwd(direc)

plot_path='../R_plots_publication/Figure_4/' # saving the plot
man_dir='../R_plots_publication/Supplementary_figs/SI4a/'
dir.create(man_dir)



# to store data used for figures
dir.create(paste0(man_dir,'/data'))

parameter_name=c('ln $k_2$','ln $k_3$','ln $k_4$','ln $k_5$','ln $K_M$','ln $\\alpha_R$','ln $T_P$','ln $T_E$','ln $\\alpha_E$','ln $\\mu_C$','ln $\\mu_M$','ln $V_{EM}$','ln $ V_{EC}$','ln $\\delta$','ln $V_{C0}$','ln $V_{M0}$','$I_{C0}$','$I_{M0}$');
parameter_des=c('GFP transfer cyt - mem','Degradation','Production GFP','Recycling constant','Membrane insertion','Recycling part. coef','Input time const.(protein)','Input time const. (Endocytosis)','Basal endocytosis','Cell growth','Membrane growth','Effective membrane vol.','Spot detection','Spot detection','Initial condition','Initial condition','Initial condition cell intensity','Initial condition memb. intensity')

names(parameter_des)=parameter_name
param_units=c('1/min','1/min','AU/(min)','1/min','AU','-','min','min','-','1/min','1/min','AU','AU','-','AU','AU','AU','AU');



FirstStage_estimates=readMat('./Mup1_Endocytosis/Pub_Results/FirstStageNLMD50_iter5.mat') #First Stage estimates
SecondStage_estimates=readMat('./Mup1_Endocytosis/Pub_Results/NLMD50_5.mat')              #Second Stage estimates
MS_estimate=readMat('./Mup1_Endocytosis/MultiStart_Results/Multistart_ResultsNLMD/Estimates_1.mat')  # One run from Multistarts
Spline_Ests=readMat('./Mup1_Endocytosis/Spline_fits/noise_bounds.mat') # Noise bounds from spline fits estimates
observables=c('$V_C$','$V_M$','$I_C$','$I_M$','$I_S$','$I_{CP}$')

# Load results from 100 multitarts
SecondStage_estimates100=readMat('./Mup1_Endocytosis/Pub_Results/NLMD100_5.mat')
FirstStage_estimates100=readMat('./Mup1_Endocytosis/Pub_Results/FirstStageNLMD100_iter5.mat')
```

The bounds and the estimated values of the kinetic parameters are: 
```{r,echo=FALSE}
#setwd('/Users/dlekshmi/polybox/Manuscript-SingleCell/')
# Tables related to the estimation
betahat=SecondStage_estimates$betahat
colnames(betahat)=parameter_name


# Bounds used
x_L=MS_estimate$MS.params[,,1]$opt.settings[,,1]$x.L
x_U=MS_estimate$MS.params[,,1]$opt.settings[,,1]$x.U

# print dataframe 
Var_Dhat=t(diag(SecondStage_estimates$Dhat))
SE_asymcov=sqrt(diag(SecondStage_estimates$Pop.ests[,,1]$Asymcov))
CoefVar_Dhat=sqrt(exp(Var_Dhat)-1)                               # for lognormal variables: sqrt(exp(sigma^2)-1)
CoefVar_Dhat[17:18]=sqrt(Var_Dhat[17:18])/betahat[17:18]         # Std. dev / mean 

# Table of population estimates
tab_betahat=data.frame(Parameter=parameter_name,unit=param_units,Lowerbounds=t(x_L),Upperbounds=t(x_U),estimate=t(betahat), s.e.=SE_asymcov,SD=t(sqrt(Var_Dhat)),CoV=t(CoefVar_Dhat))

print(xtable(tab_betahat,digits=2),sanitize.text.function=function(x){x})
```

Likewise we used  bounds for the noise estimates. To assess which variation model should be used for the different observations, a smoothing spline was fit through each observation using 'fit' function in MATLAB. The residuals of the fit was used to decide the starting values and the bounds of the noise parameters. We considered an additive and a multiplicative component as the noise model. For the additive component, 98\% quantiles and the median of the root mean squares (from the 'fit' function in MATLAB) across the cells was used to set the bounds and starting values of the parameter. For the multiplicative component, a linear model was fit to each
residuals as a function of for each cell. The 98\% quantiles and the median of the estimates of the slope for each cell was used as the bounds and the starting values for each cell. As the readouts of volume of the cell and membrane there was no evidence of multiplicative noise and so for these only an additive component was assumed.

```{r,echo=FALSE}

theta0_tab=data.frame(observable=observables,lower=Spline_Ests$theta0s.bounds[,1],upper=Spline_Ests$theta0s.bounds[,3],starting=Spline_Ests$theta0s.bounds[,2],estimated=exp(FirstStage_estimates$theta.hat[,1]),stheta=FirstStage_estimates$all.theta0s)

theta1_tab=data.frame(observable=observables,lower=Spline_Ests$theta1s.bounds[,1],upper=Spline_Ests$theta1s.bounds[,3],starting=Spline_Ests$theta1s.bounds[,2],estimated=exp(FirstStage_estimates$theta.hat[,2]),stheta=FirstStage_estimates$all.theta1s)

print(xtable(theta0_tab,digits=3,caption='Bounds and estimates for $\\theta_0$, additive component'),sanitize.text.function=function(x){x})
print(xtable(theta1_tab,digits=3,caption='Bounds and estimates for $\\theta_1$, multiplicative component'),sanitize.text.function=function(x){x})

```

## Shrinkage of individual estimates in second stage of GTS
To see if the second stage indeed did shrink the parameters, we plot the empirical bayes estimates (after the second stage) and the input to the second stage. We see that most parameters shrunk satisfactorily. Note that through the first stage was constrained with bounds, there are no constraints imposed on the second stage. 

```{r,echo=FALSE}
colnames(SecondStage_estimates$betahati)=parameter_name
colnames(SecondStage_estimates$beta.i)=parameter_name
distGLS=mahalanobis(SecondStage_estimates$beta.i,colMeans(SecondStage_estimates$beta.i),SecondStage_estimates$D.nts)
distGTS=mahalanobis(SecondStage_estimates$betahati,SecondStage_estimates$betahat,SecondStage_estimates$Dhat) 

GLS_params=data.frame(SecondStage_estimates$beta.i,Stage='before second stage',dist=distGLS,IDS=(SecondStage_estimates$cell.IDS))  
GTS_params=data.frame(SecondStage_estimates$betahati,Stage='after second stage',dist=distGTS,IDS=(SecondStage_estimates$cell.IDS)) 
Params=rbind(GLS_params,GTS_params)

melted_params=melt(Params,id=c('Stage','dist','IDS'))
levels(melted_params$variable)=parameter_name
my_palette2 =c("#009E73","#D55E00")
p1=ggplot(melted_params,aes(x=value))+geom_line(aes(col=Stage),alpha=0.55,stat='density',linetype=1,bw=0.1,lwd=1)+facet_wrap(~variable,scales='free')+theme_bw()+ylab('Probability density of individual estimates')+xlab(" ")
p1=p1+theme_classic()+theme(legend.position = c(0.9,0.1))+guides(col=guide_legend(nrow=2,byrow=TRUE))+scale_color_manual(values=my_palette2)+scale_fill_manual(values=my_palette2)
print(p1)


tikz(paste0(man_dir,'SI4-figa.tex'),width=8,height=8)
p1
dev.off()

# Save data used for plotting
data_for_figs=list(data_all=melted_params)
save(data_for_figs,file=paste0(man_dir,'/data/figa.RData'))

#p1=ggplot(melted_params,aes(x=value))+geom_histogram(aes(fill=Stage),alpha=0.5)+facet_wrap(~variable,scales='free')+theme_bw()
#p1=p1+theme_classic()+theme(legend.position = 'bottom')+guides(col=guide_legend(nrow=2,byrow=TRUE))+scale_colour_manual(values=my_palette2)+scale_color_manual(values=my_palette2)+scale_fill_manual(values=my_palette2)
#print(p1)
#################
```

## Convergence in the second stage
For the first stage the individual optimizations were done using a convergence tolerance of $1e^{-3}$. And we iterated the GLS with five iterations. These are the timing and convergence measurement done in the SGE cluster. 
```{r,echo=FALSE}
# Second stage convergence
paste0('Second stage iterations: ', SecondStage_estimates$Pop.ests[,,1]$niter)
paste0('Second stage settings: ',SecondStage_estimates$Pop.ests[,,1]$conv.setting)
paste0('Excluded cells: ', SecondStage_estimates$excluded.cells)

convergence_mean=SecondStage_estimates$Pop.ests[,,1]$Mean.convergence
convergence_var=SecondStage_estimates$Pop.ests[,,1]$Var.convergence
Conv=data.frame(iter=1:length(convergence_mean),convergence_mean,convergence_var)

# Maximum difference between successive iteration 
Conv=melt(Conv,id='iter')
p_iter=ggplot(data=Conv)+geom_point(aes(x=iter,y=value,col=variable))+xlab('Iteration Number')+ylab('Maximum Absolute difference in estimates between iterations')+scale_x_log10(breaks=c(1,5,25,75,250,500))+theme_classic()
p_iter=p_iter+scale_color_manual(labels=c('Mean','Variance'),values=c('red','black'),'Parameter')+theme(legend.position = 'bottom')

# Make plot
pdf(paste0(man_dir,'SI4-figb.pdf'),width=4,height=4)
p_iter
dev.off()

# Save data used for plotting
data_for_figs=list(Convergence=Conv)
save(data_for_figs,file=paste0(man_dir,'/data/figb.RData'))

# Time elapsed (total CPU time)
cpu_time=unique(FirstStage_estimates$cptime)+SecondStage_estimates$cpout
cpu_time=cpu_time/3600 # in hrs

cpu_time_100=unique(FirstStage_estimates100$cptime)+SecondStage_estimates100$cpout
cpu_time_100=cpu_time_100/3600 # in hrs



```

## Is the optima we found reliable
To assess if we converged to a good optimum, we compared the GTS individual empirical bayes estimates when using 50 multistarts and 100 multistarts respectively in the first stage. We see that even with just 50 multistart optimizations for each cell, we converged to similar values of parameter distribution. The bhattacharya distance between the two distributions is reported. 

```{r,echo=FALSE}
library(fpc) # Bhattacharya distance
colnames(SecondStage_estimates100$betahati)=parameter_name
colnames(SecondStage_estimates100$beta.i)=parameter_name

multi_start_plot=as.data.frame(rbind(cbind(SecondStage_estimates$betahati,Num_starts=50),cbind(SecondStage_estimates100$betahati,Num_starts=100)))
melted_beta.i2=melt(multi_start_plot,id=c('Num_starts'))
melted_beta.i2$Num_starts=factor(melted_beta.i2$Num_starts)
p_multistart=ggplot(melted_beta.i2,aes(x=value))+geom_line(aes(col=Num_starts),stat='density',alpha=0.5,lwd=1)+facet_wrap(~variable,scales='free')+theme_classic()+scale_color_discrete(name = "Number of starts")
p_multistart=p_multistart+theme(legend.position = c(0.9,0.2),legend.direction = 'vertical')

my_palette2 =c("#009E73","#D55E00")
p_multistart
#tikz(paste0(man_dir,'SI4-figc.tex'),width=8,height=8)
#p_multistart
#dev.off()

# Save data used for plotting
data_for_figs=list(multi_start_plot=multi_start_plot)
save(data_for_figs,file=paste0(man_dir,'/data/figc.RData'))

b_dist=bhattacharyya.dist(SecondStage_estimates100$betahat,SecondStage_estimates$betahat,SecondStage_estimates100$Dhat,SecondStage_estimates$Dhat)
paste0('Bhattacharya distance is: ',b_dist)


```

Lastly, we also see that the correlation of the parameters from the different runs is not very different. 
```{r,echo=FALSE}

# Retrieve correlations from the covariance matrices
Correl_100=cov2cor(SecondStage_estimates100$Dhat)
Correl_50=cov2cor(SecondStage_estimates$Dhat)
dimnames(Correl_100)=list(parameter_name,parameter_name)
dimnames(Correl_50)=list(parameter_name,parameter_name)


# Make plots: Refer to main text for correlation 50. 
tikz(paste0(man_dir,'SI4-figd.tex'), width = 8,height = 4) #define plot name size and font size
par(mfrow=c(1,2),oma=c(0,0,0,0),mar=c(0,0,0,0))
corrplot((Correl_50-Correl_100),method="color",addCoef.col = "black",cl.lim=c(-0.5,0.5),type='upper',tl.cex = 0.7,number.cex = 0.4,mar = c(1,1,1,1), tl.col = "black")

  
corrplot((Correl_100),method="color",addCoef.col = "black",type='upper',tl.cex = 0.7,number.cex = 0.4,mar = c(1,1,1,1), tl.col = "black")
dev.off()


# Save data used for plotting
data_for_figs=list(Correl_100=Correl_100,Correl_50=Correl_50)
save(data_for_figs,file=paste0(man_dir,'/data/figd.RData'))

```