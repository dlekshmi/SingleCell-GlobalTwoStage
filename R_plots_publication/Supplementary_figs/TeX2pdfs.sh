## Get the figures in suplementary text 2 (a)
cd SI2
mkdir pdfs
xelatex -pdf -shell-escape figSI2.tex

cd ../SI4a
mkdir pdfs
## Get the figures in suplementary text 4 (a)
xelatex -pdf -shell-escape figSI4.tex 



cd ../SI4b
mkdir pdfs
echo "Takes a lot of time: time to take coffee"
## Get the figures in suplementary text 4 (b)
xelatex -pdf -shell-escape figSI4b.tex


cd ../SI5
mkdir pdfs
echo "Takes a lot of time: time to take coffee"
## Get the figures in suplementary text 5
xelatex -pdf -shell-escapefigSI5.tex


echo " deleting logs and aux files"
cd ..
find . -name \*.aux -type f -delete
find . -name \*.auxlock -type f -delete
find . -name \*.fdb_latexmk -type f -delete
find . -name \*.fls -type f -delete
find . -name \*.md5 -type f -delete
find . -name \*.dpth -type f -delete
find . -name \*.log -type f -delete