function g=noise_model_user(noise_params,f_t)

% Needs to be modified by the user
theta_0=noise_params(1);
theta_1=exp(noise_params(2));
g=theta_0+theta_1*f_t; % Std. deviation.

end