This is the accopanying .GIT repo for the 
manuscirpt #$%


This is organised as follows: 


1. ``Single_Cell_Comparison``: All the code

2. ``R_plots_publication``: R scripts to generate figures in manuscript
			NOTE: reset directory. The base should point to ``Single_Cell_Comparison/``
			The main figures stored in  ``R_plots_publication/Main_figs``
			The suppl. figures stored in  ``R_plots_publication/Supplementary_figs``

3. ``Template_scripts``: Some skeleton scripts that can be modified for other data/ model.
                         Functions that have to be changed are indicated in the README in the folder. 
                         Note that in general, things in the estimation/ convergence criteria/ assessing uncertainity may need to be modified based on the complexity of the model. 
                         General comments are provided at relevant places.

## System Requirements
1. The accompanying scripts use MATLAB, R and Monolix. 
    Version used in paper: All analysis was done using MATLAB2016b or Monolix (version2016) and R version 3.3.3. 
2. Scripts to run on Linux based Sun Grid Engine (SGE) gird or cluster are also provided but may need modification based on the type of cluster/grid used.
3. Installation of the dependencies and requirements needed are explained in the respective folders. Once the dependencies are installed (NLOPT, the optimizer used may take some time to install in order of minutes), you can simply execute the scripts
     to reproduce what was done. 

Refer to respective READMEs in the folders for the dependencies which can be cloned/ downloaed from the websites. 


For any comments/ issues please contact: 
``lekshmi.dharmarajan@gmail.com`` and/ or ``HMK`` along with 
the corresponding authors in the manuscript.
